<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantHour extends Model
{
    protected $table = 'restaurant_timing';
    protected $primaryKey = 'id';

    protected $guarded = [];
}
