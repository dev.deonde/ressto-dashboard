<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletHistory extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $table = 'recharge_wallet';

    protected $guarded = [];
}
