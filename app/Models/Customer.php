<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    public $timestamps = true;
	 
	  protected $table = 'customers';

   protected $primaryKey = 'id';

    protected $guarded = [];
    public function orders(){
		return $this->hasMany(Order::class,'user_id','user_id')->where('order_status','Delivered');
	}

  public function walletHistory($type = false, $orderBy = 'asc')
  {
      return $this->hasMany(WalletHistory::class,'customer_id')
    ->where('recharge_status','success')
    ->when(!blank($type),function($q)use($type){
      return $q->where('type',$type);
    });
  }
  public function wallet(){
    return $this->hasOne(CustomerWallet::class,'customer_id');
  }
}
