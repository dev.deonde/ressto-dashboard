<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantCuisine extends Model
{
    protected $table = 'restaurant_cuisine';
    protected $primaryKey = 'id';

    protected $guarded = [];
}
