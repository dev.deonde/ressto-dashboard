<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiTextMessage extends Model
{
    public $timestamps = true;
    protected $primaryKey = 'id'; 
    protected $table = 'api_msg_text';
}
