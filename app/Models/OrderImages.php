<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderImages extends Model
{
    use HasFactory;

    public $timestamps = true;
    protected $table = 'order_images';
    protected $primaryKey = 'id';
}
