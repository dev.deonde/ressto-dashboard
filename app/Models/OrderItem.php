<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;

    protected $table = "order_item";
    protected $guarded = [];

    public function menuItemSelections(){
        return $this->belongsToMany(MenuItemSelection::class,'order_menu_item_selection','order_item_id','menu_item_selection_id')->withPivot('price');
    }

    public function menuItemWithTrashed(){
        return $this->belongsTo(RestaurantMenuItem::class,'id','restaurant_menu_item_id')->withTrashed();
    }

    public function masterItem(){
        return $this->belongsTo(MasterItem::class,'master_menu_item_id','id');
    }
}
