<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterItemImage extends Model
{
    protected $table = 'master_item_images';
    protected $primaryKey = 'id';

    protected $appends = ['item_image_path', 'master_media_path'];

    protected $guarded = [];

    public function getItemImagePathAttribute()
    {
		return (config('constant.storage_path').'media/Master_Item_image/'.Auth()->user()->myid()."/".$this->image_name);
	}
	
	public function getMasterMediaPathAttribute()
    {
		
		return (config('constant.storage_path').'media/Master_Item_image/'.Auth()->user()->myid());
    }

}
