<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomizationType extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $table = 'customize_type';
    protected $primaryKey = 'id';

    protected $guarded = [];

    public function Menu_customization()
    {
        return $this->hasMany(ItemSelection::class,'customize_type_id','id')->where('parent_item_id','0');
    }
}
