<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public $timestamps = true;
	 
	protected $table = 'orders';
    protected $guarded = [];
    
	protected $appends = ['base64_id','hash_id'];

    // 1 - pending
    // 2 - accepted
    // 3 - Driver Rejected
    // 4 - auto rejected on other driver accepted order
    // 5 - onpickup
    // 6 - OnTheWay
    // 7 - delivered
    // 8 - customer cancelled
    // 9 - Arrived
    // 10 - Driver Cancelled

    public function driver()
    {
        return $this->belongsToMany(Driver::class,'driver_order_request','orderid','id')->wherePivotIn('order_status',['2','5','6','7','9','11']);
	}



    public function getBase64IdAttribute()
    {
        return base64_encode($this->id);
    }
    public function getHashIdAttribute()
    {
        return '#'.$this->id;
    }
    
    public function customer()
    {
        return $this->belongsTo(Customer::class,'user_id','id');
	}

	public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id', 'id');
    }

    public function reddemedCouponOrder()
    {
        return $this->hasOne(UserRedeemedCoupon::class, 'order_id', 'id');
    }

    public function redeemedCoupon()
    {
        return $this->belongsToMany(Coupon::class, 'fa_user_redeemed_coupons', 'coupon_id','order_id');
    }

    public function user()
    {
        return $this->hasOne(Customer::class, 'id', 'user_id');
    }
    public function orderRequest()
    {
        return $this->driver_order()->where('order_status','7');
    }
    public function driver_order() // This methods is not proper order has multiple driver in driver_order_request table
    {
        return $this->hasOne(DriverOrder::class,'orderid','id');
    }

    public function driver_requests() 
    {
        return $this->hasMany(DriverOrder::class,'orderid');
    }

    public function driver_ongoing_requests() 
    {
        return $this->driver_requests()->whereIn('order_status',['2','5','6','7','9','11']);
    }
    
    public function drivers()
    {
        return $this->belongsToMany(Driver::class,'fa_driver_order_request','orderid','driverid')->wherePivot('order_status','7');
    }
    
    // public function driver()
    // {
    //     return $this->belongsToMany(Driver::class,'fa_driver_order_request','orderid','driverid')->wherePivotIn('order_status',['2','5','6','7','9','11']);
	// }
    
    public function orderItem(){
        return $this->hasMany(OrderItem::class,'order_id');
    }
    
    public function orderrestaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id', 'id');
    }

    public function access()
    {
        return $this->belongsTo(Client::class, 'user_access_id');
    }

    // public function dunzoDriver() {
    //     return $this->hasOne(DunzoHistory::class, 'order_id', 'id')->whereNull('deleted_at')->whereNull('cancelled_at');
    // }
    public function loadshareDriver() {
        return $this->hasOne(LoadSharingHistory::class, 'order_id', 'id')->whereNull('deleted_at')->whereNull('cancelled_at');
    }
}
