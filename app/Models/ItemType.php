<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table = 'item_type';
    protected $primaryKey = 'id';

    protected $guarded = [];
}
