<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VendorSetting extends Model
{
    protected $table = 'vendor_settings';
    protected $primaryKey = 'id';

    protected $guarded = [];
}
