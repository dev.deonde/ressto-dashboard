<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DefaultSetting extends Model
{
    protected $table = 'default_settings';
    protected $primaryKey = 'id';

    protected $guarded = [];
}
