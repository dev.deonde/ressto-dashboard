<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerPromoCode extends Model
{

    public $timestamps = true;

    protected $table = 'promo_code_customer';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
