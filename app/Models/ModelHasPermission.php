<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelHasPermission extends Model
{
    public $timestamps = false;
    protected $table = 'model_has_permissions';
    protected $primaryKey = 'id';

    protected $guarded = [];
}
