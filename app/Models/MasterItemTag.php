<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterItemTag extends Model
{
    public $timestamps = true;
    protected $table = 'master_item_tag';
    protected $primaryKey = 'id';

    protected $guarded = [];
}
