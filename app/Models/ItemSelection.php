<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemSelection extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $table = 'item_selection';
    protected $primaryKey = 'id';

    protected $guarded = [];

    public function item_customization()
    {
        return $this->belongsTo(CustomizationType::class,'customize_type_id','id');
    }

    public function sub_customization()
    {
        return $this->hasMany(ItemSelection::class,'parent_item_id');
    }
}
