<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    public $timestamps = true;
    protected $table = 'promo_code';

    protected $primaryKey = 'id';

    protected $guarded = [];

    protected $appends = ['coupon_image_path','datetime'];

    public function getCouponImagePathAttribute()
    {
        return (config('constant.storage_path').'media/Coupon');
    }

    public function couponCustomer()
    {
        return $this->belongsToMany(Customer::class,'promo_code_customer','coupon_id','customer_id');
    }

    public function getdatetimeAttribute()
    {
        return "{$this->start_datetime} - {$this->end_datetime}";
    }

    public function getCustomer()
    {
        return $this->hasMany('App\Models\CustomerPromoCode','coupon_id','id')->select(['coupon_id','customer_id'])->where('customer_id','0');
    }

 
}
