<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientPaymentGateways extends Model
{
    protected $table = 'client_payment_gateways';
    public $timestamps = true;
    protected $primaryKey = 'id';

    protected $guarded = [];
}
