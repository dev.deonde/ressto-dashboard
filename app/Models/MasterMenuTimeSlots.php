<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterMenuTimeSlots extends Model
{
    protected $table = 'master_menu_time_slots';
    protected $primaryKey = 'id';

    protected $guarded = [];
}
