<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuItemSelection extends Model
{
    use HasFactory;

    protected $table = 'item_selection';
    protected $primaryKey = 'id';
    protected $guarded = [];
}
