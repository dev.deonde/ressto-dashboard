<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterMenu extends Model
{
    protected $table = 'master_menu';
    protected $primaryKey = 'id';

    protected $guarded = [];

    protected $appends = ['menu_image_path', 'menu_media_path'];

    public function getMenuImagePathAttribute()
    {
		return (config('constant.storage_path').'media/Master_Menu_image/original/'.$this->image);
	}
	
	public function getMenuMediaPathAttribute()
    {
        return (config('constant.storage_path').'media/Master_Menu_image');
	}

    public function item()
    {
        return $this->hasMany(MasterItem::class,'main_category_id','id');
	}
}
