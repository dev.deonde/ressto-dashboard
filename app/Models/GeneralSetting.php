<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GeneralSetting extends Model
{
    public $timestamps = true;
  	protected $table = 'general_settings';

    protected $guarded = [];
    protected $casts = [
        'setting_values' => 'array'
    ];
}
