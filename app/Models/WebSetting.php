<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebSetting extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $table = 'web_settings';

    protected $guarded = [];
    
    protected $appends = ['web_logo_image_path','web_home_page_image_path','section_image_path'];

    public function getWebLogoImagePathAttribute() {
        return (config('constant.storage_path').'media/WebPageLayout');
    }

    public function getWebHomePageImagePathAttribute() {
        return (config('constant.storage_path').'media/WebHomePageImage');
    }

    public function getSectionImagePathAttribute() {
        return (config('constant.storage_path').'media/WebSectionImage');
    }
}
