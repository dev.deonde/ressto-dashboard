<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $table = 'restaurant';
    protected $primaryKey = 'id';

    protected $guarded = [];

    public function country(){
        return $this->belongsTo(Country::class, 'country', 'id');
    }
    public function state(){
        return $this->belongsTo(State::class, 'state', 'id');
    }
    public function city(){
        return $this->belongsTo(City::class, 'city', 'id');
    }

    public function client(){
        return $this->belongsTo(Client::class, $this->client_id, 'id');
    }
    public function user()
    {
        // return $this->belongsTo(User::class,'id','user_id');
        return $this->belongsTo(Client::class,'client_id','id');
    }

    public function restaurantSettings() {
        return $this->hasMany(RestaurantSetting::class, 'restaurant_id', 'id',);
    }
}
