<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Mail extends Model
{
    public $timestamps = true;
    protected $primaryKey = 'id'; 
    protected $table = 'email_templates';

    protected $guarded = [];

}
