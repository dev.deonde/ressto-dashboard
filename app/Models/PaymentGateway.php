<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentGateway extends Model
{
    protected $table = 'payment_gateways';
    public $timestamps = true;
    protected $primaryKey = 'id';

    protected $guarded = [];

    protected $casts = [
        'required_key' => 'array'
    ];

    public function clientGatewey(){
        return $this->hasMany(ClientPaymentGateways::class,'payment_gateway_id','id')->where('client_id', auth()->user()->myid());
    }
}
