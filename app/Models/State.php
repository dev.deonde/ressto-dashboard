<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    public $timestamps = true;
    protected $table = 'states';
    protected $primaryKey = 'id';

    protected $guarded = [];
}
