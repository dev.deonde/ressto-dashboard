<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{
    public $timestamps = true;
    protected $primaryKey = 'id'; 
    protected $table = 'log_activities';

    protected $guarded = [];

    public function vendor()
    {
        return $this->belongsTo(Vendor::class, 'user_id','vendor_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'user_id','user_id');
    }
}
