<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class Client extends Authenticatable implements JWTSubject
{

    use Notifiable,HasRoles;

    protected $primaryKey = "id";
    protected $table = 'client';
    public static $guard = 'client';

    protected $guarded = [];

    protected $appends = ['login_type'];

    protected $hidden = [
        'password', 'remember_token',
    ];

   /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getLoginTypeAttribute()
    {
        return 'client';
    }
    
    public function setting()
    {
        return $this->hasMany(VendorSetting::class,'client_id', !blank($this->parent_client_id)?'parent_client_id':'id');
    }

    public function myid()
    {
        return (!blank($this->parent_client_id)?$this->parent_client_id:$this->id);
    }

    public function resturant(){
        return $this->hasOne(Restaurant::class,$this->id,'client_id');
    }
}
