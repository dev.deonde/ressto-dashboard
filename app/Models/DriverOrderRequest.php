<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverOrderRequest extends Model
{
    use HasFactory;

    public $timestamps = true;
    
    protected $table = 'driver_order_request';

    protected $guarded = [];
}
