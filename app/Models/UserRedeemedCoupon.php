<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRedeemedCoupon extends Model
{
    use HasFactory;
    
    public $table = 'user_redeemed_coupons';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function coupon(){
        return $this->belongsTo(PromoCode::class,'coupon_id');
    }
}
