<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SampleImport extends Model
{
	public $timestamps = false;

	protected $guarded = [];
	protected $table = 'sample_import';

	protected $hidden = [
        'id'
    ];
}
