<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryCode extends Model
{
 	public $timestamps = true;
    protected $table = 'country_codes';    
}