<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class FAQ extends Authenticatable
{
    public $timestamps = true;
  	protected $table = 'faq';
}
