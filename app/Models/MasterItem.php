<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterItem extends Model
{
    // use SoftDeletes;
    public $timestamps = true;
    protected $table = 'master_item';
    protected $primaryKey = 'id';

    protected $guarded = [];

    public function item_image()
    {
        return $this->hasMany(MasterItemImage::class,'master_item_id','id')->orderBy('id','ASC');
	}

    public function customization()
    {
		    return $this->hasMany(CustomizationType::class,'master_item_id','id');
	}

    public function tag(){
		return $this->belongsToMany(Tag::class,'master_item_tag','master_item_id','tag_id')->where('status','0');
	}
}
