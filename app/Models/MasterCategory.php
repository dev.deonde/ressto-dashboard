<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterCategory extends Model
{
    protected $table = 'master_category';
    protected $primaryKey = 'id';

    protected $appends = ['category_image_path', 'category_media_path'];

    protected $guarded = [];

    public function getCategoryImagePathAttribute()
    {
        return (config('constant.storage_path').'media/Master_Category/'.$this->image);
	}
	
	public function getCategoryMediaPathAttribute()
    {
        return (config('constant.storage_path').'media/Master_Category');
    }

    public function item()
    {
        return $this->hasMany(MasterItem::class,'main_category_id','id');
	}
}
