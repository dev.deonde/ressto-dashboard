<?php
namespace App;
use Illuminate\Support\Str;

class Helper
{
    public static function settings($client_id,$key_name){
        $controller = \App::make('App\Http\Controllers\Controller');
        $settings = $controller->settings($client_id,$key_name);
        return $settings;
      }
    public static function app_name_snake_case() 
    {
        return \Str::snake(\Str::lower(config('constant.APP_NAME')));
    }

    public static function escape_like($string) {
        $search = array('%', '_');
        $replace   = array('\%', '\_');
        return str_replace($search, $replace, $string);
    }
    public static function logactivity($subject)
    {
        $controller = \App::make('App\Http\Controllers\Controller');
        $logactivity = $controller->addToLog($subject);
        return  $logactivity;
    }

    public static function color_buttons($client_id)
    {
        $controller = \App::make('App\Http\Controllers\Controller');
        $app_color_buttons = $controller->app_color_buttons($client_id);        
        return  $app_color_buttons;
    }
    public static function header_color($vendor_id)
    {
        $controller = \App::make('App\Http\Controllers\Controller');
        $app_color_header = $controller->app_color_header($vendor_id);
        return  $app_color_header;
    }

    public static function timezone($vendor_id)
    {
        $controller = \App::make('App\Http\Controllers\Controller');
        $timezone = $controller->getSystemTimezone($vendor_id);
        return  $timezone;
    }
}