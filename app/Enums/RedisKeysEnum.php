<?php

namespace App\Enums;


enum RedisKeysEnum: string{
    case MASTER_MENU = 'master_menu';
    case MASTER_ITEM = 'master_item';
    case RESTAURANT_MENU_TAG = 'restaurant_menu_tag';
    case API_MENU_ITEMS = 'api_menu_items';
    case API_MENU_ITEMS_by_id = 'api_menu_items_by_id';

}

