<?php
namespace App\Http\Middleware;

use Closure;
use Session;

class CheckURL
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next)
    {
	    $str = $request->url();
        $hostname = explode('.', $str);
        $data = explode("://",$hostname[0]);
        $urls = $data[1].".".$hostname[1];
        $check_host_exist = \App\Models\Client::where('project_name',$hostname[0])->orwhere('project_name','=',$urls)->first();
        if (!empty($check_host_exist)) {
            \Session::put('client_id', $check_host_exist->id);
            \Session::put('vendor', $check_host_exist);

            return $next($request);
        } else {
            $check_host_exist = \App\Models\Client::where('project_name', 'resstodashboard.test')->first();
            if(!blank($check_host_exist)){
                \Session::put('client_id', $check_host_exist->id);
                \Session::put('vendor', $check_host_exist);
            }
            return $next($request);
            return abort(404);
        }
    }
}
