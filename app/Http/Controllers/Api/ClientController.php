<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\ConfirmEmail;
use Illuminate\Http\Request;
use App\Http\Services\ClientService;
use App\Http\Services\RestaurantService;
use App\Http\Services\VendorSettingService;
use App\Http\Services\CuisineService;
use App\Http\Services\ResponseService;
use App\Http\Services\CommonService;
use App\Models\Client;
use App\Models\RestaurantCuisine;
use App\Models\RestaurantHour;
use App\Models\DefaultSetting;
use App\Models\GeneralSetting;
use App\Models\VendorSetting;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Str,Validator,Session,Auth;
use Spatie\Permission\Models\Permission;
use App\Models\Restaurant;
use App\Models\Mail;
use App\Http\Services\MailService;
use App\Http\Services\GeneralSettingService;



class ClientController extends Controller
{
    public function __construct(ClientService $clientService, RestaurantService $restaurantService, VendorSettingService $vendorSettingService, CuisineService $cuisineService, CommonService $commonService, MailService $mailService)
    {
        $this->clientService = $clientService;
        $this->restaurantService = $restaurantService;
        $this->vendorSettingService = $vendorSettingService;
        $this->cuisineService = $cuisineService;        
        $this->commonService = $commonService;
        $this->responseService = new ResponseService;
        $this->mailService = $mailService;
        $this->generalSettingservice = new GeneralSettingService;
    }

    public function signup(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'client_name' => 'required',
            'password' => 'required',
            'contact_number' => 'required',
            'restaurant_name' => 'required',
        ]);        
        \DB::beginTransaction();
        try{
            if (!blank($request->email)) {
                if($request->is_quick_signup == false){                          
                    $client_check = Client::where('email',$request->email)->first();
                    if (!blank($client_check) && $client_check->is_email_verified == '1') {
                        return $this->responseService->response([],__('The email is already exists.'),101);
                    }else if(!blank($client_check) && $client_check->is_email_verified != '1'){
                        $data['code'] =  mt_rand(100000, 999999);
                        $data['subject'] = 'Confirm Email';
                        $data['client_name'] = $request->client_name;
                        try {
                            $view_mail =  $this->mailService->staticSendMail($client->id,'signup-otp');
                            if($view_mail){
                                $data['content'] = html_entity_decode($view_mail->content);                    
                            }
                            $data['content'] = str_replace('{user_name}',$request->client_name,$data['content']);
                            $data['content'] = str_replace('{otp}',$data['code'],$data['content']);
                            $data['content'] = str_replace('{app_name}',config('constant.APP_NAME'),$data['content']);                
                            $response = \Mail::to($request->email)->send(new \App\Mail\RegisterOTPEmail($data));
                            // $html = \Response::view('email.confirm_email', ['data'=>$data])->getContent();
                            // $email = new \SendGrid\Mail\Mail();
                            // $email->setFrom(env('MAIL_FROM_ADDRESS'), env('APP_NAME'));
                            // $email->setSubject($data['subject']);
                            // $email->addTo($request->email, env('APP_NAME'));
                            // $email->addContent("text/html", $html);
                            // $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
                            // $res = $sendgrid->send($email);
                            Log::channel('client')->info('Client Confirm Email',[ 'code' => 101,'msg' => $res]);
                        } catch (\Throwable $th) {                            
                            Log::channel('client')->info('Client Confirm Email',[ 'code' => 101,'msg' => $th->getMessage()]);
                        }
                        
                        $client_updated = $this->clientService->updatedClient(
                            [
                                ['id',$client_check->id]
                            ], 
                            [
                                'email_verified_code' => $data['code'],
                                'client_name' => $request->client_name,
                                'business_name' => $request->restaurant_name, 
                                'email' => $request->email,
                                'contact_number' => $request->contact_number, 
                                'password' => bcrypt($request->password), 
                                'trial_expried_date' => \Carbon\Carbon::now()->adddays(6)->format('Y-m-d H:i:s'), 
                                'ip_address' => $request->ip(), 'slug' => strtoupper(substr($request->client_name,0,4))
                            ]
                        );
                        // updated resturant
                        $resturant_data = ['client_id' => $client_check->id, 'name' => $request->restaurant_name, 'restaurant_user_name' => $request->client_name, 'contact_number' => $request->contact_number, 'email' => $request->email, 'address' => $request->address, 'password' => bcrypt($request->password)];
                        $resturant = $this->restaurantService->updatedClient([['client_id', $client_check->id]] ,$resturant_data);
                        if($client_updated){
                            \DB::commit();
                            return $this->responseService->response([ 'client_id' => $client_check->id],__('Thanks for signing up ! Our team will activate your account as soon as possible.'));
                        }else{
                            \DB::rollBack();
                            return $this->responseService->response([],__('Something went wrong'),101);
                        }
                    }
                }
            }            
            //Add Client
            $client_data = ['client_name' => $request->client_name, 'business_name' => $request->restaurant_name, 'email' => $request->email, 'contact_number' => $request->contact_number, 'password' => bcrypt($request->password), 'trial_expried_date' => \Carbon\Carbon::now()->adddays(6)->format('Y-m-d H:i:s'), 'ip_address' => $request->ip(), 'slug' => strtoupper(substr($request->client_name,0,4))];
            $client = $this->clientService->storeClient($client_data);

            //Web Settings url store
            $webSettingsData = ['client_id'=>$client->id,'settings_key'=>'website_url','setting_values'=>$request->restaurant_name.'.'.config('constant.sub_url'),'status'=>1];
            $storeSettingsData = $this->clientService->storeURLSetting($webSettingsData);

            //Add Restaurant
            $resturant_data = ['client_id' => $client->id, 'name' => $request->restaurant_name, 'restaurant_user_name' => $request->client_name, 'contact_number' => $request->contact_number, 'email' => $request->email, 'address' => $request->address, 'password' => bcrypt($request->password)];
            $resturant = $this->restaurantService->storeRestaurant($resturant_data);

            //Add Vendor Setting
            $settings = DefaultSetting::all();
            foreach($settings as $key => $value){
                $data = ['client_id' => $client->id, 'settings_key' => $value->settings_key, 'setting_values' => $value->setting_values];
                $vendor_settings = $this->vendorSettingService->storeVendorSettings($data);
            }

            //Add cuisine
            $cuisine_data = ['Global','Italian','Indian','Thai','Chinese'];
            foreach($cuisine_data as $key => $value) {
                $cuisine_data = ['client_id' => $client->id, 'cuisine_name' => $value, 'cuisine_description' => $value];
                $cuisine = $this->cuisineService->storeCuisine($cuisine_data);
                //Add Restaurant Cuisine
                if (!blank($cuisine)) {
                    RestaurantCuisine::updateOrCreate(
                        ['restaurant_id' => $resturant['id'], 'cuisine_id' => $cuisine['id']],
                        ['restaurant_id' => $resturant['id'], 'cuisine_id' => $cuisine['id']]
                    );
                }
            }
                        
            $app_signin_page = [
                (object)[ 'type' => 'facebook', 'value' => '0'],
                (object)[ 'type' => 'google', 'value' => '0'],
                (object)[ 'type' => 'phone', 'value' =>'0'],
                (object)[ 'type' => 'email', 'value' => '1'],                
            ];            
            $this->generalSettingservice->defaultUpdate('app_signin_page', $app_signin_page, $client->id);

            //Add Restaurant Hours
            $pickupDelivery = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
            foreach ($pickupDelivery as $key => $timeday) {
                RestaurantHour::updateOrCreate(
                    ['restaurant_id' => $resturant['id'], 'days' => $timeday],
                    ['start_time' => '00:00:00', 'end_time' => '00:00:00', 'status' => '0']
                );
            }            
            $data['code'] =  mt_rand(100000, 999999);
            $data['subject'] = 'Confirm Email';
            // Mail::to($request->email)->send(new ConfirmEmail($data));
            $data['client_name'] = $request->client_name;
            try {
                $view_mail =  $this->mailService->staticSendMail($client->id,'signup-otp');
                if($view_mail){
                    $data['content'] = html_entity_decode($view_mail->content);                    
                }
                $data['content'] = str_replace('{user_name}',$request->client_name,$data['content']);
                $data['content'] = str_replace('{otp}',$data['code'],$data['content']);
                $data['content'] = str_replace('{app_name}',config('constant.APP_NAME'),$data['content']);                
                $response = \Mail::to($request->email)->send(new \App\Mail\RegisterOTPEmail($data));
                
                // $html = \Response::view('email.confirm_email', ['data'=>$data])->getContent();
                // $email = new \SendGrid\Mail\Mail();
                // $email->setFrom(env('MAIL_FROM_ADDRESS'), env('APP_NAME'));
                // $email->setSubject($data['subject']);
                // $email->addTo($request->email, env('APP_NAME'));
                // $email->addContent("text/html", $html);
                // $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
                // $response = $sendgrid->send($email);
                Log::channel('client')->info('Client',[ 'code' => 101,'msg' => $response]);
            } catch (\Throwable $th) {
                Log::channel('client')->info('Client',[ 'code' => 101,'msg' => $th]);
            }            
            $this->clientService->updatedClient([['id',$client->id]], ['email_verified_code' => $data['code']]);
            $clients = Client::where('email',$client->email)->get();            
            foreach($clients as $client){
                $client->givePermissionTo(Permission::all());
            }
            \DB::commit();
            return $this->responseService->response([ 'client_id' => $client->id],__('Thanks for signing up ! Our team will activate your account as soon as possible.'));
        }catch(\Exception $e){            
            \DB::rollBack();
            Log::channel('client')->info('Client',[ 'code' => 101,'msg' => $e->getMessage()]);
            return $this->responseService->response([],__('Something went wrong'),101);
        }
    }

    public function confirEmailClient(Request $request){
        $request->validate([
            'client_id' => 'required|exists:client,id',
            'email_code' => 'required'
        ]);
        \DB::beginTransaction();
        try{
            $client = $this->clientService->getClientWithWhere([['id', $request->client_id]]);
            if(!blank($client)){
                if($client->email_verified_code == $request->email_code){
                    $data = $this->clientService->updatedClient([['id',$client->id]], ['is_email_verified' => '1','email_verified_code' => NULL]);                    
                    if($data) {
                        // $loginData = ['email'=> $client->email,'password' => $client->password];
                        \DB::commit();
                        $loginData = $this->clientlogin($client);
                        return $this->responseService->response(['user'=>$loginData['user'],'userDetail' => $loginData['userDetail'],'languages'=>$loginData['languages'],'token'=> $loginData['token']],__('Login successfully'));
                        //return $this->responseService->response([],__('The verification Successfully'),200);
                    }else{
                        \DB::rollBack();
                        return $this->responseService->response([],__('Something went wrong'),101);
                    }
                }else{
                    \DB::rollBack();
                    return $this->responseService->response([],__('The verification Code does not match'),101);
                }
            }else{
                \DB::rollBack();
                return $this->responseService->response([],__('Client not found'),101);
            }
        }catch(\Exception $e){
            \DB::rollBack();
            Log::channel('client')->info('confirEmailClient',[ 'code' => 101,'msg' => $e->getMessage(),]);
            return $this->responseService->response([],__('Something went wrong'),101);
        }
    }

    public function validation($request)
    {
        return Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
    }

    public function login(Request $request)
    {        
        $getClient = $this->clientService->getClientWithWhere([['email',$request->email]]);        
        if(!blank($getClient) && $getClient->is_email_verified == '0') {
            return $this->responseService->response([],__('Please complete sign up process'),422);
        }
        if(blank($getClient)){
            return $this->responseService->response([],__('Email address not exits!'),101);
        }
        $client_id = \Session()->get('client_id');
        $clientHost = '';
        if ($request->getHttpHost()) {
            $clientHostDomain = $request->getHttpHost();
            if ($clientHostDomain != ''){
                $clientHost = substr($clientHostDomain,0 ,strrpos($clientHostDomain,"."));
                if ($clientHost && $clientHost != '') {
                    $client = $this->clientService->getClientFromHost($clientHost);
                    if (!blank($client)) {
                        $client_id = $client->id;
                    }
                }
            }
        }        
        $client_id = $getClient->id;        
        $validator = $this->validation($request);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }        
        $getuser =  $this->clientService->login($request->email,$request->password,$client_id, $request->has('login_type') ? $request->login_type : '' );
        if ($getuser['user'] && $getuser['is_login'] == 1) {
            $request->session()->forget('client_id');
            session(['client_id' => $getuser['user']->id]);
            session(['client_id_login' => $getuser['user']->parent_client_id != "" ? $getuser['user']->parent_client_id : $getuser['user']->id]);

            $userDetail = null;

            $languages = 'en';
            $arr = [];            
            $settings = VendorSetting::where('client_id',$client_id)->get();
            foreach($settings as $key => $value) {
                $arr = array_merge($arr, [$value['settings_key'] => $value['setting_values']]);
            }
            $primary_lang = ($arr && isset($arr['primary_lang'])) ? $arr['primary_lang'] : 'en';
            $secondary_lang = isset($arr['secondary_lang']) ? $arr['secondary_lang'] : '';
            $languages = $this->commonService->getSelectedLanguage([$primary_lang,$secondary_lang]);            

            $generalSetting = GeneralSetting::where('client_id',$client_id)->get();
            foreach($generalSetting as $key => $value){
                $arr = array_merge($arr, [$value['settings_key'] => $value['setting_values']]);
            }

            if($getuser['user']->client_role != 4) {
                $getuser['user']->access = $getuser['user']->permissions->map(function($p){ return $p->name; });
                if ($getuser['user']->access->filter(function($a) {
                    return (strpos($a, 'Read') !== false);
                })->count() == 0) {
                    auth('client')->logout();
                    return $this->responseService->response([],__('All Permissions revoked by admin'),101);
                }
            }
            return $this->responseService->response(['user'=>$getuser['user'],'userDetail' => $arr,'languages'=>$languages,'token'=> $getuser['token']],__('Login successfully'));
        } else if($getuser['user'] && $getuser['is_login'] == 3) {
            return $this->responseService->response([],__('Login failure'),101);
        } else if($getuser['user'] && $getuser['is_login'] == 0) {
            return $this->responseService->response([],__('Your account is blocked.Please contact admin'),101);
        } else if($getuser['user'] && $getuser['is_login'] == 2) {
            return $this->responseService->response([],__('Please verify your email first'),101);
        } else{
            return $this->responseService->response([],__('No user found'),101);
        }
    }

    public function forgotPasswor(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:client,email'
        ]);
        \DB::beginTransaction();
        try{
            $client = $this->clientService->getClientWithWhere([['email',$request->email]]);
            if(!blank($client)){
                $token = \Str::random(100);
                $data['name'] = $client->client_name;
                $data['link'] = env('APP_URL').'/reset-password/'.$token.'?email='.$client->email;

                $data['subject'] = __('Forgot Password');
                $html = \Response::view('email.forgot_password', ['data'=>$data])->getContent();
                $email = new \SendGrid\Mail\Mail();
                $email->setFrom(env('MAIL_FROM_ADDRESS'), env('APP_NAME'));
                $email->setSubject($data['subject']);
                $email->addTo($request->email, env('APP_NAME'));
                $email->addContent(
                    "text/html", $html
                );
                $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
                $response = $sendgrid->send($email);
                \DB::table('password_reset_tokens')->insert([
                    'email' => $client->email,
                    'token' => $token,
                    'created_at' => Carbon::now()
                ]);
                \DB::commit();
                return $this->responseService->response([],__('We have emailed your password reset link!'));
            }else{
                \DB::rollBack();
                return $this->responseService->response([],__('The selected email is invalid'),422);
            }
        }catch(\Exception $e){
            \DB::rollBack();
            Log::channel('client')->info('Forgot password',[ 'code' => 101,'msg' => $e->getMessage(),]);
            throw ValidationException::withMessages([
                'email' => ['Reset password link already share in your email address.'],
            ]);
            // return $this->responseService->response([],__('Something went wrong'),101);
        }
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            "email" => 'required',
            "password" => 'required',
            "token" => 'required',
        ]);
        $userToken = \DB::table('password_reset_tokens')->where('token',$request->token)->where('email',$request->email)->first();
        $client = $this->clientService->getClientWithWhere([['email',$request->email]]);
        if(!blank($userToken) && !blank($client)){
            $client->password = bcrypt($request->password);
            if($client->save()){
                \DB::table('password_reset_tokens')->where('token',$request->token)->where('email',$request->email)->delete();
                return $this->responseService->response([],__('Password Reset successfully'));
            }
        }else{
            throw ValidationException::withMessages([
                'email' => [$userToken ? 'Client Not found' :'Client Token not match'],
            ]);
        }
    }

    public function userDetail()
    {
        $user = auth()->user();
        if($user->client_role != 4){
            if( is_null($user->parent_client_id) && $user->permissions()->count() == 0){
                $permissions = Permission::all();
                $user->permissions()->sync($permissions);
            }
            if(Auth::guard('client')->user()){
                $user->access = auth()->user()->permissions->map(function($p){ return $p->name; });
            }
        }

        $userDetail = null;

        $languages = 'en';
        $arr = [];
        $settings = VendorSetting::where('client_id',$user->id)->get();
        foreach($settings as $key => $value){
            $arr = array_merge($arr, [$value['settings_key'] => $value['setting_values']]);
        }
        $generalSetting = GeneralSetting::where('client_id',$user->id)->get();
        foreach($generalSetting as $key => $value){
            $arr = array_merge($arr, [$value['settings_key'] => $value['setting_values']]);
        }
        $primary_lang = ($arr && isset($arr['primary_lang'])) ? $arr['primary_lang'] : 'en';
        $secondary_lang = isset($arr['secondary_lang']) ? $arr['secondary_lang'] : '';
        $languages = $this->commonService->getSelectedLanguage([$primary_lang,$secondary_lang]);

        $userDetail = $arr;

        if(Auth::guard('client')->user()){
            $user->access = auth()->user()->permissions->map(function($p){ return $p->name; });
        }
        return response()->json([ 'code' => 200, 'users' => $user,'userDetail' => $userDetail,'languages'=>$languages]);
    }

    public function logout(Request $request)
    {
        $getuser =  $this->clientService->logout($request->has('login_type') ? $request->login_type : '');
        if($getuser && $getuser['user']){
            return $this->responseService->response(['user'=>$getuser['user']], trans('admin.logout_success'));
        }else{
            return $this->responseService->response(['user'=>($getuser) ? $getuser['user'] : ''],trans('admin.no_user'),101);
        }
    }

    public function uploadTmpImage(Request $request)
    {

        $extension = $request->file->getClientOriginalExtension();
        if($extension !== 'zip' && $extension !== 'xlsx' && $extension !== 'xls' && $extension !== 'pem'){
            $file = $this->commonService->uploadImage($request->file, 'Temp');
           // $this->commonService->uploadImageInLocal($request->file, 'Temp',$file);
        }else{
            ini_set('max_execution_time', 0);
            ini_set('max_input_time', 64000);
            ini_set('memory_limit', '1024M');
            ini_set('upload_max_filesize','1024M');
            ini_set('post_max_size','1024M');

            $file = $this->commonService->uploadImageInLocal($request->file, 'Temp');
        }

        return $this->responseService->response(['file' =>  $file,'base_url'=> config('constant.temp_img_path') ],__('Success'));
    }

    public function removeTmpImage(Request $request)
    {
        $deleteFiles = [];
        $data = $request->all();        
        foreach($data['file'] as $file){
            $file = $this->commonService->deleteImage($file, 'Temp');
            array_push($deleteFiles, $file);
        }
        return $this->responseService->response(['deleted'=>$deleteFiles],__('Success'));
    }

    public function updateClientDetaile(Request $request){        
        $request->validate([
            "restaurant_name" => 'required',
            "email" => 'required|email',
            "client_name" => 'required',
            "contact_number" => 'required',
        ]);
        \DB::beginTransaction();
        try {
            $clientData = [
                'client_name' => $request->client_name, 
                'business_name' => $request->restaurant_name,
                'email' => $request->email,
                'contact_number' => $request->contact_number,                                
            ];
            $clientSettings = [
                'primary_lang'=>$request->primary_lang,
                'currency'=>$request->currency,
                'fromemail'=>$request->fromemail,
                'date_format'=>$request->date_format,
                'distance_metric'=>$request->distance_metric,
                'secondary_lang'=>$request->secondary_lang,
                'currency_short_name'=>$request->currency_short_name,
                'timezone'=>$request->timezone,
                'country_code'=>$request->country_code,
            ];
            $restaurantData = [
                'restaurant_user_name' => $request->client_name, 
                'name' => $request->restaurant_name,
                'email' => $request->email,
                'contact_number' => $request->contact_number,
            ];
            foreach($clientSettings as $key => $value){
                if(!blank($value)) {
                    $vendor_settings = $this->vendorSettingService->clientVendorSettings($key,$value);
                }
            }
            $client = $this->clientService->updatedClient([['id', auth('client')->user()->id]],  $clientData);
            $restaurant = $this->restaurantService->updatedClient([['client_id', auth('client')->user()->id]], $restaurantData);
            if($client && $restaurant){
                \DB::commit();
                return $this->responseService->response([],__('Profile updated successfully'),200);

            }else{
                \DB::rollBack();
                Log::channel('client')->info('Client Update',[ 'code' => 101,'msg' => 'client not found',]);
                // return $this->responseService->response([],__('Something went wrong'),101);
            }
            
        } catch (\Exception $e) {            
            \DB::rollBack();
            Log::channel('client')->info('Client Update',[ 'code' => 101,'msg' => $e->getMessage(),]);
            return $this->responseService->response([],__('Something went wrong'),101);
        }
    }

    public function updateClientPassword(Request $request){
        $request->validate([
            "old_password" => 'required',
            "password" => 'required',
            "confirm_password" => 'required|same:password',
        ]);
        try {
            if(\Hash::check($request->old_password, auth('client')->user()->password)){
                $clientData = [
                    'password' =>  bcrypt($request->password)
                ];
                $restaurantData = [
                    'password' => bcrypt($request->password)
                ];
                $client = $this->clientService->updatedClient([['id', auth('client')->user()->id]],  $clientData);
                $restaurant = $this->restaurantService->updatedClient([['client_id', auth('client')->user()->id]], $restaurantData);
                if($client && $restaurant){
                    \DB::commit();
                    return $this->responseService->response([],__('Password updated successfully'),200);
    
                }else{
                    \DB::rollBack();
                    Log::channel('client')->info('Client Password Update',[ 'code' => 101,'msg' => 'client not found',]);
                    return $this->responseService->response([],__('Something went wrong'),101);
                }
            }else{
                \DB::rollBack();
                Log::channel('client')->info('Client Password Update',[ 'code' => 101,'msg' => "old passwords don't match",]);
                return $this->responseService->response([],__("old passwords don't match"),101);
            }
            
        } catch (\Exception $e) {
            \DB::rollBack();
            Log::channel('client')->info('Client Password Update',[ 'code' => 101,'msg' => $e->getMessage(),]);
            return $this->responseService->response([],__('Something went wrong'),101);
        }
    }
    
    public function clientlogin($data) {
        $client_id = $data->id;
        $getuser = $this->clientService->clientlogin($data->email, $client_id);
        if ($getuser['user'] && $getuser['is_login'] == 1) {
            Session::flush();
            session(['client_id' => $getuser['user']->id]);
            session(['client_id_login' => $getuser['user']->parent_client_id != "" ? $getuser['user']->parent_client_id : $getuser['user']->id]);
            $userDetail = null;
            $languages = 'en';
            $arr = [];
            $settings = VendorSetting::where('client_id',$client_id)->get();            
            foreach($settings as $key => $value) {
                $arr = array_merge($arr, [$value['settings_key'] => $value['setting_values']]);
            }
            $primary_lang = ($arr && isset($arr['primary_lang'])) ? $arr['primary_lang'] : 'en';
            $secondary_lang = isset($arr['secondary_lang']) ? $arr['secondary_lang'] : '';
            $languages = $this->commonService->getSelectedLanguage([$primary_lang,$secondary_lang]);
            $userDetail = $arr;
            if($getuser['user']->client_role != 4) {
                $getuser['user']->access = $getuser['user']->permissions->map(function($p){ return $p->name; });
                if ($getuser['user']->access->filter(function($a) {
                    return (strpos($a, 'Read') !== false);
                })->count() == 0) {
                    auth('client')->logout();
                    return $this->responseService->response([],__('All Permissions revoked by admin'),101);
                }
            }
            return ['user'=>$getuser['user'],'userDetail' => $userDetail,'languages'=>$languages,'token'=> $getuser['token']];
            //return $this->responseService->response(['user'=>$getuser['user'],'userDetail' => $userDetail,'languages'=>$languages,'token'=> $getuser['token']],__('Login successfully'));
        } else if($getuser['user'] && $getuser['is_login'] == 3) {
            return $this->responseService->response([],__('Login failure'),101);
        } else if($getuser['user'] && $getuser['is_login'] == 0) {
            return $this->responseService->response([],__('Your account is blocked.Please contact admin'),101);
        } else if($getuser['user'] && $getuser['is_login'] == 2) {
            return $this->responseService->response([],__('Please verify your email first'),101);
        } else{
            return $this->responseService->response([],__('No user found'),101);
        }
    }
    
    public function resendOTP(Request $request) {
        $request->validate([
            "email" => 'required',
        ]);        
        \DB::beginTransaction();
        try{
            $client = $this->clientService->getClientWithWhere([['email',$request->email]]);            
            if(!blank($client)){
                if(!blank($client) && $client->is_email_verified != '1') {
                    $data['code'] =  mt_rand(100000, 999999);
                    $data['subject'] = 'Resend OTP';
                    
                    $view_mail =  $this->mailService->staticSendMail($client->id,'signup-otp');
                    if($view_mail) {
                        $data['content'] = html_entity_decode($view_mail->content);                    
                    }
                    $data['content'] = str_replace('{user_name}',$request->client_name,$data['content']);
                    $data['content'] = str_replace('{otp}',$data['code'],$data['content']);
                    $data['content'] = str_replace('{app_name}',config('constant.APP_NAME'),$data['content']);                
                    $response = \Mail::to($request->email)->send(new \App\Mail\RegisterOTPEmail($data));

                    // $html = \Response::view('email.confirm_email', ['data'=>$data])->getContent();
                    // $email = new \SendGrid\Mail\Mail();
                    // $email->setFrom(env('MAIL_FROM_ADDRESS'), env('APP_NAME'));
                    // $email->setSubject($data['subject']);
                    // $email->addTo($request->email, env('APP_NAME'));
                    // $email->addContent("text/html", $html);
                    // $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
                    // $response = $sendgrid->send($email);

                    $client_updated = $this->clientService->updatedClient(
                        [['id',$client->id]],
                        ['email_verified_code' => $data['code']]);
                    \DB::commit();
                    return $this->responseService->response([],__('OTP send on your email address.'),200);
                }
            }else{
                \DB::rollBack();
                return $this->responseService->response([],__('Client not found'),101);
            }
        }catch(\Exception $e){
            \DB::rollBack();
            Log::channel('client')->info('confirEmailClient',[ 'code' => 101,'msg' => $e->getMessage(),]);
            return $this->responseService->response([],__('Something went wrong'),101);

        }
    }
    
    public function getAllClient(Request $request){
        $user = auth()->user();        
        $clinetListing = Restaurant::whereHas('user',function($q) use($user){
            $q->where('client_id',$user->id);
        })->get();
        
        if($clinetListing){
            return $this->responseService->response($clinetListing,__('Restaurant listing'));
        }else{
            return $this->responseService->response([],__('No restaurant found'),101);
        }
    }

    public function getLanguage(Request $request){
        $data =[];
        $data['primary_lang'] = $this->commonService->primaryLanguages();
        $data['secondary_lang'] = $this->commonService->secondaryLanguages();
        $data['currency'] = $this->commonService->currencyList();
        $data['timezone'] = $this->commonService->timezoneList();
        $data['country_code'] = $this->commonService->countryCodeList();
        // $data['logo'] = Helper::settings(config('constant.default_vendor'), 'flat_icon');
        $data['app_name'] = config('constant.APP_NAME');
        $data['flat_icon_path'] = (config('constant.storage_path').'media/foodapp/original');
        // $data['logo'] = Helper::settings(config('constant.default_vendor'), 'app_logo');
        if($data){
            return $this->responseService->response($data,__('Vendor Settings.'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }
    
}