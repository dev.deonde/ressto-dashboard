<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Services\WalletService;
use App\Http\Services\CustomerService;
use App\Http\Services\ResponseService;

class WalletController extends Controller{

    public $walletService,$responseService;

    public function __construct(WalletService $walletService,ResponseService $responseService,CustomerService  $customerService){
        $this->walletService = $walletService;
        $this->responseService = $responseService;
        $this->customerService = $customerService;
    }

    public function getWalletHistory(Request $request){
        $customer = $this->customerService->customerDetail($request->customer_id);
        $history = $customer->walletHistory($request->type)->orderBy('id','DESC')->paginate(config('constant.per_page'));
        return $this->responseService->response($history,__('Success'));
    }

    public function getWalletBalance(Request $request){
        $wallet = $this->walletService->getBalance(base64_decode($request->customer_id));
        return $this->responseService->response($wallet,__('Success'));
    }

    public function changeStatus(Request $request){
        $wallet = $this->walletService->changeWalletStatus(base64_decode($request->customer),$request->status);
        if($wallet){
            return $this->responseService->response(['updated' =>  true],__('Status changed successfully'));
        }
        return $this->responseService->response(['updated' =>  false],__('Something went wrong'));
    }

}
