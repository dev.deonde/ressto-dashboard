<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Services\CommonService;
use App\Http\Services\GeneralSettingService;
use App\Http\Services\ResponseService;
use App\Models\GeneralSetting;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Spatie\LaravelIgnition\Recorders\DumpRecorder\Dump;

class GeneralSettingController extends Controller
{
    public function __construct(){
        $this->responseService = new ResponseService;
        $this->generalSettingservice = new GeneralSettingService;
        $this->commonService = new CommonService;
    }

    public function getSetting(){
        $getSettings = $this->generalSettingservice->getSetting();        
        $settings = [];
        // dd($getSettings);
        // $restaurantSettings = RestaurantSetting::where('restaurant_id',$this->restaurant_id)->get();
        // dd($restaurantSettings);
        if(!blank($getSettings)){
            foreach($getSettings as $value){
                $settings = array_merge($settings,[$value->settings_key => $value->setting_values]);
            }
        }
        return $this->responseService->response($settings,__('General Setting get Successfully'),200);

    }

    public function updateSetting(Request $request){
        $request->validate([
            'data' => 'required'
        ]);
        DB::beginTransaction();        
        try{
            $pem = ['customer_pem','owner_pem', 'driver_pem'];
            foreach ($request->data as $key => $value) {

                if(in_array($key, $pem)){
                    $setting = $this->getGeneralSettingByKey($key);
                    if(!blank($setting)&& blank($setting->setting_values)){
                        $temp = $value;
                        $value = 'pem file/live_cust/'.$this->commonService->getMovedFileInLocal($value,'pem file/live_cust/',$value.'_');
                        $this->commonService->deleteImage($temp, 'Temp');
                    }
                }
                $data[] = $this->generalSettingservice->updateOrCreate($key, $value);
            }
            DB::commit();
            return $this->responseService->response([],__('General Setting Update Successfully'),200);
        }catch(Exception $e){
            DB::rollBack();
            Log::channel('client')->error('Update Setting',[ 'code' => 101,'msg' => $e->getMessage(),]);
            return $this->responseService->response([],__('Something went wrong'),101);
        }

    }
    
    public function getGeneralSettingByKey($key=null){
        if(!blank($key)){
            return GeneralSetting::where('settings_key',$key)->first();
        }else{
            return null;
        }
    }

    public function deleteGeneralSettingByKey(Request $request){
        $request->validate([
            'key' => 'required'
        ]);
        if(!blank($request->key)){
            if(GeneralSetting::where('settings_key',$request->key)->update(['setting_values'=>''])){
                return $this->responseService->response([],__('General Setting Update Successfully'),200);
            }
        }
    }
    public function removeIosPem(Request $request){

        if(!blank($request['file'])){
            $api_url = config('constant.api_url');
            $res = Http::post("{$api_url}remove_pem",[
                'path' => $request['file']
            ]);
            info('remove pem ',[$res]);
            return $this->responseService->response(['deleted'=>'Pem File Delete Successfully'],__('Pem File Delete Successfully'));
        }
    }
}
