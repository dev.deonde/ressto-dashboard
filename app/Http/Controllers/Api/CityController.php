<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\City;

class CityController extends Controller
{
    protected $client_id;
    public function __construct(){
        $this->client_id = auth()->user()->myid();
    }
    public function storeCity($state_id = '', $city_name = ''){
        if(!blank($state_id) && !blank($city_name)){
            $getCity = City::updateOrCreate(
                [
                    'client_id' => $this->client_id,
                    'state_id' => $state_id,
                    'cityname' => $city_name
                ],
                [
                    'client_id' => $this->client_id,
                    'state_id' => $state_id,
                    'cityname' => $city_name
                ]
            );
            return $getCity->id;
        }
    }
}
