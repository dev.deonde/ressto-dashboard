<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Services\ResponseService;
use App\Http\Services\RestaurantService;
use App\Models\RestaurantSetting;
use Illuminate\Http\Request;

class RestaurantSettingController extends Controller
{
    protected  $responseService, $restaurant_id, $restaurantService;

    public function __construct(
        ResponseService $responseService,
        RestaurantService $restaurantService
        ){
        $this->responseService = $responseService ;
        $this->restaurantService = $restaurantService ;
        if(!blank($this->restaurantService->getRestaurant(auth()->user()->myid()))){
            $this->restaurant_id = $this->restaurantService->getRestaurant(auth()->user()->myid())->id;
        }else{
            return $this->responseService->response([], __('Restaurant Not Found'),101);
        }
    }
    public function getReataurantSetting(){
        $arr = [];
        $settings = RestaurantSetting::where('restaurant_id',$this->restaurant_id)->get();

        foreach($settings as $key => $value){
            $arr = array_merge($arr, [$value['settings_key'] => $value['setting_values']]);
        }
        
        return $this->responseService->response(!blank($arr)?$arr:(object)[], __('Restaurant Setting Get Successfully'));
    }
}
