<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Services\RedisService;
use App\Http\Services\ResstoApiService;
use Illuminate\Http\Request;
use App\Http\Services\MenuService;
use App\Http\Services\ResponseService;
use App\Models\MasterMenu;
use App\Models\MasterMenuTimeSlots;
use App\Models\MasterItem;
use App\Models\MasterItemImage;
use App\Http\Services\CommonService;
use App\Enums\RedisKeysEnum;
use App\Models\MasterItemTag;
use Illuminate\Validation\Rule;

class MenuController extends Controller
{  
    public function __construct(MenuService $menuService, CommonService $commonService)
    {
        $this->menuService = $menuService;
        $this->commonService = $commonService;
        $this->responseService = new ResponseService;
        $this->redisService = new RedisService;
        $this->resstoApiService = new ResstoApiService;
    }

    public function addMenu(Request $request){
        $user = auth()->user();
       
        $request->validate([
           'menu_name' => [
                'required', Rule::unique('master_menu')->where(function ($q) use($user){
                    return $q->where('client_id', $user->myid());
                })
            ],
        ]);
        \DB::beginTransaction();
        try {
            $image = '';
            if ($request->has('image') && !blank($request->image)) {
                $image = $this->commonService->getMovedFile($request->image,'media/Master_Menu_image/original/','Menu');
            }

            if(isset($request->is_wholeday) && $request->is_wholeday == '1'){
                $selling_time = 'WholeDay';
            }else{
                $selling_time = '';
            }
            $start_date = null;
            $end_date = null;
            if(isset($request->start_date) && $request->start_date != ''){
                $dates = explode(',',$request->start_date);
                $start_date = date("Y-m-d", strtotime($dates['0']));
                $end_date = date("Y-m-d", strtotime($dates['1']));
            }
            $menu = ['menu_name' => $request->menu_name,'status' => $request->status,'client_id' => $user->id, 'menu_name_thai' => $request->menu_name_thai, 'selling_time' => $selling_time, 'is_selling_time_slot' => isset($request->is_selling_time_slot) ? $request->is_selling_time_slot : 0,'image'=> $image];            
            $master_menu = $this->menuService->storeMenu($menu);
            if ($master_menu) {
                $get_menu = $this->menuService->getMenuById($user,$master_menu->id);
                // old master menu get in redis
                $redis_menu = $this->redisService->get(RedisKeysEnum::MASTER_MENU->value);
                if(!blank($get_menu) && !blank($redis_menu)){
                    $menus = $this->menuService->getMasterMenu($user);
                    // master menu add in redis
                    $this->redisService->set(RedisKeysEnum::MASTER_MENU->value, $menus);
                }
                if (($start_date != null && $end_date != null) || (isset($request->selling_time) && $request->selling_time != '')) {
                    if (isset($request->is_selling_time_slot) && $request->is_selling_time_slot == '0' && $start_date != null && $end_date != null) { //TimeNotSelected
                        $time_data = ['master_menu_id' => $master_menu->id,'is_wholeday' => 1,'selling_start_date' => $start_date,'selling_end_date' => $end_date, 'timeslot_id' => isset($request->timeslot_id) ? $request->timeslot_id : 0];
                        $add_time = $this->menuService->storeMenuTime($time_data);
                    } else if (isset($request->is_selling_time_slot) && $request->is_selling_time_slot == '1' && (isset($request->selling_time) && $request->selling_time != '')) { //TimeSelected
                        $selling_time = $request->selling_time;
                        //foreach ($selling_time as $key => $value) {
                        $start_end_time = explode('-',$selling_time);
                        $time_data = ['restaurant_menu_id' => $restaurant->id,'is_wholeday' => 0,'selling_start_date' => $start_date,'selling_end_date' => $end_date,'selling_start_time' => $start_end_time[0],'selling_end_time' => $start_end_time[1], 'timeslot_id' => isset($request->timeslot_id) ? $request->timeslot_id : 0];
                        $add_time = $this->menuService->storeMenuTime($time_data);
                        //}
                    }
                }
            }
        } catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($master_menu){
            $this->resstoApiService->setMenuItems();
            return $this->responseService->response($master_menu,__('Menu Added Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function getMasterMenu(Request $request){
        $user = auth()->user();
        // master menu list get in redis
        $get_master_menu = $this->redisService->get(RedisKeysEnum::MASTER_MENU->value);
        if(blank($get_master_menu)){
            // master menu list add in redis
            $get_master_menu = $this->menuService->getMasterMenu($user, $request->menuSkip, $request->menuLimit);
            $this->redisService->set(RedisKeysEnum::MASTER_MENU->value, $get_master_menu );
        }else{
            if(count($get_master_menu->data) <= $request->menuSkip){
                $get_master_menu = $this->menuService->getMasterMenu($user, $request->menuSkip, $request->menuLimit);
            }
        }
        
        if($get_master_menu)
        {
            return $this->responseService->response($get_master_menu,__('Master Menu listing'));
        }else{
            return $this->responseService->response([],__('No master menu found '),101);
        }
    }

    public function getMenuById(Request $request){
        $user = auth()->user();
        $get_menus_item = $this->menuService->getMenuById($user,$request->menu_id);
        if ($get_menus_item) {
            $time_slot = $this->menuService->getMenuTime($get_menus_item->id);
            if ($time_slot){
                if ($time_slot->selling_start_date != null && $time_slot->selling_end_date != null) {
                    $get_menus_item->datetime = date("Y-m-d", strtotime($time_slot->selling_start_date)).'~'. date("Y-m-d", strtotime($time_slot->selling_end_date));
                } else {
                    $get_menus_item->datetime = '';
                }
                if ($time_slot->selling_start_time != null && $time_slot->selling_end_time != null) {
                    $get_menus_item->time = date("H:i:s", strtotime($time_slot->selling_start_time)).'-'. date("H:i:s", strtotime($time_slot->selling_end_time));
                } else {
                    $get_menus_item->time = "WholeDay";
                }
                $get_menus_item->is_wholeday = $time_slot->is_wholeday;
            }else{
                $get_menus_item->datetime = '';//Carbon::now()->format('Y-m-d').'~'.Carbon::now()->format('Y-m-d');
                $get_menus_item->time = 'WholeDay';//Carbon::now()->format('H:i:s').'-'.Carbon::now()->format('H:i:s');
                $get_menus_item->is_wholeday = '1';
                $get_menus_item->selling_time = 'WholeDay';
            }
            return $this->responseService->response($get_menus_item,__('Master menu By Id'));
        } else {
            return $this->responseService->response([],__('No master menu found '),101);
        }
    }

    public function editMenu(Request $request) {
        $user = auth()->user();
        $get_menus_item = $this->menuService->getMenuById($user,$request->id);
        $request->validate([
            'menu_name' => 'required',
        ]);
        \DB::beginTransaction();
        try {
            $menu = $this->menuService->getMenuById($user,$request->id);
            if ($request->has('image') && !blank($request->image)){
                $image = $this->commonService->getMovedFile($request->image,'media/Master_Menu_image/original/','Menu');
            }

            if (isset($request->is_wholeday) && $request->is_wholeday == '1'){
                $selling_time = 'WholeDay';
            } else{
                $selling_time = $menu->selling_time;
            }
            $start_date = null;
            $end_date = null;
            if (isset($request->datetime)){
                // if (empty($request->datetime)) {
                    $start_date = (isset($request->datetime['0']) && $request->datetime['0'] != null && $request->datetime['0'] != '') ? date("Y-m-d", strtotime($request->datetime['0'])) : null;
                    $end_date = (isset($request->datetime['1']) && $request->datetime['1'] != null && $request->datetime['1'] != '') ? date("Y-m-d", strtotime($request->datetime['1'])) : null;
                // }
            }
            $menu_data = ['menu_name' => isset($request->menu_name)?$request->menu_name:$menu->menu_name,'status' => isset($request->status)?$request->status:$menu->status, 'menu_name_thai' => isset($request->menu_name_thai)?$request->menu_name_thai:$menu->menu_name_thai, 'is_selling_time_slot' => isset($request->is_selling_time_slot) ? $request->is_selling_time_slot : $menu->is_selling_time_slot, 'selling_time' => $selling_time,'image'=> isset($image)? $image:$menu->image];
            $restaurant = $this->menuService->updateMenu($menu->id,$menu_data);

            if ($restaurant){
                MasterMenuTimeSlots::where('master_menu_id',$restaurant->id)->delete();
                if (($start_date != null && $end_date != null) || (isset($request->time) && $request->time != '')) {
                    if (isset($request->is_selling_time_slot) && $request->is_selling_time_slot == '0' && $start_date != null && $end_date != null) { //TimeNotSelected
                        $time_data = ['master_menu_id' => $restaurant->id,'is_wholeday' => 1,'selling_start_date' => $start_date,'selling_end_date' => $end_date, 'timeslot_id' => isset($request->timeslot_id) ? $request->timeslot_id : 0];
                        $add_time = $this->menuService->storeMenuTime($time_data);
                    } else if (isset($request->is_selling_time_slot) && $request->is_selling_time_slot == '1' && (isset($request->time) && $request->time != '')) { //TimeSelected
                        $selling_time = $request->time;
                        //foreach ($selling_time as $key => $value) {
                        $start_end_time = explode('-',$selling_time);
                        $time_data = ['master_menu_id' => $restaurant->id,'is_wholeday' => 0,'selling_start_date' => $start_date,'selling_end_date' => $end_date,'selling_start_time' => $start_end_time[0],'selling_end_time' => $start_end_time[1], 'timeslot_id' => isset($request->timeslot_id) ? $request->timeslot_id : 0];
                        $add_time = $this->menuService->storeMenuTime($time_data);
                        //}
                    }
                }
            }
            // old master menu get in redis
            $redis_menu = $this->redisService->get(RedisKeysEnum::MASTER_MENU->value);
            if(!blank($redis_menu) && !blank($restaurant)){
                // master menu filter to get maching data and update in redis
                // $data = array_filter($redis_menu , function($v, $k) use($restaurant) {
                //     if($v->id == $restaurant->id){
                //         return $k;
                //     }
                // }, ARRAY_FILTER_USE_BOTH);
                $menus = $this->menuService->getMasterMenu($user);
                $this->redisService->set(RedisKeysEnum::MASTER_MENU->value, $menus);
            }
        } catch(\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($restaurant){
            $this->resstoApiService->setMenuItems();
            return $this->responseService->response($restaurant,__('Menu Updated Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function deleteMenu(Request $request){
        $user = auth()->user();

        \DB::beginTransaction();
        try{
            $menu = $this->menuService->getMenuById($user,$request->id);
            if($menu){
                $this->commonService->deleteImage($menu->image,'Master_Menu_image/'.$menu->client_id.'/');
                $items = MasterItem::where('main_category_id',$menu->id)->get();
                if(!blank($items)){
                    foreach($items as $item){
                        if ($item) {
                            $images = MasterItemImage::where('master_item_id',$item->id)->get();
                            if(!empty($images)){
                                if(!empty($images)){
                                    foreach($images as $image){
                                        $this->commonService->deleteImage($image->image_name,'Master_Item_image/'.$image->client_id.'/');
                                    }
                                }else{
                                    $this->commonService->deleteImage($images[0]->image_name,'Master_Item_image/'.$images[0]->client_id.'/');
                                }
                                $image_delete = MasterItemImage::where('master_item_id',$item->id)->delete();
                            }
                            $item_tag = MasterItemTag::where('master_item_id', $item->id)->delete();
                            $item->delete();
                        }
                    }
                }
                $master_menu_time_slots = MasterMenuTimeSlots::where('master_menu_id',$request->id)->delete();
                $menu_delete = $menu->delete();


                $redis_menu = $this->redisService->get(RedisKeysEnum::MASTER_MENU->value);
                $redis_menu_item = $this->redisService->get(RedisKeysEnum::MASTER_ITEM->value.':'.$request->id);
                if(!blank($redis_menu)){
                    if(!blank($redis_menu_item) && !blank($redis_menu_item->data)){
                        foreach($redis_menu_item->data as $key => $value){
                            if($value->main_category_id == $request->id){
                                unset($redis_menu_item->data[$key]);
                            }
                        }
                        $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$request->id, $redis_menu_item);
                    }
                    // foreach($redis_menu as $key => $value){
                    //     if($value->id == $request->id){
                    //         unset($redis_menu[$key]);
                    //     }
                    // }
                    $menus = $this->menuService->getMasterMenu($user);
                    $this->redisService->set(RedisKeysEnum::MASTER_MENU->value, $menus);
                }

                
            }
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        if($menu_delete){
            \DB::commit();
            $this->resstoApiService->setMenuItems();
            return $this->responseService->response($menu_delete,__('Menu deleted successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function updateMasterMenuStatus(Request $request){
        $request->validate([
            'status' => 'required',
            'id' => 'required',
        ]);
        \DB::beginTransaction();
        try{
            $user = auth()->user();
            $menu = $this->menuService->updateMasterMenuStatus($request->id,['status' => $request->status]);

        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($menu){
            return $this->responseService->response($menu,__('Master menu status updated'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function deleteMenuImage(Request $request){
        $request->validate([
            'file'  =>  'required'
        ]);
        $this->commonService->deleteImage($request->file,'Master_Menu_image/original');
        $menu = $this->menuService->getMenuWhere([['image','=',$request->file]])->first();
        if(!blank($menu)){
            $menu->image = "";
            $menu->save();
            $this->resstoApiService->setMenuItems();
        }
        return $this->responseService->response(['deleted'  =>  true],__('Image deleted successfully'));
    }

    public function updateMenuStatus(Request $request){
        $request->validate([
            'status' => 'required',
            'id' => 'required',
        ]);
        \DB::beginTransaction();
        try{
            $user = auth()->user();
            $restaurant = $this->menuService->updateMenu($request->id,['status' => $request->status]);

        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($restaurant){
            return $this->responseService->response($restaurant,__('Menu status Updated'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function updateMenuOrder(Request $request){

        if (!empty($request->menu_ids)) {
            foreach ($request->menu_ids as $key => $value) {
                $restaurant = $this->menuService->updateMenu($value,['reorder_data' => $key]);
            }
            return $this->responseService->response($restaurant,__('Menu Order Changed'));
        }
    }
}
