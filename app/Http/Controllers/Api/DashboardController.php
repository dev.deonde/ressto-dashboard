<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Services\DashboardService;
use App\Http\Services\CommonService;
use App\Models\Client;
use App\Http\Services\ResponseService;
// use App\Http\Models\VendorSetting;
// use App\Http\Models\Vendor;
// use App\Http\Models\User;
// use App\Http\Models\Update;
// use App\Http\Models\Restaurant;
// use App\Http\Services\RestaurantService;

class DashboardController extends Controller
{

  protected $dashboardService,$user, $setting ,$timezone;
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(DashboardService $dashboardService)
  {
    $this->commonService = new CommonService();
    $this->dashboardService = $dashboardService;
    $this->responseService = new ResponseService;
    // $this->restaurantService = $restaurantService;
    $this->user = auth()->user();
    if($this->user){      
      $this->client_id = (blank(auth()->user()->parent_vendor_id) ? auth()->user()->id : auth()->user()->parent_client_id);      
      $this->setting = auth()->user()->setting;
    }elseif($this->user = auth('partner')->user()) {
      $this->client_id = $this->user->client_id;
      $this->setting = $this->user->vendor->setting;        
    }
    $this->user_id = 0;
    // if(auth('partner')->check()){
    //     $this->user_id = $this->user->user_id;
    // }
  }

  public function getDashboard(Request $request)
  {    
    $user = auth()->user();
    if(auth()->guard('client')->user()) {
      $client = Client::where('id',$user->myid())->first();
      // $this->authorize('view',$client);
    }
    $settings = auth()->user()->setting;
    $timezone = $this->commonService->getSetting('timezone', 'client');    
    $get_dashboard =  $this->dashboardService->getDashboardList($timezone);    
    if($get_dashboard){
      return $this->responseService->response($get_dashboard,__('Dashboard listing'));
    }else{
      return $this->responseService->response([],__('No data found'),101);
    }
  }

  public function monthWiseDeliveredOrders(Request $request)
  {
    $settings = auth()->user()->setting;
    $timezone = $this->commonService->getSetting('timezone', 'client');        
    $month_wise_order =  $this->dashboardService->getMonthwiseDeliveredOrder($this->client_id,$timezone,$this->user_id);
    if($month_wise_order){
      return $this->responseService->response($month_wise_order,__('Monthwise Delivered Order'));
    }else{
      return $this->responseService->response([],__('No order found'),101);
    }
  }

  public function monthWiseSales(Request $request)
  {   
    $user = $this->user;
    $restaurant_id = 0;
    $client_id = $user->myid();
    $month = 11;
    if (isset($request->months)) {
      $month = $request->months;
    }
    $month_wise_item =  $this->dashboardService->getMonthwiseSales($client_id,$this->timezone,$request->chart,$this->user_id, $month);    
    if($month_wise_item){
      return $this->responseService->response($month_wise_item,__('Monthwise Sales'));
    }else{
      return $this->responseService->response([],__('No sales found'),101);
    }
  }
}