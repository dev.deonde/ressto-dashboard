<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Services\PromoCodeService;
use App\Http\Services\ResponseService;
use App\Http\Services\CommonService;
use App\Models\PromoCode;
use App\Models\CustomerPromoCode;

class PromoCodeController extends Controller
{
    protected $PromoCodeService = null;

    public function __construct(PromoCodeService $PromoCodeService)
    {
        $this->commonService = new CommonService();
        $this->PromoCodeService = $PromoCodeService;
        $this->responseService = new ResponseService;
       
    }

    public function getPromocode(Request $request)
    {
        $user = auth()->user();
        $promo_code_listing =  $this->PromoCodeService->promoCodeListing($user,$request->search,$request->status,$request->sortBy,$request->orderBy,50);
        if($promo_code_listing){
            return $this->responseService->response($promo_code_listing,__('Promo code listing'));
        }else{
            return $this->responseService->response([],__('No promo code found'),101);
        }
    }

    public function getPromoCodeCustomer(Request $request)
    {
          $user = auth()->user();
            
          $get_customer = $this->PromoCodeService->customer($user,$request->search);
          if($get_customer){
                return $this->responseService->response($get_customer,__('Customer listing'));
          }else{
                return $this->responseService->response([],__('No customer found '),101);
          }
    }

    public function addPromoCode(Request $request)
    {   
        $client_id = auth()->user()->id;
        $request->validate([
            'coupon_name' => ['required',
            function ($attribute, $value, $fail)use($client_id) {
                if (PromoCode::where('client_id',$client_id)->where('coupon_name',$value)->count() > 0) {
                    $fail('The '.$attribute.' is already exists.');
                }
            },],
            'coupon_code' => ['required',
            function ($attribute, $value, $fail)use($client_id) {
                if (PromoCode::where('client_id',$client_id)->where('coupon_code',$value)->count() > 0) {
                    $fail('The '.$attribute.' is already exists.');
                }
            },],
            'minimum_order_amount' => 'required',
            'description' => 'required'
        ]);
        \DB::beginTransaction();
        try{
            $coupon_image = '';
            $selectedCustomer = [];

            if($request->has('coupon_image') && !blank($request->coupon_image)){
                $filename = $this->commonService->getMovedFile($request->coupon_image,'media/Coupon/','Coupon');
            }
            $tmp_date= explode(',', $request->start_datetime);
            $start_datetime= date("Y-m-d H:i:s", strtotime($tmp_date['0']));
            $end_datetime=date("Y-m-d H:i:s", strtotime($tmp_date['1']));
            $is_flat_discount = isset($request->is_flat_discount) && $request->is_flat_discount == "1";
            $is_show_customer_app = isset($request->is_show_customer_app) && $request->is_show_customer_app == "1";
        
            if (isset($is_flat_discount) && ($is_flat_discount == "1")) {
                $is_flat_discount=true;
                $flat_discount=$request->flat_discount;
                $discount_percentage = 0;
            } else {
                $is_flat_discount=false;
                $flat_discount=0;
            }
            $coupon_data = [
                'coupon_name' => $request->coupon_name,
                'coupon_code' => $request->coupon_code, 
                'description' => $request->description, 
                'discount_percentage' => isset($request->discount_percentage) ? $request->discount_percentage : '',
                'maximum_discount_amount' => isset($request->maximum_discount_amount) ? $request->maximum_discount_amount : '',
                'minimum_order_amount' => isset($request->minimum_order_amount) ? $request->minimum_order_amount : '', 
                'start_datetime' => $start_datetime,
                'end_datetime' => $end_datetime,
                'per_user_usage' => isset($request->per_user_usage) ? $request->per_user_usage : '',
                'is_flat_discount' => $is_flat_discount,
                'flat_discount' => $flat_discount,
                'status' => '1',
                'is_admin_coupon' => isset($request->is_admin_coupon) ? $request->is_admin_coupon : '',
                'max_user_count' => isset($request->max_user_count) ? $request->max_user_count : '',
                'orders_valid_for_first' => isset($request->orders_valid_for_first) ? $request->orders_valid_for_first : '-1',
                'client_id'=> $client_id, 
                'is_show_customer_app' => $is_show_customer_app,
                'coupon_image' => $filename,
                'coupon_name_other_lang' => isset($request->coupon_name_other_lang) ? $request->coupon_name_other_lang : '',
                'description_other_lang' => isset($request->description_other_lang) ? $request->description_other_lang : '', 
                'coupon_type' => isset($request->coupon_type) ? $request->coupon_type : '',
                'show_display_text' => isset($request->show_display_text) ? $request->show_display_text : '',
                'show_display_text_another_lang' => isset($request->show_display_text_another_lang) ? $request->show_display_text_another_lang : '',
                'promo_code_type' => isset($request->promo_code_type) ? $request->promo_code_type : '1'
            ];
            $coupon = $this->PromoCodeService->storePromoCode($coupon_data);

            if($request->has('selectedCustomer')){
                $this->PromoCodeService->syncCustomer($coupon,$request->selectedCustomer);
            }
            else
            {
                $this->PromoCodeService->syncCustomer($coupon,$request->customercheckbox);
            }
            \DB::commit();
            if($coupon){
                return $this->responseService->response($coupon,__('Coupon added successfully'));
            }
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function deletePromoCodeImage(Request $request)
    {
        $request->validate([
            'file'  =>  'required'
        ]);
        $this->commonService->deleteImage($request->file,'Coupon');
        $coupon = $this->PromoCodeService->getPromoCodeWhere([['coupon_image','=',$request->file]])->first();
        if($coupon){
            $coupon->coupon_image = "";
            $coupon->save();
        }
        return $this->responseService->response(['deleted'  =>  true],__('Coupon image deleted successfully'));
    }

    public function getPromoCodeById(Request $request)
    {
        $user = auth()->user();
        $get_promo_code = $this->PromoCodeService->getPromoCode($user,$request->id);
        if($get_promo_code)
        {
              return $this->responseService->response($get_promo_code,__('Coupon listing'));
        }else{
              return $this->responseService->response([],__('No coupon found '),101);
        }
    }

    public function editPromoCode(Request $request)
    {   
        $client_id = auth()->user()->myid();
        
        $request->validate([
            'coupon_name' => ['required',
            function ($attribute, $value, $fail)use($client_id,$request) {
                if (PromoCode::where('client_id',$client_id)->where('id','!=',$request->id)->where('coupon_name',$value)->count() > 0) {
                    $fail('The '.$attribute.' is already exists.');
                }
            },],
            'minimum_order_amount' => 'required',
            'description' => 'required'
        ]);
        \DB::beginTransaction();
        try{
            $tmp_date= explode(',', $request->datetime);
            // dd($tmp_date);
            $start_datetime= date("Y-m-d H:i:s", strtotime($tmp_date['0']));
            $end_datetime=date("Y-m-d H:i:s", strtotime($tmp_date['1']));
            //$coupon_image = '';
            $coupon_data = [
                'coupon_name' => $request->coupon_name,
                'description' => $request->description, 
                'discount_percentage' => isset($request->discount_percentage) ? $request->discount_percentage : '',
                'maximum_discount_amount' => isset($request->maximum_discount_amount) ? $request->maximum_discount_amount : '',
                'minimum_order_amount' => isset($request->minimum_order_amount) ? $request->minimum_order_amount : '', 
                'start_datetime' => $start_datetime,
                'end_datetime' => $end_datetime, 
                'per_user_usage' => isset($request->per_user_usage) ? $request->per_user_usage : '',
                'is_flat_discount' => isset($request->is_flat_discount) ? $request->is_flat_discount : '',
                'flat_discount' => isset($request->flat_discount) ? $request->flat_discount : '',
                'status' => '1',
                'is_admin_coupon' => isset($request->is_admin_coupon) ? $request->is_admin_coupon : '',
                'max_user_count' => isset($request->max_user_count) ? $request->max_user_count : '',
                'orders_valid_for_first' => isset($request->orders_valid_for_first) ? $request->orders_valid_for_first : '-1',
                'client_id'=> $client_id, 
                'is_show_customer_app' => isset($request->is_show_customer_app) ? $request->is_show_customer_app : '',
                'coupon_name_other_lang' => isset($request->coupon_name_other_lang) ? $request->coupon_name_other_lang : '',
                'description_other_lang' => isset($request->description_other_lang) ? $request->description_other_lang : '', 
                'coupon_type' => isset($request->coupon_type) ? $request->coupon_type : '',
                'show_display_text' => isset($request->show_display_text) ? $request->show_display_text : '',
                'show_display_text_another_lang' => isset($request->show_display_text_another_lang) ? $request->show_display_text_another_lang : '',
                'status' => $request->status,
                'promo_code_type' => isset($request->promo_code_type) ? $request->promo_code_type : '1'
            ];
            if($request->has('coupon_image') && !blank($request->coupon_image)){
                $coupon_data['coupon_image'] = $this->commonService->getMovedFile($request->coupon_image,'media/Coupon/','Coupon');
            }
            $coupon = $this->PromoCodeService->updatePromoCode($request->id,$coupon_data);

            
            if($request->has('coupon_customer')){
                CustomerPromoCode::where('coupon_id', $coupon->id)->delete();
                $this->PromoCodeService->syncCustomer($coupon,$request->coupon_customer);
            }
            else
            {
                $this->PromoCodeService->syncCustomer($coupon,$request->get_customer);
            }
            \DB::commit();
            if($coupon){
                return $this->responseService->response($coupon,__('Promo code updated successfully'));
            }

        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }
}
