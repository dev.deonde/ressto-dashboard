<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Services\CustomizationService;
use App\Http\Services\ResponseService;
use App\Http\Services\MasterItemService;
use App\Models\ItemSelection;
use App\Models\CustomizationType;
use App\Models\MasterItem;
use App\Http\Services\MenuService;
use App\Http\Services\RedisService;
use App\Enums\RedisKeysEnum;
use App\Http\Services\ResstoApiService;

class CustomizationController extends Controller
{
    public function __construct(CustomizationService $customizationService,MasterItemService $masterItemService, MenuService $menuService)
    {
        $this->customizationService = $customizationService;
        $this->masterItemService = $masterItemService;
        $this->menuService = $menuService;
        $this->responseService = new ResponseService;
        $this->redisService = new RedisService;
        $this->resstoApiService = new ResstoApiService;
    }

    public function getItemCustomization(Request $request){
        $user = auth()->user();
        $master_item_id = base64_decode($request->master_item_id);
        $get_item_details = $this->masterItemService->getItemByID($master_item_id);
        $get_item_customization = null;
        if(!blank($get_item_details)){
            $get_item_customization = $this->customizationService->getitemCustomization(['master_item_id' => $master_item_id]);
            return $this->responseService->response(['data'=>$get_item_customization,'item_name'=> $get_item_details->item_name],__('Customization List'));
        }
        return $this->responseService->response(['data'=>$get_item_customization,'item_name'=> ''],__('Customization List'));
    }

    public function addItemCustomization(Request $request){
        $request->validate([
            'type_name' => 'required'
        ]);
        $user = auth()->user();
        $client_id = $user->id;        
        \DB::beginTransaction();
        try{
            $data = $request->only('is_veg_non','type_name','type_name_thai','is_min_selection','is_selection')+['master_item_id' => base64_decode($request->master_item_id)];
            $customisation = $this->customizationService->storeCustomizationType($data);

        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        $menuItemId = MasterItem::where('id',base64_decode($request->master_item_id))->pluck('main_category_id')->first();        
        // menu item add in redis        
        $get_master_item_redis = $this->redisService->get(RedisKeysEnum::MASTER_ITEM->value.':'.$menuItemId);
        if(!blank($get_master_item_redis)){            
            $get_master_item = $this->masterItemService->getMasterItemByID($user,$client_id,$menuItemId,'',50);            
            $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$menuItemId, $get_master_item);
        }
        \DB::commit();
        if($customisation){
            $this->resstoApiService->setMasterItems(base64_decode($request->master_item_id));
            $this->resstoApiService->setMenuItems();
            return $this->responseService->response($customisation,__('Customization Type Added Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function getCustomizationType(Request $request){
        $customisation = $this->customizationService->getCustomization(['id' => $request->id]);
        if($customisation){
            return $this->responseService->response($customisation,__('Customization'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function updateCustomizationType(Request $request){
        $user = auth()->user();
        $client_id = $user->client_id;
        $request->validate([
            'type_name' => 'required'
        ]);
        \DB::beginTransaction();
        try{
            $customisation = $this->customizationService->getCustomization(['id' => $request->id]);
            $data = ['type_name' => isset($request->type_name) ? $request->type_name : $customisation->type_name, 'type_name_thai' => isset($request->type_name_thai) ? $request->type_name_thai : $customisation->type_name_thai, 'is_selection' => isset($request->is_selection) ? $request->is_selection : $customisation->is_selection, 'is_min_selection' => isset($request->is_min_selection) ? $request->is_min_selection : $customisation->is_min_selection, 'is_veg_non' => isset($request->is_veg_non) ? $request->is_veg_non : $customisation->is_veg_non];
            $edit_customisation = $this->customizationService->updateCustomization($request->id,$data);
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($edit_customisation){
            return $this->responseService->response($edit_customisation,__('Customization Type Updated Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function addMenuSelection(Request $request){
        $user = auth()->user();
        $client_id = $user->client_id;
        $item_id = 0;
        \DB::beginTransaction();
        try{
            if (!empty($request->menu_customization)) {
                foreach ($request->menu_customization as $value) {
                    if (!(isset($value['id']) && !blank($value['id']))) {
                        if($value['selection_name'] != ""){
                            $menuCount = ItemSelection::where('customize_type_id', $value['customize_type_id'])->where('selection_price', $value['selection_price'])->where('selection_name', $value['selection_name'])->where('parent_item_id','0')->first();
                            if (empty($menuCount)) {
                                if($value['status'] == null){
                                    $status = '0';
                                }else{
                                    $status = $value['status'];
                                }
                                
                                $menu_item = ['customize_type_id' => $value['customize_type_id'],'selection_name' => $value['selection_name'],'selection_name_thai' => $value['selection_name_thai'], 'selection_price' => ($value['selection_price'] != "") ? $value['selection_price'] : 0, 'is_default' => (string)(isset($value['is_default']) ? $value['is_default'] : 0), 'status' => $status];
                                $add_menu_item = $this->customizationService->storeMenuItemSelection($menu_item);
                            }
                        }
                    }else{ 
                        $data = ['selection_name' => $value['selection_name'],'selection_name_thai' => $value['selection_name_thai'], 'selection_price' => ($value['selection_price'] != "") ? $value['selection_price'] : 0, 'is_default' => (string)(isset($value['is_default']) ? $value['is_default'] : 0), 'status' => isset($value['status']) ? $value['status'] : 0];
                        $add_menu_item = $this->customizationService->updateMenuItemSelection(['id' => $value['id']],$data);
                    }  
                    $check_cust_type = $this->customizationService->getCustomization(['id' => $value['customize_type_id']]);
                    if($check_cust_type){
                        $item_id = $check_cust_type->master_item_id;
                        MasterItem::where('id',$check_cust_type->master_item_id)->update(['is_customizable'=>'1']);

                        $menuItemId = MasterItem::where('id',$check_cust_type->master_item_id)->pluck('main_category_id')->first();  
                        $get_master_item_redis = $this->redisService->get(RedisKeysEnum::MASTER_ITEM->value.':'.$menuItemId);
                        if(!blank($get_master_item_redis)){            
                            $get_master_item = $this->masterItemService->getMasterItemByID($user,$client_id,$menuItemId,'',50);            
                            $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$menuItemId, $get_master_item);
                        }
                    }
                }
            }
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        
        
        \DB::commit();
        $this->resstoApiService->setMasterItems($item_id);
        $this->resstoApiService->setMenuItems();
        if($add_menu_item){
            return $this->responseService->response($add_menu_item,__('Customization Added Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function deleteItemCustomization(Request $request){
        $request->validate([
            'id' => 'required'
        ]);
        \DB::beginTransaction();
        try{ 
            $delete_item_customisation = $this->customizationService->deleteItemCustomization(['id' => $request->id]);
            
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($delete_item_customisation){
            return $this->responseService->response($delete_item_customisation,__('Customization Deleted Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function saveTemplate(Request $request){
        $request->validate([
            'template_name' => 'required'
        ]);
        \DB::beginTransaction();
        try{ 
            $customisation = $this->customizationService->getCustomization(['id' => $request->id]);
            if(!empty($customisation)){
                $edit_customisation = $this->customizationService->updateCustomization($request->id,['template_name' => $request->template_name['template_name'], 'is_template' => '1']);
            }
            
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($edit_customisation){
            return $this->responseService->response($edit_customisation,__('Template Save Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function addTemplate(Request $request){
        $request->validate([
            'cust_type_search' => 'required'
        ]);
        \DB::beginTransaction();
        try{ 
            $menu_id = base64_decode($request->master_item_id);
            if(!empty($request->cust_type_search)){
                foreach ($request->cust_type_search as $key_type => $value_type) {
                    $check_cust_type = $this->customizationService->getCustomization(['id' => $value_type]);
                    if($check_cust_type){
                        $check_cust = $this->customizationService->getCustomization(['id' => $value_type, 'master_item_id' => $menu_id]);
                        //if(empty($check_cust) == 0){
                            $data = ['type_name' => $check_cust_type->type_name,'type_name_thai' => $check_cust_type->type_name_thai,'master_item_id' => $menu_id, 'is_selection' => $check_cust_type->is_selection, 'is_min_selection' => $check_cust_type->is_min_selection, 'is_veg_non' => $check_cust_type->is_veg_non, 'status' => $check_cust_type->status];
                            $add_cust_type = $this->customizationService->storeCustomizationType($data);
                            $check_menu_selection = $this->customizationService->getItemSelection(['id' => $value_type]);
                            if (!empty($check_menu_selection)) {
                                foreach ($check_menu_selection as $key => $value) {

                                    $menu_item = ['customize_type_id' => $add_cust_type->id,'selection_name' => $value->selection_name,'selection_name_thai' => $value->selection_name_thai, 'selection_price' => $value->selection_price, 'is_selection' => $value->is_selection, 'is_min_selection' => $value->is_min_selection, 'is_default' => $value->is_default];
                                    $add_menu_item = $this->customizationService->storeMenuItemSelection($menu_item);

                                    $check_menu_item_selection = $this->customizationService->getItemSelection(['id' => $value_type, 'parent_item_id' => 1]);
                                    foreach ($check_menu_item_selection as $key1 => $value1) {
                                        $menu_item_selection = ['customize_type_id' => $add_cust_type->id,'selection_name' => $value->selection_name,'selection_name_thai' => $value->selection_name_thai, 'selection_price' => $value->selection_price, 'is_selection' => $value->is_selection, 'is_min_selection' => $value->is_min_selection, 'is_default' => $value->is_default, 'created_at' => date('Y-m-d H:i:s')];
                                        $add_menu_item = $this->customizationService->storeMenuItemSelection($menu_item_selection);
                                    }
                                }
                            }
                        //}
                    }
                }
            }
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        $this->resstoApiService->setMasterItems($menu_id);
        if($add_cust_type){
            return $this->responseService->response($add_cust_type,__('Customization Added Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function getTemplate(Request $request){
        $user = auth()->user();
        $menu_id = base64_decode($request->master_item_id);
        $get_item =  MasterItem::pluck('id');
        $search_cusine = $this->customizationService->getTemplate($get_item,$request->searchtext);
        if($search_cusine){
            return $this->responseService->response($search_cusine,__('Template List'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function deleteCustomizationType(Request $request){        
        $user = auth()->user();
        $client_id = $user->client_id;
        $request->validate([
            'id' => 'required'
        ]);
        \DB::beginTransaction();
        try{ 
            $customisation = $this->customizationService->getCustomization(['id' => $request->id]);
            $delete_customisation = $this->customizationService->deleteCustomization(['id' => $request->id]);            
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        
        $menuItemId = MasterItem::where('id',$request->master_item_id)->pluck('main_category_id')->first();
        // menu item add in redis        
        $get_master_item_redis = $this->redisService->get(RedisKeysEnum::MASTER_ITEM->value.':'.$menuItemId);
        if(!blank($get_master_item_redis)){            
            $get_master_item = $this->masterItemService->getMasterItemByID($user,$client_id,$menuItemId,'',50);            
            $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$menuItemId, $get_master_item);
        }
        \DB::commit();
        if($delete_customisation){
            $this->resstoApiService->setMasterItems($request->master_item_id);
            $this->resstoApiService->setMenuItems();
            return $this->responseService->response($delete_customisation,__('Customization Type Deleted Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function getSubCustomization(Request $request){
        $user = auth()->user();
        $id = base64_decode($request->id);
        $get_item_details = $this->customizationService->getItemSelectionById(['id' => $id]);
        $item = ItemSelection::with('item_customization')->where('parent_item_id',$id)->get();
        $get_cust = CustomizationType::where('id',$get_item_details->customize_type_id)->first();
        $res_menu_itm = $this->masterItemService->getItemByID($get_cust->master_item_id);
        $data['item'] = $item;
        $data['parent_item_id'] = (int)$id;
        $data['id'] = $get_cust->id;
        if($res_menu_itm->is_menu == '1'){
            $data['master_item_id'] = $res_menu_itm->id;
        }else{
            $data['master_item_id'] = $res_menu_itm->main_category_id;
        }
        $data['item_detail'] = $get_item_details;
        return $this->responseService->response($data,__('Sub Customization List'));  
    }

    public function addSubMenuSelection(Request $request){
        $user = auth()->user();
        $client_id = $user->client_id;
        $master_item_id = 0;
        \DB::beginTransaction();
        try{
            if (!empty($request->menu_customization)) {
                foreach ($request->menu_customization as $value) {
                    $master_item_id = base64_decode($value['item_Id']);
                    if (!(isset($value['id']) && !blank($value['id']))) {
                        if($value['selection_name'] != ""){
                            $menuCount = ItemSelection::where('customize_type_id', $value['customize_type_id'])->where('selection_price', $value['selection_price'])->where('selection_name', $value['selection_name'])->where('parent_item_id',$value['parent_item_id'])->get();
                            if (empty($menuCount) == 0) {
                                if($value['status'] == null){
                                    $status = '0';
                                }else{
                                    $status = $value['status'];
                                }
                                
                                $menu_item = ['customize_type_id' => $value['customize_type_id'],'selection_name' => $value['selection_name'],'selection_name_thai' => $value['selection_name_thai'], 'selection_price' => ($value['selection_price'] != "") ? $value['selection_price'] : 0, 'is_default' => (string)(isset($value['is_default']) ? $value['is_default'] : 0), 'status' => $status,'parent_item_id' => $value['parent_item_id']];
                                $add_menu_item = $this->customizationService->storeMenuItemSelection($menu_item);
                            }
                        }
                    }else{ 
                        $data = ['selection_name' => $value['selection_name'],'selection_name_thai' => $value['selection_name_thai'], 'selection_price' => ($value['selection_price'] != "") ? $value['selection_price'] : 0, 'is_default' => (string)(isset($value['is_default']) ? $value['is_default'] : 0), 'status' => isset($value['status']) ? $value['status'] : 0];
                        $add_menu_item = $this->customizationService->updateMenuItemSelection(['id' => $value['id']],$data);
                    }  
                }
            }
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        $this->resstoApiService->setMasterItems($master_item_id);
        if($add_menu_item){
            return $this->responseService->response($add_menu_item,__('Customization Added Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function minSelection(Request $request){

        $request->validate([
            'id' => 'required',
            'is_selection' => 'required'
        ]);
        \DB::beginTransaction();
        try{ 
            $update = $this->customizationService->updateMenuItemSelection(['id' => $request->id],['is_selection' => $request->is_selection]);
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($update){
            return $this->responseService->response($update,__('Item Saved'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);

    }

    public function maxSelection(Request $request){

        $request->validate([
            'id' => 'required',
            'is_min_selection' => 'required'
        ]);
        \DB::beginTransaction();
        try{ 
            $update = $this->customizationService->updateMenuItemSelection(['id' => $request->id],['is_min_selection' => $request->is_min_selection]);
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($update){
            return $this->responseService->response($update,__('Item Saved'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);

    }

}
