<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Services\CommonService;
use App\Http\Services\ResponseService;
use App\Http\Services\WebSettingService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class WebSettingController extends Controller
{
    protected $webSettingService,$responseService, $commonService,$user, $setting,$timezone, $client_id ;

    public function __construct(WebSettingService $webSettingService,ResponseService $responseService, CommonService $commonService)
    {
        $this->webSettingService = $webSettingService;
        $this->responseService = $responseService;
        $this->commonService = $commonService;
        $this->user = auth()->user();
        if($this->user){
            $this->client_id = auth()->user()->myid();
        }elseif($this->user = auth('partner')->user()){
            $this->client_id = $this->user->client_id;
        }
    }

    public function updateWebSetting(Request $request){
        // $request->validate([
        //     '*' => 'required'
        // ]);
        DB::beginTransaction();
        try{
            $fevicon_icon = '';
            if ($request->has('fevicon_icon') && !blank($request->fevicon_icon)) {
                if ($request->fevicon_icon != 'undefined' && $request->fevicon_icon != 'undefined') {
                    $request['fevicon_icon'] = $this->commonService->getMovedFile($request->fevicon_icon,'media/WebPageLayout/','Favicon');
                }else{
                    unset($request['fevicon_icon']);
                }
            }
            $webSetting = $this->webSettingService->creatOrUpdate($this->client_id,$request->except('_token'));
            if($webSetting['status']){
                DB::commit();
                return $this->responseService->response($webSetting['setting'],__($webSetting['message']));
            }else{
                DB::rollBack();
                return $this->responseService->response((object)[],__($webSetting['message']),101);
            }

        }catch(\Exception $e){
            DB::rollBack();
            Log::channel('client')->error('Web Setting update',[ 'code' => 101,'msg' => $e->getMessage(),]);
            return $this->responseService->response([],__('Something went wrong'),101);
        }
    }

    public function getWebSettings(){
        $settings = $this->webSettingService->getSetting($this->client_id);
        return $this->responseService->response($settings,__('Get Setting Successfully'));
    }

    public function deleteWebLogo(Request $request){
        $request->validate([
            'file'  =>  'required'
        ]);
        $this->commonService->deleteImage($request->file,'WebPageLayout');
        $this->webSettingService->removekey($this->client_id,'fevicon_icon');
        return $this->responseService->response(['deleted'  =>  true],__('Image deleted successfully'));

    }
}
