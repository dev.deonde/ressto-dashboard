<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Services\CategoryService;
use App\Http\Services\ResponseService;
use App\Models\MasterCategory;
use App\Http\Services\CommonService;

class CategoryController extends Controller
{
    public function __construct(CategoryService $categoryService,CommonService $commonService)
    {
        $this->categoryService = $categoryService;
        $this->commonService = $commonService;
        $this->responseService = new ResponseService;
    }

    public function getMasterCategories(Request $request){
        $user = auth()->user();

        $menu_id = $request->menu_id;

        $get_master_category = MasterCategory::where('client_id',$user->id)
        ->where(function ($query) use ($menu_id){
            if($menu_id != ''){
                $query->where('menu_id',$menu_id);
            }
        })->orderBy('reorder_data', 'asc')->get();
        
        if(!empty($get_master_category)){  
            return $this->responseService->response($get_master_category,__('Master Category Listing'));
        }else{
            return $this->responseService->response([],__('No master category found'),101); 
        }
    }


    public function addMenuCategory(Request $request)
    {

        $user = auth()->user();
    
        $request->validate([
            'name' => 'required'
        ]);
        \DB::beginTransaction();
        try{    
            $image = ''; 
            $status = (isset($request->status) ? $request->status : '1');
            $color_code = (isset($request->color_code) ? $request->color_code: '#000');
            $text_color_code = (isset($request->text_color_code) ? $request->text_color_code: '#000');
            if($request->has('image') && !blank($request->image))
            {
                $image = $this->commonService->getMovedFile($request->image,'media/Master_Category/' ,'Category');
            }
            $saveData = $request->only('name','name_another_lang') + ['client_id' => $user->id, 'color_code' => $color_code , 'text_color_code' => $text_color_code , 'status' => $status, 'menu_id' => isset($request->menu_id) ? $request->menu_id : 0,'image' => $image];
            $category = $this->categoryService->storeCategory($saveData);
            \DB::commit();
            if($category){
                return $this->responseService->response($category,__('Category Added Successfully'));
            }
        }
        catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        return $this->responseService->response([],__('Something went wrong'),101); 
    }

    public function getMasterCategoryById(Request $request)
    {
        $user = auth()->user();
        $get_master_category = $this->categoryService->getMasterCategory($user,$request->id);

        if($get_master_category)
        {
            return $this->responseService->response($get_master_category,__('Master category listing'));
        }else{
            return $this->responseService->response([],__('No master category found '),101);
        }
    }

    public function updateMasterCategory(Request $request) {
        $user = auth()->user();
        $get_master_category = $this->categoryService->getMasterCategory($user,$request->id);
        $request->validate([
            'name' => 'required'
        ]);
        \DB::beginTransaction();
        try{
            $name_another_lang = (isset($request->name_another_lang) ? $request->name_another_lang : '');
            $status = (isset($request->status) ? $request->status : '1');
            $color_code = (isset($request->color_code) ? $request->color_code: '#000');
            $text_color_code = (isset($request->text_color_code) ? $request->text_color_code: '#000');
            $data = ['name' => $request->name, 'name_another_lang'  => $name_another_lang ,'client_id' => $user->id, 'color_code' => $color_code , 'text_color_code' => $text_color_code , 'status' => $status,'menu_id' => isset($request->menu_id) ? $request->menu_id : $get_master_category->menu_id];
            if($request->has('image') && !blank($request->image)){
                $data['image'] = $this->commonService->getMovedFile($request->image,'media/Master_Category/' ,'Category');
            }
            $master_category = $this->categoryService->editMasterCategory($request->id,$data);
            \DB::commit();
            if($master_category){
                return $this->responseService->response($master_category,__('Master category update successfully'));
            }
        }
        catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function deleteMasterCategory(Request $request)
    {
        $user = auth()->user();
        $category = $this->categoryService->delete($request->id);
        if($category){
            return $this->responseService->response([],__('Master category delete successfully'));
        }else{
            return $this->responseService->response([],__('Something went wrong'),101);
        }
    }

    public function deleteCategoryImage(Request $request)
    {
        $request->validate([
            'file'  =>  'required'
        ]);
        $this->commonService->deleteImage($request->file,'Category_image');
        $restaurant_menu = $this->categoryService->getMasterCategoryWhere([['image','=',$request->file]])->first();
        $restaurant_menu->image = "";
        $restaurant_menu->save();
        return $this->responseService->response(['deleted'  =>  true],__('Category Image deleted successfully'));
    }

}
