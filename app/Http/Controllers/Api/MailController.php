<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Mail;
use Illuminate\Http\Request; 
use App\Http\Services\MailService;
use App\Http\Services\ResponseService;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Customer;
use App\Models\WebSetting;
use Illuminate\Support\Str;



class MailController extends Controller
{
    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
        
        $this->responseService = new ResponseService;
    }

    public function getMailList(Request $request){                
        $user = auth()->user();
        $mail_listing =  $this->mailService->mailList($user,$request->search,$request->status,$request->sortBy,$request->orderBy,50);
        if($mail_listing){
            return $this->responseService->response($mail_listing,__('Mail listing'));
        }else{
            return $this->responseService->response([],__('No mail found'),101);
        }
    }

    public function addMailTemplate(Request $request) {
        $client_id = auth()->user()->myid();
        $request->validate([
            'subject' => ['required',
            function ($attribute, $value, $fail)use($client_id) {
                if (Mail::where('client_id',$client_id)->where('subject',$value)->count() > 0) {
                    $fail('The '.$attribute.' is already exists.');
                }
            },],
            'content' => 'required'
        ]);
        \DB::beginTransaction();
        try{
            $data = $request->only('subject')+['client_id' => $client_id,'content' => htmlentities($request->content),'slug'=>Str::slug($request->subject, '-'),'status'=>'1'];
            $mail_add =  $this->mailService->storeMailTemplate($data);
            \DB::commit();
            if($mail_add){
                return $this->responseService->response($mail_add,__('Mail Added Successfully.'));
            }
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        return $this->responseService->response([],__('Something went wrong'),101);    
    }

    public function viewMail(Request $request){
        $user = auth()->user();
        $view_mail =  $this->mailService->viewMail($user,base64_decode($request->id));
        if($view_mail){
            $view_mail->content = html_entity_decode($view_mail->content);
            return $this->responseService->response($view_mail,__('View Mail'));
        }else{
            return $this->responseService->response([],__('No mail found'),101);
        }
    }

    public function getMailById(Request $request) {
        $mail = $this->mailService->getMailById(['id' => base64_decode($request->id)]);
        if($mail){
            $mail->content = html_entity_decode($mail->content);
            return $this->responseService->response($mail,__('Page detail'));
        }else{
            return $this->responseService->response([],__('Something went wrong'),101);
        }
    }

    public function editMailTemplate(Request $request){
        $client_id = auth()->user()->myid();
        $request->validate([
            'subject' => ['required',
            function ($attribute, $value, $fail)use($request,$client_id) {
                if (Mail::where('id','!=',$request->id)->where('client_id',$client_id)->where('subject',$value)->count() > 0) {
                    $fail('The '.$attribute.' is already exists.');
                }
            }],
            'content' => 'required'
        ]);
        \DB::beginTransaction();
        try{
            $data = $request->only('subject')+['client_id' => $client_id,'content' => htmlentities($request->content),'status'=> $request->status];
            $mail_update =  $this->mailService->updateMailTemplate($request->id,$data);
            \DB::commit();
            if($mail_update){
                return $this->responseService->response($mail_update,__('Mail Update Successfully.'));
            }
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        return $this->responseService->response([],__('Something went wrong'),101);    
    }


}