<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Services\CommonService;
use App\Http\Services\OrderService;
use App\Http\Services\ResponseService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $orderService,$responseService,$user, $setting,$timezone, $client_id,$commonService ;

    public function __construct(OrderService $orderService,ResponseService $responseService, CommonService $commonService)
    {
        ini_set('memory_limit', '-1');
        $this->orderService = $orderService;
        $this->responseService = $responseService;
        $this->commonService = $commonService;
        $this->user = auth()->user();
        if($this->user){
            $this->client_id = (blank(auth()->user()->parent_client_id) ? auth()->user()->id : auth()->user()->parent_client_id);
            $this->setting = auth()->user()->setting;
            $this->timezone = $this->commonService->getSetting('timezone', 'client');
        }elseif($this->user = auth('partner')->user()){
            $this->client_id = $this->user->vendor_id;
            $this->setting = $this->user->vendor->setting;
            $this->timezone = $this->commonService->getSetting('timezone', 'partner');
        }
        $this->user_id = 0;
        if(auth('partner')->check()){
            $this->user_id = $this->user->user_id;
        }
    }

    public function getOrders(Request $request){
        $data = $this->orderService->getOrders($this->client_id,$this->timezone, $request->all(), true, $this->user_id);
        return $this->responseService->response($data, __('Success'));
    }

    public function getOrdersCount(Request $request){
        // \DB::enableQueryLog();
        $data = $this->orderService->getOrdersCount($this->client_id,$this->timezone, $request->all(),$this->user_id);
        return $this->responseService->response($data, __('Success'));
    }

    public function getNewOrders(Request $request){

        $data = $this->orderService->getNewOrders($this->client_id,$this->timezone,$this->user_id);

        return $this->responseService->response($data, __('Success'));
    }

    public function exportOrders(Request $request){

        $data = $this->orderService->getExportOrders($this->client_id,$this->timezone, $request->all(), false , $this->user_id);
        return $this->responseService->response($data, __('Success'));
    }

    public function getOrder(Request $request){
        $data = $this->orderService->getOrder($this->client_id,$this->timezone, base64_decode($request->order_id),
            [
                'reddemedCouponOrder:order_id,original_price,discount_price,coupon_id,cashback',
                'reddemedCouponOrder.coupon:id,coupon_code,promo_code_type',
                'restaurant',
                'user',
                'driver',
                // 'orderItem',
                'orderItem.menuItemSelections',
                'orderItem.masterItem',
                // 'orderItem.menuItem',
                // 'orderItem.menuItemWithTrashed',
                'access:id,client_name',
                // 'dunzoDriver','loadShareDriver'
            ]
        );
        if(!blank($data)){
            return $this->responseService->response($data, __('Success'));
        }else{
            return $this->responseService->response([], __('Order Not Found'),101);
        }
    }


    public function cancelOrder(Request $request){
        $parameter = $request->all();
        if (auth()->guard('vendor')->user()) {
            $vendor = auth()->user();
            $vendor_id = $vendor->myid();
        } else if (auth()->guard('partner')->user()) {
            $vendor_id = auth()->guard('partner')->user()->vendor_id;
        }

        $langauge = "en";
        $orderId = $request->orderId;

        try{
            $res = $this->orderService->cancelOrder($orderId, $parameter['is_refundable'],$parameter['cancelled_desc'], $langauge, $vendor_id);
            if ($res['code'] == 200) {
                return $this->responseService->response([], __('Order cancelled successfully'));
            } else {
                return $this->responseService->response([], (($res['msg'] != null && $res['msg'] != '') ? $res['msg'] : "Something went wrong please try again."), 101);
            }
        }catch (\Exception $e) {
            $msg=$e->getMessage();
            throw $e;
            return $this->responseService->response([], $msg, 101);
        }
    }

    public function assignDriver(Request $request) {
        $vendor = auth()->user();
        $vendor_id = $vendor->myid();

        \DB::beginTransaction();
        try {
            $is_dunzo = 'no';
            if (isset($request->is_dunzo) && !blank($request->is_dunzo)) {
                $is_dunzo = $request->is_dunzo;
            }
            $dunzoTask = $this->orderService->getOrderDunzoTask(base64_decode($request->order_id));
            if (!blank($dunzoTask)) {
                return $this->responseService->response([], __('Order already assign to dunzo'),101);
            }
            if ($is_dunzo == 'yes') {
                $response = $this->orderService->sendToDunzo($vendor_id, base64_decode($request->order_id));
                if ($response['code'] == 200) {
                    \DB::commit();
                    return $this->responseService->response([], __($response['msg']));
                } else {
                    \DB::commit();
                    return $this->responseService->response([], __($response['msg']),101);
                }
            } else {
                $orderStatus = $this->orderService->getOrderStatus(base64_decode($request->order_id));
                if ($orderStatus == 'Delivered' || $orderStatus == 'Cancelled') {
                    $errorMsg = ($orderStatus == 'Delivered' ? "Order already delivered so you can't assign driver" : ($orderStatus == 'Cancelled' ? "Order already cancelled so you can't assign driver" : ""));
                    return $this->responseService->response([], __($errorMsg),101);
                }
                $this->orderService->assignDriver(auth()->user(),base64_decode($request->order_id), [$request->driver_id]);
            }
        } catch(\Exception $e) {
            \DB::rollBack();
            return $this->responseService->response([], __('Order notification failed'),101);
        }
        \DB::commit();
        return $this->responseService->response([], __('Order notification send successfully'));
    }

    public function orderStatusChange(Request $request,$url){
        $api_url = config('constant.api_url');
        $i_url = "{$api_url}{$url}";
        $response = Http::post($i_url, $request->all());
        return response($response->json());
    }

    public function deliverOrder(Request $request) {
        if (auth()->guard('vendor')->user()) {
            $vendor = auth()->user();
            $vendor_id = $vendor->myid();
        } else if (auth()->guard('partner')->user()) {
            $vendor_id = auth()->guard('partner')->user()->vendor_id;
        }
        $langauge = "en";
        $orderId = $request->orderId;

        try {
            $res = $this->orderService->deliverOrder($request->orderId, $request->delivered_note, $langauge, $vendor_id);
            if ($res['code'] == 200) {
                return $this->responseService->response([], __('Order delivered successfully'));
            } else {
                return $this->responseService->response([], (($res['msg'] != null && $res['msg'] != '') ? $res['msg'] : "Something went wrong please try again."), 101);
            }
        } catch (\Exception $e) {
            $msg=$e->getMessage();
            throw $e;
            return $this->responseService->response([], $msg, 101);
        }
    }

    public function changeToCod(Request $request) {
        if (auth()->guard('vendor')->user()) {
            $vendor = auth()->user();
            $vendor_id = $vendor->myid();
            $user_id = $vendor_id;
        } else if (auth()->guard('partner')->user()) {
            $vendor_id = auth()->guard('partner')->user()->vendor_id;
            $user_id = $vendor_id;
        }
        $langauge = "en";
        $orderId = $request->orderId;

        try{
            $res = $this->orderService->changeToCod($orderId, $request->changeto_cod_note, $langauge, $vendor_id, $user_id);
            if ($res['code'] == 200) {
                return $this->responseService->response([], __('Order change to cod successfully'));
            } else {
                return $this->responseService->response([], $res['msg'], 101);
            }
        }catch (\Exception $e) {
            $msg=$e->getMessage();
            throw $e;
            return $this->responseService->response([], $msg, 101);
        }
    }
}
