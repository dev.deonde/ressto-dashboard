<?php

namespace App\Http\Controllers\Api;

use App\Models\Page;
use Illuminate\Http\Request; 
use App\Http\Services\PageService;
use App\Http\Controllers\Controller;
use App\Http\Services\ResponseService;
use App\Models\VendorSetting;
use App\Http\Services\VendorSettingService;

class StaticPageController extends Controller
{
  public function __construct(PageService $pageService)
  {
    $this->pageService = $pageService;
    $this->responseService = new ResponseService;
    $this->vendorSettingService = new VendorSettingService;
  }

  public function getContentPage(Request $request) {
    $clientId = auth()->user()->myid();
    $pageListing =  $this->pageService->pageList($clientId,$request->search,$request->sortBy,$request->orderBy,50);
    if($pageListing){
        return $this->responseService->response($pageListing,__('Page listing'));
    }else{
        return $this->responseService->response([],__('No page found'),101);
    }
  }

  public function addContentPage(Request $request){
    $clientId = auth()->user()->myid();
    $request->validate([
        'page_name' => ['required',
        function ($attribute, $value, $fail)use($clientId) {
          if (Page::where('client_id',$clientId)->where('page_name',$value)->count() > 0) {
            $fail('The '.$attribute.' is already exists.');
          }
        }]
    ]);
    \DB::beginTransaction();
    try{
        $content_type = ((isset($request->content_type) && $request->content_type == '1') ? '1':'0');
        $content_link = (isset($request->content_link) ? $request->content_link : '');
        $appName = \App\Helper::settings($clientId,'app_name');
        $data = $request->only('page_name')+['client_id'=>$clientId,'content_type' => $content_type, 'content_link' => $content_link, 'page_content' => htmlentities($request->page_content), 'display_in' => $request->display_in, 'identifier' => $request->identifier];
        $page_add =  $this->pageService->storeContentPage($data);
        if($page_add){
          $this->pageService->updateOldPageIdentifier($page_add->id,$clientId,$page_add->identifier);
        }        
        $appName = \App\Helper::settings($clientId,'app_name');
        if($request->content_type == '1'){
          $url = url('page/'.$appName.'/'.strtolower(str_replace(' ','-',$request->page_name)));
          $page_name = strtolower(str_replace(' ','_',$request->page_name));
          $key = $page_name.'_url';
          $value =  $request->$page_name ? $request->$page_name : $url;
        }else {
          $link = $request->content_link;
          $page_name = strtolower(str_replace(' ','_',$request->page_name));
          $key = $page_name.'_url';
          $value = $request->$link ? $request->$link : $link;
        }
        $vendor_settings = $this->vendorSettingService->clientVendorSettings($key,$value);
        \DB::commit();
        if($page_add){
          return $this->responseService->response($page_add,__('Content Page Added Successfully.'));
        }
    }catch(\Exception $e){
        \DB::rollBack();
        throw $e;
    }
    return $this->responseService->response([],__('Something went wrong'),101);    
  }

  public function getContentPageById(Request $request) {
    $content = $this->pageService->getContentPage(['id' => base64_decode($request->id)]);
    if($content){      
      if(isset($content->display_in) && ($content->display_in == null || $content->display_in == 'null')) {
        $content->display_in = json_decode('[]');
      } else {
        $content->display_in = (isset($content->display_in)) ? json_decode($content->display_in) : [];
      }
      $content->page_content = htmlspecialchars_decode($content->page_content);
      return $this->responseService->response($content,__('Page detail'));
    }else{
      return $this->responseService->response([],__('Something went wrong'),101);
    }
  }

  public function editContentPage(Request $request) {    
    $clientId = auth()->user()->myid();
    $request->validate([
        'page_name' => ['required',
        function ($attribute, $value, $fail)use($clientId,$request) {
            if (Page::where('client_id',$clientId)->where('id','!=',$request->id)->where('page_name',$value)->count() > 0) {
                $fail('The '.$attribute.' is already exists.');
            }
        },]
    ]);
    \DB::beginTransaction();
    try{
        $content_type = (isset($request->content_type) ? $request->content_type : '1');
        $content_link = (isset($request->content_link) ? $request->content_link : '');
        $data = $request->only('page_name')+['client_id' => $clientId,'content_type' => $content_type, 'content_link' => $content_link, 'page_content' => htmlentities($request->page_content),'display_in' => $request->display_in, 'identifier' => $request->identifier];
        $page_update =  $this->pageService->updateContentPage($request->id,$data);
        if($page_update){
          $this->pageService->updateOldPageIdentifier($page_update->id,$clientId,$page_update->identifier);
        }
        $appName = \App\Helper::settings($clientId,'app_name');
        $url = url('page/'.$appName.'/'.strtolower(str_replace(' ','-',$request->page_name)));
        $page_name = strtolower(str_replace(' ','_',$request->page_name));
        if($request->content_type == '1'){
          $url = url('page/'.$appName.'/'.strtolower(str_replace(' ','-',$request->page_name)));
          $page_name = strtolower(str_replace(' ','_',$request->page_name));            
          $key = $page_name.'_url';
          $value =  $request->$page_name ? $request->$page_name : $url;
        }else {
          $link = $request->content_link;
          $page_name = strtolower(str_replace(' ','_',$request->page_name));
          $key = $page_name.'_url';
          $value = $request->$link ? $request->$link : $link;
        }
        $vendor_settings = $this->vendorSettingService->clientVendorSettings($key,$value);
        \DB::commit();
        if($page_update){
          return $this->responseService->response($page_update,__('Content Page Updated Successfully.'));
        }
    }catch(\Exception $e){
        \DB::rollBack();
        throw $e;
    }
    return $this->responseService->response([],__('Something went wrong'),101);    
  }

  public function deleteContentPage(Request $request){
    $getPage = $this->pageService->getContentPage(['id' => base64_decode($request->id)]);
    $pageName = strtolower(str_replace(' ','_',$getPage->page_name));
    $keyName = $pageName.'_url';
    if($getPage){
      $delete = $this->pageService->deleteContentPage(['id' => base64_decode($request->id)]);      
      if($delete){
        $vendor_settings = $this->vendorSettingService->deleteVendorSettingKey($keyName);
        return $this->responseService->response($delete,__('Content Page Deleted Successfully.'));
      }
      return $this->responseService->response([],__('Something went wrong'),101);
    }
  }
 
}
?>