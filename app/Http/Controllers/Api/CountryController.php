<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Support\Facades\DB;

class CountryController extends Controller
{
    protected $client_id;
    public function __construct(){
        $this->client_id = auth()->user()->myid();
    }
    public function storeCountry($country_name=''){
        if(!blank($country_name)){
            $getCountry = Country::updateOrCreate(
                [
                    'country_name' => $country_name,
                    'client_id' => $this->client_id
                ],
                [
                    'country_name' => $country_name,
                    'client_id' => $this->client_id
                ]
            );
            return $getCountry->id;
        }
    }
}
