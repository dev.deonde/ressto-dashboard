<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Services\OutletService;
use App\Http\Services\ResponseService;
use App\Http\Services\RestaurantService;
use App\Models\Cuisine;
use App\Models\RestaurantCuisine;
use App\Models\RestaurantSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\WebSetting;
use App\Models\Restaurant;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class OutletController extends Controller
{
    protected $restaurant_id, $restaurantService, $responseService, $outletService, $countryController, $stateController, $cityController;

    public function __construct(
        RestaurantService $restaurantService, 
        ResponseService $responseService, 
        OutletService $outletService,
        CountryController $countryController,
        StateController $stateController,
        CityController $cityController
        ){
        $this->restaurantService = $restaurantService;
        $this->responseService = $responseService ;
        $this->outletService = $outletService ;
        $this->countryController = $countryController;
        $this->countryController = $countryController;
        $this->stateController = $stateController;
        $this->cityController = $cityController;
        if(!blank($this->restaurantService->getRestaurant(auth()->user()->myid()))){
            $this->restaurant_id = $this->restaurantService->getRestaurant(auth()->user()->myid())->id;
        }else{
            return $this->responseService->response([], __('Restaurant Not Found'),101);
        }
    }
    public function getRestaurant(){
        $restaurant = $this->restaurantService->getRestaurant(auth()->user()->myid());
        if(!blank($restaurant)){
            return $this->responseService->response($restaurant, __('Restaurant get successfully'));
        }else{
            return $this->responseService->response([], __('Restaurant Not Found'),101);

        }
    }

    public function updatedtRestaurant(Request $request){
        $request->validate([
            'id' => 'required|exists:restaurant,id',
            'name' => 'required',
            'contact_number' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'address' => 'required',
            'pin_code' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
        DB::beginTransaction();
        try{
            $countryID = $this->countryController->storeCountry($request->country);
            $stateID = $this->stateController->storeState($countryID, $request->state);
            $cityID = $this->cityController->storeCity($stateID, $request->city);
            if(blank($countryID) && blank($stateID) && blank($cityID)){
                DB::rollBack();
                return $this->responseService->response([], __('Somthing Went Wrong'), 101);
            }
            $data = [
                'name' => $request->name,
                'contact_number' => $request->contact_number,
                'address' => $request->address,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'pin_code' => $request->pin_code,
                'country' => $countryID,
                'state' => $stateID,
                'city' => $cityID,
            ];
            $restaurantUpdated = $this->restaurantService->updatedClient([['id',$request->id]], $data);
            if($restaurantUpdated){
                DB::commit();
                return $this->responseService->response([], __('Restaurant Updated Successfully'));
            }else{
                DB::rollBack();
                return $this->responseService->response([], __('Somthing Went Wrong'), 101);
            }
        }catch(\Exception $e){
            DB::rollBack();
            Log::channel('client')->error('Restaurant Updated', [$e->getMessage()]);
            return $this->responseService->response([], __('Somthing Went Wrong'), 101);
        }
    }

    public function getRestaurantCuisines(){
        $cuisines = Cuisine::where('client_id',auth()->user()->myid())->select('id', 'client_id', 'cuisine_name', 'cuisine_description')->get();
        $selectedcuisines = RestaurantCuisine::where('restaurant_id',$this->restaurant_id)->select( 'cuisine_id')->get();
        return $this->responseService->response(['cuisines' => $cuisines, 'selectedcuisines' => $selectedcuisines], __('Restaurant cuisines get successfully'));
    }

    public function updatedRestaurantCuisines(Request $request){
        $restaurantCuisines = json_decode($request->data);
        $data = $this->outletService->updatedRestaurantCuisines($restaurantCuisines, $this->restaurant_id);
        return $this->responseService->response([], __('Restaurant Cuisines Updated Successfully'));
    }

    public function getServiceOffering(){
        $serviceOffering = $this->restaurantService->getServiceOffering($this->restaurant_id);
        if($serviceOffering){
            return $this->responseService->response($serviceOffering,__('Service Offering get Successfully'));
        }
    }

    public function saveServiceOffering(Request $request){
        DB::beginTransaction();
        try{
            $data = $this->restaurantService->storeAndUpdatedRestaurantHours($this->restaurant_id, $request->slot, $request->restaurantSetting);
            DB::commit();
            return $this->responseService->response([],__('Service Offering Updated Successfully'));
        }catch(\Exception $e){
            DB::rollBack();
            Log::channel('client')->info('Resturant hour Updated',[ 'code' => 101,'msg' => $e->getMessage(),]);
            return $this->responseService->response([],__('Something went wrong'),101);
        }
    }

    public function taxSettingStoreUpdat(Request $request){
        $request->validate([
            'taxSetting' => 'required'
        ]);
        DB::beginTransaction();
        try{
            $taxSetting = $this->restaurantService->taxSettingStoreUpdat($this->restaurant_id, json_decode($request->taxSetting), 'taxSetting');
            if(!blank($taxSetting )){
                $data['taxSetting']=json_decode($taxSetting->setting_values);
                DB::commit();
                return $this->responseService->response($data,__('Tax Updated Successfully'));
            }else{
                DB::rollBack();
                Log::channel('client')->info('Resturant hour Updated',[ 'code' => 101,'msg' =>'updated Error',]);
                return $this->responseService->response([],__('Something went wrong'),101);
            }
        }catch(\Exception $e){
            DB::rollBack();
            Log::channel('client')->info('Resturant hour Updated',[ 'code' => 101,'msg' => $e->getMessage(),]);
            return $this->responseService->response([],__('Something went wrong'),101);
        }
    }

    public function getTaxSetting(){
        $taxSetting = $this->restaurantService->getRestaurantSettingWithKey($this->restaurant_id, 'taxSetting');
        $data['taxSetting']= !blank($taxSetting) ? json_decode($taxSetting[0]->setting_values): (object)[] ;
        return $this->responseService->response($data,__('Tax Get Successfully'));
    }

    public function otherSettingStoreUpdate(Request $request){
        $request->validate([
            'minimum_order_value' => 'required',
            'cost_for_two_person' => 'required',
            'packaging_charge' => 'required',
            'convince_charge' => 'required',
        ]);
        DB::beginTransaction();
        try{
            $outherSetting = $this->restaurantService->otherSettingStoreUpdate($this->restaurant_id, $request->all());
            DB::commit();
            return $this->responseService->response($outherSetting,__('Other Setting Updated Successfully'));
        }catch(\Exception $e){
            DB::rollBack();
            Log::channel('client')->info('Resturant hour Updated',[ 'code' => 101,'msg' => $e->getMessage(),]);
            return $this->responseService->response([],__('Something went wrong'),101);
        }

    }

    public function getOtherSetting(){
        $otherSetting = $this->restaurantService->getOtherSetting($this->restaurant_id);
        return $this->responseService->response($otherSetting,__('Tax Get Successfully'));
    }

    public function getQrCode(Request $request) {
        $user = [];
        $client_id = 0;
        $webordername = '';
        if (auth('partner')->check()) {
            $client_id = auth('partner')->user()->client_id;
            $user = auth('partner')->user();
        } else {
            $client_id = auth()->user()->parent_vendor_id?auth()->user()->parent_vendor_id:auth()->id();
            $user = auth()->user();
        }        
        $webordername = \App\Helper::settings($client_id,'website_url');
        $restaurant = Restaurant::where('id',base64_decode($request->id))->first();        
        $restuarnt_id = base64_encode($restaurant->id);
		$feedback_url = '';
        $url = '';
        if ($webordername != '') {
            $url = 'https://'.$webordername.'/qr/'.$restuarnt_id.'';
            $feedback_url = 'https://'.$webordername.'/feedback/'.$restuarnt_id.'';
        } else {
            $url = $restaurant->name;
        }        
        \DB::beginTransaction();
        try { 
            if(!\File::exists(public_path('media/Restaurant_Qr_Code/'))){
                \File::makeDirectory(public_path('media/Restaurant_Qr_Code/'),0777,true.true);
            }
            if(!\File::exists(public_path('media/Restaurant_Qr_Code/restaurant_'.$restuarnt_id.'.png'))){
                $qr_code = \QrCode::size(500)->format('png')->generate($url, public_path('media/Restaurant_Qr_Code/restaurant_'.$restuarnt_id.'.png'));
            }
            $path = config('constant.Base_V').'media/Restaurant_Qr_Code/restaurant_'.$restuarnt_id.'.png';
            if(!\File::exists(public_path('media/Feedback_Qr_Code/'))){
                \File::makeDirectory(public_path('media/Feedback_Qr_Code/'),0777,true.true);
            }            
            if(!\File::exists(public_path('media/Feedback_Qr_Code/feedback_'.$restuarnt_id.'.png')) && $feedback_url != ''){                
                $feedback_qr_code = \QrCode::size(500)->format('png')->generate($feedback_url, public_path('media/Feedback_Qr_Code/feedback_'.$restuarnt_id.'.png'));
            }
            $feedback_path = config('constant.Base_V').'media/Feedback_Qr_Code/feedback_'.$restuarnt_id.'.png';
        } catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        return $this->responseService->response([
            'path' => $path,
            'feedback' => $feedback_path
        ],__('Qr code generate successfully'));
    }

}
