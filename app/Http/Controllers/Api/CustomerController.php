<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Services\CommonService;
use App\Http\Services\CustomerService;
use App\Http\Services\ResponseService;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    protected $customerService, $responseService, $commonService;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
        $this->responseService = new ResponseService;
        $this->commonService = new CommonService();
                ini_set('memory_limit', '-1');
    }

    public function getCustomer(Request $request){
        
        $user = auth()->user();
        $customer_listing = $this->customerService->customerListing($user, $request->search, $request->status, $request->sortBy, $request->orderBy, 50);
        if ($customer_listing) {
            return $this->responseService->response($customer_listing, __('Customer Listing'));
        } else {
            return $this->responseService->response([], __('No Customer'), 101);
        }
    }
    public function customerExportCsv(Request $request){

        $user = auth()->user();
        $customer_export = $this->customerService->exportToCsv($user, $request->status, $request->sortBy, $request->orderBy);
        if($customer_export){
            return $this->responseService->response($customer_export, __('Customer Listing'));
        }else{
            return $this->responseService->response([], __('No Customer'), 101);
        }
    }

    public function getCustomerProfile(Request $request){
        $customer = $this->customerService->customerDetail($request->user_id);
        // $partner = User::where('vendor_id',$customer->vendor_id)->first();
        // $this->authorize('parter_update',[Vendor::class,$partner]);
        if($customer){
            return $this->responseService->response($customer, __('Customer Detail'));
        }else{
            return $this->responseService->response([], __('No Customer'), 101);
        }
    }

    public function updateCustomerProfile(Request $request){

        $customers = Customer::where('client_id',auth()->user()->id)
        ->where('mobile_number',$request->mobile_number)
        ->whereNot('id',$request->id)
        ->get();
        if(!blank($customers)){
            return $this->responseService->response([], __('Mobile Number Already Exists'), 101);
        }

        $customer_update = $this->customerService->updateCustomer($request->id,[
            'user_name' => $request->user_name, 
            'last_name' => $request->last_name, 
            'email' => $request->email, 
            'mobile_number' => $request->mobile_number,
            'status' => $request->status,
            'is_cod_enable' => $request->is_cod_enable,
            'wallet_status' =>     $request->wallet_status,
            'wallet_recharge_limit' =>     $request->wallet_recharge_limit,
            'maximum_wallet_amount' =>     $request->maximum_wallet_amount,
            'customise' =>     $request->customise,
        ]);
        // $partner = User::where('vendor_id',auth()->user()->vendor_id)->first();
        // $this->authorize('parter_update',[Vendor::class,$partner]);
        $customer = Customer::where('id',$request->id)->first();
        if($customer){
            \App\Helper::logactivity('Updated customer '.$customer->user_name. ' (' .$customer->email. ') .');
            return $this->responseService->response($customer, __('Profile Updated Successfully'));
        }else{
            return $this->responseService->response([], __('Somthing Went Wrong'), 101);
        }
        
    }

    public function getCustomerOrder(Request $request){

        $order = $this->customerService->orderListing($request->user_id, $request->order_status, $request->search, $request->sortBy, $request->orderBy, 50);
        if($order){
            return $this->responseService->response($order, __('Order Listing'));
        }else{
            return $this->responseService->response([], __('Somthing Went Wrong'), 101);
        }
        
    }

    public function ChangeCustPassword(Request $request)
    {
        $get_customer = $this->customerService->customerDetail($request->user_id);
        if($get_customer){
            try{
                $update_cust_password = Customer::where('id', $get_customer->id)->first();
                $update_cust_password->password = Hash::make($request->new_password);
                if($update_cust_password->save()){
                    \App\Helper::logactivity('Updated customer password '.$get_customer->user_name. ' (' .$get_customer->email. ') .');
                    return $this->responseService->response([],__('Password successfully changed'));
                }else{
                    return $this->responseService->response([],__('Something went wrong'),101);
                }

            }catch(\Exception $e){
                \Log::error('customer chanage password error ',[$e->getMessage()]);
                return $this->responseService->response([],__('Something went wrong'),101);
            }
        }else{
            return $this->responseService->response([],__('Something went wrong'),101);
        }
    }
}
