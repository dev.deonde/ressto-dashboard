<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\State;

class StateController extends Controller
{
    protected $client_id;
    public function __construct(){
        $this->client_id = auth()->user()->myid();
    }
    public function storeState($country_id = '', $state_name = ''){
        if(!blank($country_id) && !blank($state_name)){
            $getState = State::updateOrCreate(
                [
                    'client_id' => $this->client_id,
                    'country_id' => $country_id,
                    'state_name' => $state_name
                ],
                [
                    'client_id' => $this->client_id,
                    'country_id' => $country_id,
                    'state_name' => $state_name
                ]
            );
            return $getState->id;
        }
    }
}
