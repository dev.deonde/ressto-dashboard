<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Services\PaymentGatewayService;
use App\Http\Services\ResponseService;
use App\Models\PaymentGateway;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PaymentGatewayController extends Controller
{
    public function __construct(ResponseService $responseService, PaymentGatewayService $paymentGatewayService){
        $this->responseService = $responseService;
        $this->paymentGatewayService = $paymentGatewayService;
    }
    public function getPaymentgateways(){
        return $this->responseService->response(PaymentGateway::with('clientGatewey')->get(),__('Payment Gateway Get Successfully'));
    }

    public function storeClientPaymentGateway(Request $request){
        $request->validate([
            'gatewayId' => 'required|exists:payment_gateways,id',
            'keys' => 'required'
        ]);
        DB::beginTransaction();
        try{
            $data = $this->paymentGatewayService->storeClientPaymentGateway($request->gatewayId, json_decode($request->keys));
            if($data){
                DB::commit();
                return $this->responseService->response((object)[],__('Payment Gateway Active Successfully'));
            }else{
                DB::rollBack();
                return $this->responseService->response((object)[],__('Please Enter Keys'),101);
            }
        }catch(Exception $e){
            DB::rollBack();
            Log::channel('client')->error('Client Payment Gateway Error',[ 'code' => 101,'msg' => $e->getMessage(),]);
            return $this->responseService->response((object)[],__('Something went wrong'),101);
        }
    }

    public function getClientPaymentGateway(Request $request){
        $request->validate([
            'gatewayId' => 'required|exists:payment_gateways,id',
        ]);
        return $this->responseService->response($this->paymentGatewayService->getClientPaymentGateway($request->gatewayId),__('Client Payment Gateway Get Successfully'));
    }

    public function clientGateweyDeactive(Request $request){
        $request->validate([
            'gatewayId' => 'required|exists:payment_gateways,id',
        ]);
        DB::beginTransaction();        
        try{            
            $data = $this->paymentGatewayService->clientGateweyDeactive($request->gatewayId,$request->status);            
            if($data){
                DB::commit();
                if($request->status == '1'){
                    return $this->responseService->response((object)[],__('Payment Gateway Active Successfully'));
                }else{
                    return $this->responseService->response((object)[],__('Payment Gateway Deactive Successfully'));
                }
            }else{
                DB::rollBack();
                return $this->responseService->response((object)[],__('Something went wrong'),101);
            }
        }catch(Exception $e){            
            DB::rollBack();
            Log::channel('client')->error('Client Payment Gateway Error',[ 'code' => 101,'msg' => $e->getMessage(),]);
            return $this->responseService->response((object)[],__('Something went wrong'),101);
        }

    }
}
