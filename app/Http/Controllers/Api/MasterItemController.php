<?php

namespace App\Http\Controllers\Api;

use App\Enums\RedisKeysEnum;
use App\Http\Controllers\Controller;
use App\Http\Services\RedisService;
use Illuminate\Http\Request;
use App\Http\Services\ResponseService;
use App\Http\Services\MasterItemService;
use App\Http\Services\CategoryService;
use App\Http\Services\MenuService;
use App\Models\MasterItem;
use App\Models\MasterCategory;
use App\Http\Services\CommonService;
use App\Http\Services\ResstoApiService;
use App\Models\MasterItemImage;
use App\Imports\MenuImport;
use App\Imports\UpdatMenuItem;
use App\Models\MasterItemTag;
use App\Models\MasterMenu;
use Maatwebsite\Excel\Importer;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Redis;
use Zip;
use App\Models\ItemType;

class MasterItemController extends Controller
{
    protected $resstoApiService, $redisService, $masterItemService, $categoryService, $menuService, $commonService, $importer, $responseService;
    public function __construct(MasterItemService $masterItemService, Importer $importer,CategoryService $categoryService, MenuService $menuService, CommonService $commonService)
    {
        $this->masterItemService = $masterItemService;
        $this->categoryService = $categoryService;
        $this->menuService = $menuService;
        $this->commonService = $commonService;
        $this->importer = $importer;
        $this->responseService = new ResponseService;
        $this->redisService = new RedisService;
        $this->resstoApiService = new ResstoApiService;
    }

    public function addMasterItem(Request $request){
        $user = auth()->user();
        $client_id = $user->id;

        $request->validate([
            'item_name' => 'required',
            'price' => 'required',
        ]);
        \DB::beginTransaction();
        try{
            $data = $request->only('item_name','item_name_thai','item_description','item_description_thai','price','item_type','quantity','qty_reset','is_available','is_featured','mrp','item_packaging_charge')+['quantity' => $request->quantity ? $request->quantity:'0','client_id' => $user->id,'main_category_id' => $request->is_menu_item ? $request->is_menu : $request->main_category_id, 'is_menu' => $request->is_menu_item ? 1:0];
            $master_item = $this->masterItemService->storeMasterItem($data);

            $tag = !blank($request->tag) ? $request->tag : [];
            $master_item->tag()->sync($tag); 
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        // menu item add in redis
        $menu_id = $request->is_menu_item ? $request->is_menu : $request->main_category_id;
        $get_master_item_redis = $this->redisService->get(RedisKeysEnum::MASTER_ITEM->value.':'.$menu_id);
        if(!blank($get_master_item_redis) && !blank($master_item)){
            $get_master_item = $this->masterItemService->getMasterItemByID($user,$client_id,$menu_id,'',50);
            $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$menu_id, $get_master_item);
        }
        \DB::commit();
        if($master_item){
            $this->resstoApiService->setMasterItems($master_item->id);
            $this->resstoApiService->setMenuItems();
            return $this->responseService->response($master_item,__('Master Item Added Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function getMenuItem(Request $request){
        $user = auth()->user();

        $get_item = MasterItem::with('tag')->where('id',$request->id)->where('client_id',$user->id)->first();
       
        if($get_item)
        {
            return $this->responseService->response($get_item,__('Menu item detail'));
        }else{
            return $this->responseService->response([],__('No menu item found'),101);
        }
    }

    public function getMasterItemByID(Request $request){
        // $request->validate([
        //     'itemLimit' => 'required',
        //     'itemSkip' => 'required'
        // ]);
        $user = auth()->user();
        $client_id = $user->id;
        // menu item list get in redis
        $get_master_item = $this->redisService->get(RedisKeysEnum::MASTER_ITEM->value.':'.$request->menu_id);
        if(blank($get_master_item)){
            $get_master_item = $this->masterItemService->getMasterItemByID($user,$client_id,$request->menu_id,$request->search,$request->itemSkip,$request->itemLimit);
            // menu item list add in redis
            $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$request->menu_id, $get_master_item);
        }else{
            if(count($get_master_item->data) <= $request->itemSkip){
                $get_master_item = $this->masterItemService->getMasterItemByID($user,$client_id,$request->menu_id,$request->search,$request->itemSkip,$request->itemLimit);
            }
            if(!blank($request->search)){
                // menu item list search data
                $get_master_item = $this->masterItemService->getMasterItemByID($user,$client_id,$request->menu_id,$request->search,$request->itemSkip,$request->itemLimit);
                // $get_master_item->data = array_filter($get_master_item->data, function($v, $k) use($request) {
                //     return stripos($v->item_name, $request->search) !== FALSE;
                // }, ARRAY_FILTER_USE_BOTH);
            }
            // if(blank($get_master_item)){
            //     // menu list data notfound to get and add redis
            //     $get_master_item = $this->masterItemService->getMasterItemByID($user,$client_id,$request->menu_id,$request->search,50);
            //     $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$request->menu_id, $get_master_item);
            // }

        }
        // master menu list data get in redias

        $get_restaurant_menu = $this->redisService->get(RedisKeysEnum::MASTER_MENU->value);
        // if(blank($get_restaurant_menu)){
        //     if(blank($get_restaurant_menu)){
        //         $get_restaurant_menu = $this->masterItemService->getMasterMenu($user,$request->search);
        //         // master menu list data add in redias
        //         $this->redisService->set(RedisKeysEnum::MASTER_MENU->value, $get_restaurant_menu);
        //     }
        // }
        if ($get_master_item) {
            // restaurant menu tag data list get in redias
            $get_restaurant_menu_tag = $this->redisService->get(RedisKeysEnum::RESTAURANT_MENU_TAG->value);
            if(blank($get_restaurant_menu_tag)){
                $get_restaurant_menu_tag = $this->masterItemService->getStoreAndGlobalTag($client_id);
                // restaurant menu tag data list add in redias
                $this->redisService->set(RedisKeysEnum::RESTAURANT_MENU_TAG->value, $get_restaurant_menu_tag);
            }
            // return $this->responseService->response(compact('get_master_item','get_restaurant_menu','get_restaurant_menu_tag'),__('Master Item listing'));
            return $this->responseService->response(compact('get_master_item','get_restaurant_menu_tag'),__('Master Item listing'));
        } else {
            return $this->responseService->response([],__('No master item found '),101);
        }
    }

    public function updateMasterItem(Request $request){
        $user = auth()->user();
        $client_id = $user->id;
        $request->validate([
            'item_name' => 'required',
            'price' => 'required',
        ]);
        \DB::beginTransaction();
        try{
            $get_master_item = MasterItem::where('id',$request->id)->where('client_id',$user->id)->first();
            
            $data = [
                'item_name' => isset($request->item_name) ? $request->item_name : $get_master_item->item_name,
                'item_name_thai' =>  isset($request->item_name_thai) ? $request->item_name_thai : $get_master_item->item_name_thai,
                'item_description' =>  isset($request->item_description) ? $request->item_description : '',
                'item_description_thai' => isset($request->item_description_thai) ? $request->item_description_thai : '',
                'price' => isset($request->price) ? $request->price : $get_master_item->price,
                'mrp' => isset($request->mrp) ? $request->mrp : $get_master_item->mrp,
                'item_type' => isset($request->item_type) ? $request->item_type : $get_master_item->item_type,
                'quantity' => isset($request->quantity) ? $request->quantity : $get_master_item->quantity,
                'is_available' => isset($request->is_available) ? $request->is_available : $get_master_item->is_available,
                'is_featured' => isset($request->is_featured) ? $request->is_featured : $get_master_item->is_featured,
                'qty_reset' => isset($request->qty_reset) ? $request->qty_reset : $get_master_item->qty_reset,
                'item_packaging_charge' => (isset($request->item_packaging_charge) && $request->item_packaging_charge != "") ? $request->item_packaging_charge : 0,
            ];
            $update = $this->masterItemService->updateMasterItem($request->id,$data);
            $tag = !blank($request->edit_tag) ? $request->edit_tag : [];
            $update->tag()->sync($tag);
            $get_master_item_redis = $this->redisService->get(RedisKeysEnum::MASTER_ITEM->value.':'.$request->main_category_id);
            if(!blank($get_master_item_redis) && !blank($update)){
                $get_master_item = $this->masterItemService->getMasterItemByID($user,$client_id,$request->main_category_id,'',50);
                $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$request->main_category_id, $get_master_item);
            }
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($update){
            $this->resstoApiService->setMasterItems($request->id);
            $this->resstoApiService->setMenuItems();            
            return $this->responseService->response($update,__('Master Item Updated Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function deleteMasterItem(Request $request){
        $user = auth()->user();
        
        \DB::beginTransaction();
        try{
            $item_delete = MasterItem::where('main_category_id',$request->id)->where('client_id',$user->id)->delete();

            $menu_delete = MasterCategory::where('id',$request->id)->where('client_id',$user->id)->delete();

        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($menu_delete){
            return $this->responseService->response($menu_delete,__('Menu deleted successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function deleteMenuItem(Request $request){
        $user = auth()->user();
        \DB::beginTransaction();
        try{
            $menu_item = MasterItem::where('id',$request->id)->where('client_id',$user->id)->first();
            $item_delete = MasterItem::where('id',$request->id)->where('client_id',$user->id)->delete();
            $item_tag = MasterItemTag::where('master_item_id', $request->id)->delete();
            $get_master_item_redis = $this->redisService->get(RedisKeysEnum::MASTER_ITEM->value.':'.$menu_item->main_category_id);
            if(!blank($get_master_item_redis) && !blank($menu_item)){
                $get_master_item = $this->masterItemService->getMasterItemByID($user,$user->id,$menu_item->main_category_id,'',50);
                $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$menu_item->main_category_id, $get_master_item);
            }
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($item_delete){
            $this->resstoApiService->setMasterItems($request->id);
            $this->resstoApiService->setMenuItems();
            return $this->responseService->response($item_delete,__('Menu item deleted successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function deleteMasterWithItem(Request $request){
        $user = auth()->user();
        
        \DB::beginTransaction();
        try{
                $menu = $this->menuService->getMenuById($user,$request->id);  
                if($menu){
                    $item = MasterItem::where('is_menu',$request->id)->get();
                    
                    foreach ($item as $itmvalue) {
                        $images = MasterItemImage::where('master_item_id',$itmvalue->id)->get();
                        if(!empty($images)){
                            foreach($images as $image){
                                // \Log::info('deleteMenuWithItem issue', [$image]);
                                $this->commonService->deleteImage($image->image_name,'Master_Item_image/'.$image->client_id.'/');
                            }
                            $image_delete = MasterItemImage::where('master_item_id',$itmvalue->id)->delete(); 
                        }
                    }
                    
                    $item_delete = MasterItem::where('is_menu',$request->id)->delete();
                    
                    $menu_delete = MasterMenu::where('id',$request->id)->delete();
                    
                }
        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($menu_delete){
            return $this->responseService->response($menu_delete,__('Menu deleted successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function getMasterItemImage(Request $request){
        
        $item = MasterItem::with('item_image')->where('id',$request->id)->first();
        if($item){
            $client_id = \Session::get('client_id');
            $item->master_item_media_path = asset('public/media/Temp/original/');
            return $this->responseService->response($item,__('Image List'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function addMasterImage(Request $request){
        $user = auth()->user();

        $request->validate([
            'image_name' => 'required'
        ]);
        \DB::beginTransaction();
        try{

            $client_id = $user->id;
            if(blank($request->master_item_id)){
                $item = MasterItem::where('id',$request->id)->first();
            }else{

                $item = MasterItem::where('id',$request->master_item_id)->first();
            }
  
            if(!blank($request->image_name)){
                $pics = [];
                foreach($request->image_name as $pic){
                    $path = 'media/Master_Item_image/'.$client_id.'/';
                    $v_pic = $this->commonService->getMovedFile($pic,$path,$this->app_name_snake_case().'_master');
                    array_push($pics, ['master_item_id' => $item->id,'image_name' => $v_pic,'client_id' => $client_id]);  
                }
                $add_image = $item->item_image()->createMany($pics);

                $get_master_item_redis = $this->redisService->get(RedisKeysEnum::MASTER_ITEM->value.':'.$item->main_category_id);
                if(!blank($get_master_item_redis)){
                    $get_master_item = $this->masterItemService->getMasterItemByID($user,$user->id,$item->main_category_id,'',50);
                    $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$item->main_category_id, $get_master_item);
                }

            }

        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        if($add_image){
            $this->resstoApiService->setMasterItems($item->id);
            $this->resstoApiService->setMenuItems();
            return $this->responseService->response($add_image,__('Image Added Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function deleteMasterImageById(Request $request){
        $request->validate([
            'file'  =>  'required'
        ]);
        $user = auth()->user();
        $image = MasterItemImage::where('image_name',$request->file)->first();
        $item = MasterItem::where('id',$image->master_item_id)->first();
        $this->commonService->deleteImage($request->file,'Master_Item_image/'.$image->client_id);
        MasterItemImage::where('image_name',$request->file)->delete();
        $get_master_item_redis = $this->redisService->get(RedisKeysEnum::MASTER_ITEM->value.':'.$item->main_category_id);
        if(!blank($get_master_item_redis) && !blank($item)){
            $get_master_item = $this->masterItemService->getMasterItemByID($user,$user->id,$item->main_category_id,'',50);
            $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$item->main_category_id, $get_master_item);
        }

        $this->resstoApiService->setMasterItems($item->id);
        $this->resstoApiService->setMenuItems();
        return $this->responseService->response(['deleted'  =>  true],__('Image deleted successfully'));
    }

    public function importMenuItem(Request $request){

        $user = auth()->user();

        ini_set('memory_limit', '-1');
        $request->validate([
            'import_file' => 'required'
        ]);
        \DB::beginTransaction();
        try{
            $client_id = $request->client_id;
            if($request->import_image_zip){
                $extract_path = public_path('media/Temp/original/'.$client_id.'/');
                if (!is_dir($extract_path)) {
                    mkdir($extract_path, 0777,true);
                    chmod($extract_path, 0777);
                }
                $file = new Filesystem;
                $file->cleanDirectory($extract_path);
                $zip_path = public_path('media/Temp/original/'.$request->import_image_zip);
                
                $zip = Zip::open($zip_path);
                if($zip){
                    $zip->extract($extract_path);
                }
                
                if (file_exists($zip_path)) {
                    \Log::info('importMenuItem delete zip', [$zip_path]);
                    unlink($zip_path);
                }
            }
            
            if($request->import_file) {
                $tmp_path = public_path('media/Temp/original/'.$request->import_file);
                $reader = $this->importer->import(new MenuImport($client_id),$tmp_path);

                if (file_exists($tmp_path)) {
                    \Log::info('importMenuItem delete xls', [$tmp_path]);
                    unlink($tmp_path);
                }
            } else {
                return back()->withErrors();
            }
        

        }catch(\Exception $e){
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();
        // master menu list data get in redias
        $get_restaurant_menu = $this->redisService->get(RedisKeysEnum::MASTER_MENU->value);
        if(!blank($get_restaurant_menu)){
            $this->redisService->del(RedisKeysEnum::MASTER_MENU->value, );
            // $get_restaurant_menu = $this->masterItemService->getMasterMenu($user,$request->search);
            // master menu list data add in redias
        }
        if($tmp_path){
            $this->resstoApiService->setMenuItems();
            return $this->responseService->response($tmp_path,__('Import Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);
    }

    public function uplodUpdetMasterItem(Request $request){
        $user = auth()->user();

        ini_set('memory_limit', '-1');
        $request->validate([
            'import_file' => 'required'
        ]);
        \DB::beginTransaction();
        try{
            $client_id = $request->client_id;
            if($request->import_image_zip){
                $extract_path = public_path('media/Temp/original/'.$client_id.'/');
                if (!is_dir($extract_path)) {
                    mkdir($extract_path, 0777,true);
                    chmod($extract_path, 0777);
                }
                $file = new Filesystem;
                $file->cleanDirectory($extract_path);
                $zip_path = public_path('media/Temp/original/'.$request->import_image_zip);
                
                $zip = Zip::open($zip_path);
                if($zip){
                    $zip->extract($extract_path);
                }
                
                if (file_exists($zip_path)) {
                    \Log::info('importMenuItem delete zip', [$zip_path]);
                    unlink($zip_path);
                }
            }
            
            if($request->import_file) {
                $tmp_path = public_path('media/Temp/original/'.$request->import_file);
                $reader = $this->importer->import(new UpdatMenuItem($client_id),$tmp_path);
                // dd($reader);
                if (file_exists($tmp_path)) {
                    \Log::info('importMenuItem delete xls', [$tmp_path]);
                    unlink($tmp_path);
                }
            } else {
                return back()->withErrors();
            }
        

        }catch(\Maatwebsite\Excel\Validators\ValidationException $e){
            \DB::rollBack();
            return $this->responseService->response($e->failures()[0]->errors(),__('Something went wrong'),101);
            throw $e->failures()[0]->errors();
        }
        \DB::commit();
        // master menu list data get in redias
        $get_restaurant_menu = $this->redisService->get(RedisKeysEnum::MASTER_MENU->value);
        if(!blank($get_restaurant_menu)){
            $get_restaurant_menu = $this->masterItemService->getMasterMenu($user,$request->search);
            // master menu list data add in redias
            $this->redisService->set(RedisKeysEnum::MASTER_MENU->value, $get_restaurant_menu);
        }
        if($tmp_path){
            $this->resstoApiService->setMenuItems();
            return $this->responseService->response($tmp_path,__('Import Successfully'));
        }
        return $this->responseService->response([],__('Something went wrong'),101);

    }
    public function updateItemAvailability(Request $request){
        $user = auth()->user();

        $is_available = $request->is_available;
    
        $item = $this->masterItemService->updateMasterItem($request->id,['is_available' => $is_available]);
        return $this->responseService->response($item,__('Menu Item Availability Changed'));
    }

    public function updateItemOrder(Request $request){

        if (!empty($request->menu_item_ids)) {
            foreach ($request->menu_item_ids as $key => $value) {
                $item = $this->masterItemService->updateMasterItem($value,['reorder' => $key]);
            }
            return $this->responseService->response($item,__('Menu Item Order Changed'));
        }
    }

    public function getItemType(Request $request){
        $items = ItemType::all();
        return $this->responseService->response($items,__('Master Item Added Successfully'));
    }


}
