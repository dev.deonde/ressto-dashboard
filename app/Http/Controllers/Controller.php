<?php

namespace App\Http\Controllers;

use App\Models\LogActivity;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Str;
use App\Imports\SampleExport;
use App\Imports\Export;
use App\Models\GeneralSetting;
use App\Models\MasterCategory;
use App\Models\MasterMenu;
use App\Models\VendorSetting;
use App\Models\WebSetting;
use App\Models\Page;
use Excel;
use Illuminate\Routing\Controller as BaseController;

use Auth,Request,DB;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function settings($client_id,$key_name){
      $arr = [];
      $setting = VendorSetting::where('client_id', $client_id)->where('settings_key',$key_name)->get();
      if(!blank($setting)){
          foreach($setting as $key => $value){
              return $value['setting_values'];
          }
      }else{
          $setting = GeneralSetting::where('client_id', $client_id)->where('settings_key',$key_name)->get();
          if(!blank($setting)){
              foreach($setting as $key => $value){
                  return $value['setting_values'];
              }
          }else{
              $setting = WebSetting::where('client_id', $client_id)->where('settings_key',$key_name)->get();
              if(!blank($setting)){
                  foreach($setting as $key => $value){
                      return $value['setting_values'];
                  }
              }
          }
      }
  }
    public function app_name_snake_case()
    {
        return \Str::snake(\Str::lower(config('constant.APP_NAME')));
    }

    public function blankSampleMenuItemExport($client_id) {
        return Excel::download(new SampleExport(base64_decode($client_id)), 'blank_sample_menu_item_'.date("Y-m-d-H-i-s").'.xls');
    }

    public function export($menu_id,$client_id,$type){
      if(base64_decode($type) == 'item'){
        $menu = MasterMenu::select('menu_name')->where('id',base64_decode($menu_id))->first();
        return Excel::download(new Export($menu_id,$type,$client_id), $menu->menu_name.'.xls');
      }else{
        $menu = MasterMenu::select('menu_name')->where('id',base64_decode($menu_id))->first();
        return Excel::download(new Export($menu_id,$type,$client_id), $menu->menu_name.'.xls');
        }
    }

    public static function addToLog($subject)
    {

      if(isset(Auth::guard('client')->user()->id)){
        $userId = Auth::guard('client')->user()->id;
        $userType = 0;
      }else if(Auth::guard('partner')->user()->user_role == '2'){
        $userId = Auth::guard('partner')->user()->user_id;
        $userType = 1;
      }elseif(Auth::user()->user_role == '3'){
        $userId = Auth::user()->user_id;
        $userType = 2;
      }      
      $log = [];
      $log['subject'] = $subject;
      $log['url'] = Request::fullUrl();
      $log['method'] = Request::method();
      $log['ip'] = Request::ip();
      $log['agent'] = Request::header('user-agent');
      $log['user_id'] = $userId;
      $log['user_type'] = $userType;
      LogActivity::create($log);

    }

    public function page($id) {
      $decoded = explode(',',base64_decode($id));
      if (count($decoded) == 2) {
        $page_id = $decoded[0];
        $vendor_id = $decoded[1];                
        $page_data = Page::where('id',$page_id)->where('client_id',$vendor_id)->first();
        return view('content-page.view-page',['page_data' => $page_data,'client_id'=>$vendor_id]);
      }
    }

    public function app_color_buttons($clientid) {      
      $setting =  WebSetting::where('client_id', $clientid)->where('settings_key', 'button_primary_color')->first();
      if($setting){      
        return $setting->setting_values;
      }
      return "#0433ff";
    }

    public function app_color_header($clientid)
    {
      $setting =  WebSetting::where('client_id', $clientid)->where('settings_key', 'theme_primary_color')->first();
      if($setting){      
        return $setting->setting_values;
      }
      return "#0433ff";
    }

    public function getSystemTimezone($client_id)
    {
      $setting = VendorSetting::where('client_id', $client_id)->where('settings_key','timezone')->first();
      if(count((array)$setting) == 0){
        return "";
      }
      $setting = $setting->setting_values;
      return $setting;
    }
}
