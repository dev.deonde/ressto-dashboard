<?php

namespace App\Http\Services;
use Spatie\Permission\Models\Permission;
use App\Models\Language;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\Http\Services\ResponseService;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\File as MyFile;
use Illuminate\Support\Facades\Storage;
// use Google\Cloud\Storage\StorageClient;
use App\Models\CountryCode;
use App\Models\Currency;

class CommonService
{
    protected $bucketName;

    public function __construct(){
        $this->bucketName = config('constant.storage_bucket');
        $this->storage_type = config('constant.storage_type');
        $this->backTo = '';
        ini_set('max_execution_time', 0); 
    }

    public function uploadImage($image,$folder)
    {
        $fileContent = file_get_contents($image);
        $createfilename = $folder.'_'.time().rand(0000, 9999);
        $extension = $image->getClientOriginalExtension();
        $filename  = $createfilename.'.'.$extension;
        $folder = $folder."/";
        if($this->storage_type == 'local'){
            if(!File::exists(public_path('media/'.$folder.'original/'))){
                File::makeDirectory(public_path('media/'.$folder.'original/'),0777,true,true);
            }
            if(!File::exists(public_path('media/'.$folder.'thumb/'))){
                File::makeDirectory(public_path('media/'.$folder.'thumb/'),0777,true,true);
            }
    
            $image->move(public_path('media/'.$folder.'original/'), $filename);
            $path = public_path("media/Temp/original/".$filename);
            $this->resize_crop_image('600', '300', $path, str_replace('original','thumb',$path), 50);    
        } elseif($this->storage_type == 'google'){
            // connect to Google Cloud Storage using private key as authentication
            $storage = $this->getStorageClient();
            // set which bucket to work in
            $bucket = $storage->bucket($this->bucketName);
            // upload/replace file 
            $storageObject = $bucket->upload(
                $fileContent,
                ['name' => 'media/'.$folder.'original/'.$filename]
            );
            $storageObject = $bucket->upload(
                $fileContent,
                ['name' => 'media/'.$folder.'thumb/'.$filename]
                // if $cloudPath is existed then will be overwrite without confirmation
                // NOTE: 
                // a. do not put prefix '/', '/' is a separate folder name  !!
                // b. private key MUST have 'storage.objects.delete' permission if want to replace file !
            );
        } elseif($this->storage_type == 'aws') {
            //Bellow example will not allow to rename image
            // $img = Storage::disk('s3')->put(
            //     'media/'.$folder.'original',
            //     $image,
            //     $filename
            // );
            //Bellow example will allow to rename image
            Storage::disk('s3')->putFileAs(
                'media/'.$folder.'original',
                $image,
                $filename,
            );
            Storage::disk('s3')->putFileAs(
                'media/'.$folder.'thumb',
                $image,
                $filename,
            );
        }
        // is it succeed ?
        return $filename;  
    }

    public function uploadImageInLocal($image,$folder,$filename = "")
    {

        $fileContent = file_get_contents($image);
        $createfilename = $folder.'_'.time().rand(0000, 9999);
        $extension = $image->getClientOriginalExtension();
        $filename  = $createfilename.'.'.$extension;
        $image->move(public_path("media/Temp/original"), $filename);
        if($extension !== 'pem' && $extension !== 'xls' && $extension !== 'xlsx' && $extension !== 'zip'){
            $this->resize_crop_image('600', '300', "media/Temp/original/".$filename, 'media/Banner/thumb/'.$filename, 50);
        }
        return $filename;

    }

    public function getMovedPemFile($file,$path,$filename){
        // $filename = $this->getFileName($file,$folder);
        $tmp_path = public_path('media/Temp/original/'.$file);
        if(File::exists($tmp_path)){            
            base_path("master/public/$path");
            if(!File::exists(base_path("master/public/$path"))){
                File::makeDirectory(base_path("master/public/$path"),0777,true,true);
            }
            $data  =File::move($tmp_path,base_path("master/public/$path/$filename"));
            info('pem_moved',[$data,$tmp_path,base_path("master/public/$path/$filename")]);
        }
        return "$path/$filename";
    }

    public function getFileNamePem($file, $folder){
        $vendor_id = auth()->user()->parent_vendor_id??auth()->user()->vendor_id;
        $extension = explode('.',$file)[1];
        $createfilename = $folder.$vendor_id;
        $filename  = $createfilename.'.'.$extension;
        return $filename;
    }

    public function uploadPemInLocal($image,$folder,$filename = "")
    {
        $vendor_id = auth()->user()->vendor_id;
        $fileContent = file_get_contents($image);
        $createfilename = $folder.$vendor_id;
        $extension = $image->getClientOriginalExtension();
        if($filename == ""){
            $filename  = $createfilename.'.'.$extension;
        }
        $image->move(public_path("media/Temp/original/"), $filename);
        
        return $filename;

    }

    public function getMovedFileInLocal($file,$path,$folder){
        $filename = $this->getFileNamePem($file,$folder);
        $tmp_path = public_path('media/Temp/original/'.$file);
        info('getMovedFileInLocal',[$path,$tmp_path,public_path($path.$filename)]);
        if(!File::exists(public_path($path))){
            File::makeDirectory(public_path($path),0777,true,true);
        }
        if(File::exists($tmp_path)){
            rename($tmp_path,public_path($path.$filename));
            $file = fopen(public_path($path.$filename), 'r');
            $api_url = config('constant.api_url');            
            $res = Http::attach(
                'pem_file', $file, $filename
                )->post("{$api_url}upload_pem",[
                'path' => $path,
                'filename' => $filename
            ]);
            info('$res',[$res]);
        }
        return $filename;
    }


    public function deleteImage($filename, $folder,$is_original = true){        
        if (isset($filename) && $filename) {
            if($this->storage_type == 'google' && $is_original == true){
                // \Log::info('deleteImage issue menu', [$bucketBaseUrl, $this->bucketName, '/media/'.$folder.$filename]);
                if ($this->bucketName != '') {
                    // if (File::exists($bucketBaseUrl.$this->bucketName.'/media/'.$folder.'/original/'.$filename)) {
                    //   if (file_get_contents($bucketBaseUrl.$this->bucketName.'/media/'.$folder.'/original/'.$filename)) {
                            $this->delete_object("media/$folder/original/$filename");
                    //   } elseif (file_get_contents($bucketBaseUrl.$this->bucketName.'/media/'.$folder.$filename)) {
                            $this->delete_object("media/{$folder}/{$filename}");
                    //   }
                    // }
                }
                // $this->delete_object("media/$folder/original/$filename");
                if (File::exists(public_path("media/$folder/original/$filename"))){
                    return File::delete(public_path("media/$folder/original/$filename"));
                } 
                elseif (File::exists(public_path("media/{$folder}{$filename}"))) {
                    return File::delete(public_path("media/{$folder}{$filename}"));
                }
            } else if ($this->storage_type == 'aws') {
                Storage::disk('s3')->delete("media/$folder/original/$filename");
                Storage::disk('s3')->delete("media/$folder$filename");
                Storage::disk('s3')->delete("media/$folder/$filename");
                Storage::disk('s3')->delete("media/$folder/thumb/$filename");

                if(File::exists(public_path("media/{$folder}/original/{$filename}"))){
                    return File::delete(public_path("media/{$folder}/original/{$filename}"));
                }
            } else{
                $this->delete_object("media/{$folder}{$filename}");
                if(File::exists(public_path("media/{$folder}{$filename}"))){
                    return File::delete(public_path("media/{$folder}{$filename}"));
                }
            }
        }
        return true;
    }

    function delete_object($objectName, $options = [])
    {
        if($this->storage_type == 'google'){
            try{
                $storage = $this->getStorageClient();
                $bucket = $storage->bucket($this->bucketName);
                $object = $bucket->object($objectName);
                if($object->exists()){
                    $object->delete();
                }
                \Log::info('Deleted gs:',['bucketName' => $this->bucketName, 'objectName' => $objectName]);
            }catch(\Exception $e){
                \Log::info('exception Deleted gs:',['bucketName' => $this->bucketName, 'objectName' => $objectName, 'errpr' => $e->getMessage()]);
            }
        }
    }

    public function deleteFileByPath($path)
    {
        if(File::exists($path))
        {
            return File::delete($path);
        }
        return false;
    }

    public function getFileName($file, $folder){
        $extension = explode('.',$file)[1];
        $createfilename = str_replace(' ','_',$folder).'_'.time().rand(0000, 9999);
        $filename  = $createfilename.'.'.$extension;
        return $filename;
    }

    public function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80){
   
        $imgsize = getimagesize($source_file);
       
        $width = $imgsize[0];
        $height = $imgsize[1];
        $mime = $imgsize['mime'];
       
        switch($mime){
        case 'image/gif':
        $image_create = "imagecreatefromgif";
        $image = "imagegif";
        break;

        case 'image/png':
        $image_create = "imagecreatefrompng";
        $image = "imagepng";
        $quality = 7;
        break;

        case 'image/jpeg':
        $image_create = "imagecreatefromjpeg";
        $image = "imagejpeg";
        $quality = 80;
        break;

        default:
        return false;
        break;
        }

        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
       
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if($width_new > $width){
        //cut point by height
        $h_point = (($height - $height_new) / 2);
        //copy image

        if(($mime == 'image/png') OR ($mime==3)){
        imagealphablending($dst_img, false);
        imagesavealpha($dst_img,true);
        $transparent = imagecolorallocatealpha($dst_img, 255, 255, 255, 127);
        imagefilledrectangle($dst_img, 0, 0, $width, $height, $transparent);
        }
      
        imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        }else{
        //cut point by width
        $w_point = (($width - $width_new) / 2);
        if(($mime == 'image/png') OR ($mime==3)){
        imagealphablending($dst_img, false);
        imagesavealpha($dst_img,true);
        $transparent = imagecolorallocatealpha($dst_img, 255, 255, 255, 127);
        imagefilledrectangle($dst_img, 0, 0, $width, $height, $transparent);
        }
        imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        $image($dst_img, $dst_dir, $quality);

        if($dst_img)imagedestroy($dst_img);
        if($src_img)imagedestroy($src_img);
    }

    public function getMovedFile($file,$path,$folder){
        $storage_type = $this->storage_type;
        $filename = $this->getFileName($file,$folder);
        if($storage_type == 'local'){
            $tmp_path = public_path($this->backTo.'media/Temp/original/'.$file);
            $thumb_tmp_path = str_ireplace('original','thumb',$tmp_path);
            $thumb_path = str_ireplace('original','thumb',$path);
            info('File::exists',[File::exists($tmp_path)]);
            if(File::exists($tmp_path)){
                if(strpos($tmp_path,'original') !== false){
                    if(!File::exists(public_path($this->backTo.$thumb_path))){
                        File::makeDirectory(public_path($this->backTo.$thumb_path),0777,true,true);
                    }
                    File::move($thumb_tmp_path,public_path($this->backTo.$thumb_path.$filename));
                }
                if(!File::exists(public_path($this->backTo.$path))){
                    File::makeDirectory(public_path($this->backTo.$path),0777,true,true);
                }
                $filemoved = File::move($tmp_path,public_path($this->backTo.$path.$filename));
                info($filemoved,[File::exists($tmp_path)]);
            }
        }
        elseif($storage_type == 'google'){
            $tmp_path = public_path('media/Temp/original/'.$file);
            // connect to Google Cloud Storage using private key as authentication
            $storage = $this->getStorageClient();
    
            $bucketName = $this->bucketName;
            $objectName = 'media/Temp/original/'.$file;
            $newObjectName = $path.$filename;
            
            $bucket = $storage->bucket($bucketName);
            $object = $bucket->object($objectName);
            $object->copy($bucketName, ['name' => $newObjectName]);
            if(strpos($newObjectName,'original') !== false){
                $object->copy($bucketName, ['name' => str_replace('original','thumb',$newObjectName)]);
            }
           
            if($folder == "Banner"){
              
                 $tmp_path = public_path('media/Banner/thumb/'.$file);
                $path = "media/Banner/New_thumb/";
               
                 if(File::exists($tmp_path)){
                    $bucket->upload(
                        fopen($tmp_path,'r'),
                        [
                            'predefinedAcl' => 'publicRead',
                            'name' => $path.$filename
                        ]
                    );
                }
            }
            $object->delete();
        }else if ($storage_type == 'aws') {
            $tmp_path = public_path('media/Temp/original/'.$file);
            $s3 = Storage::disk('s3');
            $objectName = 'media/Temp/original/'.$file;
            $thumbobjectName = 'media/Temp/thumb/'.$file;
            $newObjectName = $path.$filename;
            $object = $s3->url($objectName);
            $s3->copy($objectName, $newObjectName);
            if (strpos($newObjectName, 'original') !== false) {
                $s3->copy($objectName, str_replace('original','thumb',$newObjectName));
            }
            if($folder == "Banner"){
                $tmp_path = public_path('media/Banner/thumb/'.$file);
                $path = "media/Banner/New_thumb/";

                if(File::exists($tmp_path)){
                    Storage::disk('s3')->putFileAs(
                        $path,
                        new MyFile($tmp_path),
                        $filename
                    );
                }
            }
            $s3->delete($objectName);
            $s3->delete($thumbobjectName);
        }
        return $filename;
    }

    public function getSelectedLanguage($where){
        $languages = Language::whereIN('abbrivation',$where)->get();
        return array($languages->where('abbrivation', $where[0])->first(), $languages->where('abbrivation', $where[1])->first());
    }

    public function givePermissions($client){
        $client->givePermissionTo(Permission::where('guard_name','client')->get());
    }

    public function giveSpecificPermission($client, $permissionList = []) {
        if (!empty($permissionList)) {
            $client->givePermissionTo(Permission::whereIn('name', $permissionList)->where('guard_name','client')->get());
        }
    }
    
    public function getVendorByBusinessCode($app_name = "ABC"){
        $code = isset($app_name) ? strtoupper(substr($app_name,0,4)) : "ABC";
        return $code;
    }

    public function getVendorUniqueCode($app_name = "ABC",$number = "",$type="vendor"){
        $t = 'V';
        if($type == 'customer'){
            $t = 'C';
        }
        $businessCode = $this->getVendorByBusinessCode($app_name);
        return $businessCode.$t.$number;
    }

    public function getSetting($settings_key_name, $guard = 'client'){
        return auth($guard)->user()->setting?->where('settings_key',$settings_key_name)->first()->setting_values;
    }

    private function getStorageClient()
    {
        try {
            $storage = new StorageClient([
                'keyFilePath' => $this->privateKeyFile
            ]);
        } catch (\Exception $e) {
            // maybe invalid private key ?
            return false;
        }
        return $storage;
    }

    public static function primaryLanguages(){ 
        return Language::where('status','1')->get();
    }

    public static function secondaryLanguages(){
        return Language::orderBy('status')->get();
    }

    public static function timezoneList()
    {
        static $timezones = null;
        if ($timezones === null) {
            $timezones = [];
            $offsets = [];
            $now = new \DateTime('now', new \DateTimeZone('UTC'));

            foreach (\DateTimeZone::listIdentifiers() as $timezone) {
                $now->setTimezone(new \DateTimeZone($timezone));
                $offsets[] = $offset = $now->getOffset();
                $timezones[$timezone] = '(' . Self::format_GMT_offset($offset) . ') ' . Self::format_timezone_name($timezone);
            }

            array_multisort($offsets, $timezones);
        }
        return $timezones;
    }

    public static function countryCodeList()
    {
        return $codes = CountryCode::where('status',1)->get();
    }

    public static function currencyList()
    {
        return $codes = Currency::where('status',1)->get();
    }

    public static function format_GMT_offset($offset)
    {
        $hours = intval($offset / 3600);
        $minutes = abs(intval($offset % 3600 / 60));
        return 'GMT' . ($offset ? sprintf('%+03d:%02d', $hours, $minutes) : '');
    }

    public static function format_timezone_name($name)
    {
        $name = str_replace('/', ', ', $name);
        $name = str_replace('_', ' ', $name);
        $name = str_replace('St ', 'St. ', $name);
        return $name;
    }
}