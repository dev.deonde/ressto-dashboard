<?php

namespace App\Http\Services;

use App\Models\GeneralSetting;

class GeneralSettingService
{
    public function getSetting(){
        return GeneralSetting::whereClientId(auth()->user()->myid())->get();
    }
    public function updateOrCreate($key, $value){
        return GeneralSetting::updateOrCreate(
            [
                'client_id' => auth()->user()->myid(),
                'settings_key' => $key
            ],
            [
                'setting_values' => $value
            ]
        );
    }

    public function defaultUpdate($key, $value, $clientId){
        return GeneralSetting::updateOrCreate(
            [
                'client_id' => $clientId,
                'settings_key' => $key
            ],
            [
                'setting_values' => $value
            ]
        );
    }
}