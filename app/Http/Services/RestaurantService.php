<?php

namespace App\Http\Services;

use App\Models\Restaurant;
use App\Models\RestaurantHour;
use App\Models\RestaurantSetting;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class RestaurantService
{
    public function storeRestaurant($data)
    {
        return Restaurant::create($data);
    }

    public function updatedClient($where, $updat){
        return Restaurant::where($where)->update($updat);
    }

    public function getRestaurant($clientId){
        return Restaurant::where('client_id', $clientId)->with(['country:id,country_name','state:id,state_name', 'city:id,cityname','restaurantSettings'])->first();        
    }

    public function storeAndUpdatedRestaurantHours($restaurant_id, $slots = [], $restaurant_setting){
        $model = new RestaurantHour();
        $model->where('restaurant_id',$restaurant_id)->delete();
        foreach($slots as $type => $days){
            foreach ($days as $day) {
                foreach($day as $value){
                    if(!blank($value['start_time']) && !blank($value['end_time'])){
                        $data[] = $model->create(
                            [
                                'restaurant_id' => $restaurant_id,
                                'days' => $value['day'],
                                'start_time' => Carbon::createFromFormat('h:i A',$value['start_time'])->format('H:i:s'),
                                'end_time' => Carbon::createFromFormat('h:i A',$value['end_time'])->format('H:i:s'),
                                'types' => $type,
                                'status' => $value['status'] == "1"?'1':'0',
                            ]
                        );
                    }else{
                        $data[] = $model->create(
                            [
                                'restaurant_id' => $restaurant_id,
                                'days' => $value['day'],
                                'start_time' => '00:00:00',
                                'end_time' => '00:00:00',
                                'types' => $type,
                                'status' => $value['status'] == "1"?'1':'0',
                            ]
                        );
                    }
                }
            }
        }
        foreach($restaurant_setting as $key => $value){
            $setting = RestaurantSetting::updateOrCreate(
                [
                    'restaurant_id' => $restaurant_id,
                    'settings_key' => $key,
                ],
                ['setting_values' => $value]
            );
        }
        return $data;
    }

    public function getServiceOffering($restaurant_id=''){
        if(!blank($restaurant_id)){
            $restaurantHours = RestaurantHour::where('restaurant_id',$restaurant_id)->select('restaurant_id', 'days', 'start_time', 'end_time', 'types', 'status')->get();
            $data = collect($restaurantHours)->groupBy('types');
            return $data;
        }else{
            return false;
        }
    }

    public function taxSettingStoreUpdat($restaurant_id, $data, $setting_key){
        $setting = RestaurantSetting::updateOrCreate(
            [
                'restaurant_id' => $restaurant_id,
                'settings_key' => $setting_key,
            ],
            ['setting_values' => json_encode($data)]
        );
        return $setting;
    }
     public function getRestaurantSettingWithKey($restaurant_id, $setting_key){
        return RestaurantSetting::where('restaurant_id', $restaurant_id)->where('settings_key',$setting_key)->get();
     }

     public function otherSettingStoreUpdate($restaurant_id, $data){
        foreach($data as $key => $value){
            $setting[] = RestaurantSetting::updateOrCreate(
                [
                    'restaurant_id' => $restaurant_id,
                    'settings_key' => $key,
                ],
                ['setting_values' => is_array($value)? json_encode($value): $value]
            );
        }
        // $arr = [];
        // foreach($setting as $value){
        //     $arr = array_merge([$value->settings_key => json_decode($value->setting_values)], $arr);
        // }
        return $setting;
     }

     public function getOtherSetting($restaurant_id){
        $data = [
            'minimum_order_value' => '',
            'cost_for_two_person' => '',
            'packaging_charge' => (object)[],
            'convince_charge' => (object)[],
            'restaurant_on_off' => '1'
        ];
        $minimum_order_value = RestaurantSetting::where('restaurant_id', $restaurant_id)->where('settings_key','minimum_order_value')->first();
        if(!blank($minimum_order_value)){
            $data['minimum_order_value'] = $minimum_order_value->setting_values;
        }
        $restaurant_on_off = RestaurantSetting::where('restaurant_id', $restaurant_id)->where('settings_key','restaurant_on_off')->first();
        if(!blank($restaurant_on_off)){
            $data['restaurant_on_off'] = $restaurant_on_off->setting_values;
        }
        $cost_for_two_person = RestaurantSetting::where('restaurant_id', $restaurant_id)->where('settings_key','cost_for_two_person')->first();
        if(!blank($cost_for_two_person)){
            $data['cost_for_two_person'] = $cost_for_two_person->setting_values;
        }
        $packaging_charge = RestaurantSetting::where('restaurant_id', $restaurant_id)->where('settings_key','packaging_charge')->first();
        if(!blank($packaging_charge)){
            $data['packaging_charge'] = json_decode($packaging_charge->setting_values);
        }
        $convince_charge = RestaurantSetting::where('restaurant_id', $restaurant_id)->where('settings_key','convince_charge')->first();
        if(!blank($convince_charge)){
            $data['convince_charge'] = json_decode($convince_charge->setting_values);
        }
        return $data;
     }
}