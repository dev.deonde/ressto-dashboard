<?php 

namespace App\Http\Services;
use App\Models\ClientPaymentGateways;
use App\Models\PaymentGateway;

class PaymentGatewayService 
{
    public function getClientPaymentGateway($gatewayId){
        $arr = [];
        $gatweay = ClientPaymentGateways::whereClientId(auth()->user()->myid())->wherePaymentGatewayId($gatewayId)->get();
        foreach($gatweay as $key => $value){
             array_push($arr, 
                [
                    'name' => str_replace('_', ' ', $value['payment_gateway_key']) , 
                    'id' => $value['payment_gateway_key'],
                    'value' => $value['payment_gateway_key_values'] 
                ]
            );
        }
        return $arr;

    }
    public function storeClientPaymentGateway($gatewayId, $keys=null){
        if(!blank($keys)){
            foreach ($keys as $key) {
                $data = ClientPaymentGateways::updateOrCreate(
                    [
                        'client_id' => auth()->user()->myid(),
                        'payment_gateway_id' => $gatewayId,
                        'payment_gateway_key' => $key->key
                    ],
                    [
                        'payment_gateway_key_values' => $key->value
                    ]
                );
            }
            PaymentGateway::where('id',$gatewayId)->update(['status' => '1']);
            return true;
        }
        return false;
    }

    public function clientGateweyDeactive($gatewayId,$status){                    
        return PaymentGateway::where('id',$gatewayId)->update(['status' => $status]);
    }
}