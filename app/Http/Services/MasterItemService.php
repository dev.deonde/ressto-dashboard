<?php

namespace App\Http\Services;

use App\Models\MasterItem;
use App\Models\Tag;
use App\Models\MasterMenu;

class MasterItemService
{
    public function storeMasterItem($data)
    {
        return MasterItem::create($data);
    }

    public function getMasterItemByID($user,$client_id,$menu_id,$search,$skip=0,$limit=50){

        // dd($skip,$limit);
        return MasterItem::with('item_image','customization.Menu_customization.sub_customization')
            ->where(function ($q) use($search){
            if($search != ''){
                $q->where('item_name','LIKE','%'.\App\Helper::escape_like($search).'%')
                ->orWhere('item_name_thai','LIKE','%'.\App\Helper::escape_like($search).'%')
                ->orWhere('item_description','LIKE','%'.\App\Helper::escape_like($search).'%');
            }
        })->where('client_id',$client_id)->where('main_category_id',$menu_id)->orderBy('reorder','ASC')->skip($skip)->paginate($limit);
    }

    public function getItemByID($id)
    {
        return MasterItem::where('id',$id)->first();
    }
    
    public function updateMasterItem($id,$data) {
        MasterItem::where('id',$id)->update($data);
        return MasterItem::where('id',$id)->first();
    }

    public function getStoreAndGlobalTag($client_id) {
        return Tag::where('status','0')->where('client_id',$client_id)
        ->orderBy('name','ASC')
        ->get();
    }

    public function getMasterMenu($user,$search = ""){
        return MasterMenu::
            withCount(['item'=>function($q)use($search){
                if($search != ''){
                    return $q->where('item_name','LIKE','%'.$search.'%')
                    ->orWhere('item_name_thai','LIKE','%'.$search.'%')
                    ->orWhere('item_description','LIKE','%'.$search.'%');
                }
            }])->where('client_id',$user->id)->orderBy('reorder_data','ASC')->get();
    }
}