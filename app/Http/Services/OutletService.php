<?php 

namespace App\Http\Services;

use App\Models\RestaurantCuisine;

class OutletService {
    
    public function updatedRestaurantCuisines($restaurantCuisines = [], $restaurantId){
        $restaurantCuisinesData = RestaurantCuisine::where('restaurant_id',$restaurantId)->get();
        if(!blank($restaurantCuisinesData)){
            foreach ($restaurantCuisinesData as $key => $value) {
                if(!in_array($value->cuisine_id, $restaurantCuisines)){
                    $value->delete();
                }else{
                    array_splice($restaurantCuisines,array_search($value->cuisine_id, $restaurantCuisines),1);
                }
            }
        }
        if(!blank($restaurantCuisines)){
            foreach ($restaurantCuisines as $value) {
                RestaurantCuisine::updateOrCreate(
                    ['restaurant_id' => $restaurantId, 'cuisine_id' => $value],
                    ['restaurant_id' => $restaurantId, 'cuisine_id' => $value]
                );
            }
        }
    }
}


?>