<?php

namespace App\Http\Services;

use Session;
use App\Models\Vendor;
use App\Models\DriverOrder;
use App\Models\OrderImages;
use App\Http\Services\CommonService;
use Illuminate\Support\Facades\Http;
use App\Models\RestaurantMenuItem;
use App\Models\DunzoHistory;
// use App\Http\Services\NotificationService;
use App\Models\Chat;
use App\Models\DriverOrderRequest;
use App\Models\Order;
use DB;

class OrderService
{
    protected $order,$commonService;

    public function __construct(Order $order, CommonService $commonService)
    {
        $this->order = $order;
        // $this->notificationService = $notificationService;
        $this->commonService = $commonService;
    }

    /*
     *    Placed
     *    In Kitchen
     *    Ready to Serve
     *    On The Way
     *    Arrived
     *    Delivered
     *    Cancelled
     *    PaymentPending
     *    PaymentFailure
     *
     *  --- delivery_pickup_types ---
     *  Delivery
     * Takeaway
     * Pickup
     * user_id 0 means main vendor data
     * else merchant id passed and return particular merchant data
     */

    public function getOrders($client_id, $timezone, $filters, $paginate = true, $user_id = 0)
    {
      
      $order = $this->order
      ->selectRaw("
        orders.id,
        TO_BASE64(orders.id) as base_id,
        CONCAT('#',orders.id) as order_id,
        customers.id AS customer_id,
        CONCAT(COALESCE(customers.user_name, ''),' ',COALESCE(customers.last_name, '')) AS customer_name,
        restaurant.name AS vendor_name,
        orders.order_status,
        CONVERT_TZ(orders.created_at,'UTC','{$timezone}') as created_date,
        CONVERT_TZ(orders.confirm_date,'UTC','{$timezone}') as confirm_date,
        CONVERT_TZ(orders.ontheway_date,'UTC','{$timezone}') as ontheway_date,
        CONVERT_TZ(orders.delivered_date,'UTC','{$timezone}') as delivered_date,
        CONVERT_TZ(orders.arrived_date,'UTC','{$timezone}') as arrived_date,
        orders.delivery_type,
        orders.future_delivery_date,
        CONVERT_TZ(orders.cancelled_date,'UTC','{$timezone}') as cancelled_date,
        orders.total_amount,
        orders.delivery_pickup_types,
        orders.payment_method,
        orders.wallet_amount,
        orders.amount,
        client.client_name as access_name,
        orders.is_dunzo_driver,
        orders.is_load_sharing_driver
        ")
      ->with('driver:id,firstname,lastname,latitude,longitude,profileimage')
      // ->with('dunzoDriver')
      // ->with('loadShareDriver')
      ->join('customers','customers.id','=','orders.user_id')
      ->join('restaurant','restaurant.id','=','orders.restaurant_id')
      // ->join('users','users.user_id','=','fa_restaurants.user_id')
      ->leftJoin('client','orders.user_access_id','=','client.id')
      // ->where('users.vendor_id',$vendor_id)
      // ->where('restaurant.is_confirm','1')
      ->where('orders.client_id',$client_id)
      ->when($user_id !== 0,function($q)use($user_id){
        $q->where('restaurant.client_id',$user_id);
      })
      ->where(function ($q) use ($filters, $timezone) {
        if (isset($filters['status']) && !blank($filters['status'])) {
          if($filters['status'] == "Scheduled"){
            // $q->where('future_delivery_date', '!=','0000-00-00 00:00:00')->where('order_status','Placed');
            $q->where('future_delivery_date', '!=','0000-00-00 00:00:00')->where('order_status','InKitchen');
          }else if($filters['status'] == "Placed"){
            $q->where('order_status','Placed');
          }else if($filters['status'] == "OnTheWay"){
            $q->where(function($q1){
              $q1->where('orders.order_status', 'OnTheWay')->orwhere('orders.order_status', 'Arrived')->orwhere('orders.order_status','OnPickUp');
            });
          }elseif ($filters['status'] == "InKitchen") {
            $q->where('future_delivery_date', '=','0000-00-00 00:00:00')->where('order_status','InKitchen');
          }else if($filters['status'] == "PaymentFailure"){
            if (isset($filters['abandonedstatus']) && $filters['abandonedstatus'] != '') {
              $q->where('orders.order_status', $filters['abandonedstatus']);
            } else {
              $q->whereIn('orders.order_status', ['PaymentFailure', 'PaymentPending', 'AwaitForPayment']);
            }
          }else{
            if(isset($filters['driver_id']) && !blank($filters['driver_id'])){
              if($filters['status'] == 'Delivered'){
                $q->where('orders.order_status', $filters['status']);
              }
            }else{
              $q->where('orders.order_status', $filters['status']);
            }
          }
        }
        if(isset($filters['user_id']) && !blank($filters['user_id'])){
          $q->where('orders.user_id',$filters['user_id']);
        }
        if (isset($filters['status']) && !blank($filters['status'])) {
          if ($filters['status'] != 'Scheduled' && isset($filters['start_date']) && !blank($filters['start_date']) && isset($filters['end_date']) && !blank($filters['end_date']) ) {
            $q->where(function($q1)use($filters,$timezone){
              $q1->whereRaw("DATE(CONVERT_TZ(orders.created_at,'UTC','$timezone')) >= '{$filters['start_date']}' AND DATE(CONVERT_TZ(orders.created_at,'UTC','$timezone')) <= '{$filters['end_date']}'");
            });
          }
        }
        if (isset($filters['delivery_pickup_type']) && !blank($filters['delivery_pickup_type'])) {
          $q->where("delivery_pickup_type", $filters['delivery_pickup_type']);
        }
        if (isset($filters['search']) && !blank($filters['search'])) {
          $q->where(function($q1)use($filters){
            $q1->whereRaw("CONCAT(customers.user_name,' ',customers.last_name) LIKE '%{$filters['search']}%'");
            $q1->orwhereRaw("customers.user_name LIKE '%{$filters['search']}%'");
            $q1->orwhereRaw("customers.last_name LIKE '%{$filters['search']}%'");
            // $q1->orWhere("users.user_name", "LIKE" , "%{$filters['search']}%");
            $q1->orWhereRaw("CONCAT('#',orders.id) LIKE '%{$filters['search']}%'");
          });
        }
      });
      if (isset($filters['status']) && !blank($filters['status']) && $filters['status'] == 'Scheduled') {
        $order = $order->orderBy('orders.future_delivery_date','ASC');
      } else {
        $order = $order->orderBy('orders.id','DESC');
      }
      // ->orderBy('orders.id','DESC');
      // dd($filters['driver_id']);
      if(isset($filters['driver_id']) && !blank($filters['driver_id'])){
        $q = "SELECT * FROM driver_order_request WHERE driverid = {$filters['driver_id']} AND driver_order_request.order_status = ".($filters['status']=='Delivered'?'7':'3')." GROUP BY orderid";
        $order->join(\DB::raw("($q) AS driver_order_request"),'driver_order_request.orderid','=','orders.id')
          ->where('driver_order_request.driverid',$filters['driver_id'])
          ->where('driver_order_request.order_status',$filters['status']=='Delivered'?'7':'3');
        
      }
      
      if($paginate){
        return $order->paginate(30);
      }
      return $order->get();
    }
 
    public function getOrdersCount($client_id, $timezone, $filters, $user_id = 0)
    {
      $cancelled = "SUM(IF(orders.order_status='Cancelled',1,0)) as cancelled_orders";
      if(isset($filters['driver_id']) && !blank($filters['driver_id']) && $filters['status'] == 'Cancelled' ){
        $cancelled = "COUNT(orders.id) as cancelled_orders";
      }

      $failed_orders = "SUM(IF(orders.order_status='PaymentFailure' OR orders.order_status='PaymentPending' OR orders.order_status='AwaitForPayment' ,1,0)) as failed_orders";
      if (isset($filters['abandonedstatus']) && $filters['abandonedstatus'] != '') {
        $failed_orders = "SUM(IF(orders.order_status='".$filters['abandonedstatus']."',1,0)) as failed_orders";
      }
      $order = $this->order
      ->selectRaw("
          SUM(IF(orders.order_status='Placed',1,0)) as placed_orders,
          SUM(IF(orders.future_delivery_date = '0000-00-00 00:00:00' AND orders.order_status='InKitchen',1,0)) as kitchen_orders,
          SUM(IF(orders.order_status='ReadyToServe',1,0)) as ready_orders,
          SUM(IF(orders.order_status='OnPickUp' OR orders.order_status='OnTheWay' OR orders.order_status='Arrived',1,0)) as ontheway_orders,
          SUM(IF(orders.order_status='Delivered',1,0)) as delivered_orders,
          $cancelled,
          $failed_orders,
          SUM(IF(orders.future_delivery_date != '0000-00-00 00:00:00' AND orders.order_status = 'InKitchen',1,0)) as scheduled_orders
      ")
      ->join('customers','customers.id','=','orders.user_id')
      ->join('restaurant','restaurant.id','=','orders.restaurant_id')
      // ->join('users','users.user_id','=','fa_restaurants.user_id')
      // ->where('users.vendor_id',$vendor_id)
      // ->where('restaurant.is_confirm','1')
      ->where('orders.client_id',$client_id)
      ->when($user_id !== 0,function($q)use($user_id){
        $q->where('restaurant.client_id',$user_id);
      })
      ->where(function ($q) use ($filters, $timezone) {
        if (isset($filters['start_date']) && !blank($filters['start_date']) && isset($filters['end_date']) && !blank($filters['end_date']) ) {

          $nowdate = \Carbon\Carbon::now()->timezone($timezone)->format('Y-m-d');
          $q->whereRaw(("
            CASE
              WHEN ((orders.future_delivery_date != '0000-00-00 00:00:00' || orders.future_delivery_date  != null) AND orders.order_status = 'InKitchen')
                THEN DATE(CONVERT_TZ(orders.created_at,'UTC','$timezone')) <= '{$nowdate}'
              ELSE DATE(CONVERT_TZ(orders.created_at,'UTC','$timezone')) >= '{$filters['start_date']}' AND DATE(CONVERT_TZ(orders.created_at,'UTC','$timezone')) <= '{$filters['end_date']}'
            END
          "));
        }
        if(isset($filters['user_id']) && !blank($filters['user_id'])){
          $q->where('orders.user_id',$filters['user_id']);
        }
        if (isset($filters['delivery_pickup_type']) && !blank($filters['delivery_pickup_type'])) {
          $q->where("orders.delivery_pickup_type", $filters['delivery_pickup_type']);
        }
        if (isset($filters['search']) && !blank($filters['search'])) {
          $q->where(function($q1)use($filters){
            $q1->whereRaw("CONCAT(customers.user_name,' ',customers.last_name) LIKE '%{$filters['search']}%'");
            $q1->orwhereRaw("customers.user_name LIKE '%{$filters['search']}%'");
            $q1->orwhereRaw("customers.last_name LIKE '%{$filters['search']}%'");
            // $q1->orWhere("users.user_name", "LIKE" , "%{$filters['search']}%");
            $q1->orWhereRaw("CONCAT('#',orders.id) LIKE '%{$filters['search']}%'");
          });
        }
      });
      if(isset($filters['driver_id']) && !blank($filters['driver_id'])){
        $q = "SELECT * FROM driver_order_request WHERE driverid = {$filters['driver_id']} AND driver_order_request.order_status = ".($filters['status']=='Delivered'?'7':'3')." GROUP BY orderid";
        $order->join(\DB::raw("($q) AS driver_order_request"),'driver_order_request.orderid','=','orders.id')
          ->where('driver_order_request.driverid',$filters['driver_id'])
          ->where('driver_order_request.order_status',$filters['status']=='Delivered'?'7':'3');
      }
      return $order->first();
    }

    public function getExportOrders($client_id, $timezone, $filters, $paginate = true,$user_id = 0)
    {

      $orders = $this->order
          ->selectRaw("orders.*,
          CONVERT_TZ(orders.created_at,'UTC','{$timezone}') as created_at,
          CONVERT_TZ(orders.confirm_date,'UTC','{$timezone}') as confirm_date,
          CONVERT_TZ(orders.ontheway_date,'UTC','{$timezone}') as ontheway_date,
          CONVERT_TZ(orders.delivered_date,'UTC','{$timezone}') as delivered_date,
          CONVERT_TZ(orders.arrived_date,'UTC','{$timezone}') as arrived_date
        ")
          ->whereHas('restaurant',function ($v) use($client_id,$user_id){
            $v->where('client_id',$client_id);
          })
          // ->has('customer')
          ->with([
            'restaurant' => function($q){
              $q->select('id','name','contact_number');
            },
            'customer' => function($q){
              $q->select('id','mobile_number','country_code','email')
                ->selectRaw("CONCAT(COALESCE(customers.user_name,''),' ',COALESCE(customers.last_name,'')) AS customer_name");
              },
            'reddemedCouponOrder' => function($q){
              $q->select('coupon_id','order_id','original_price','discount_price')->with(['coupon'=>function($q1){
                $q1->select('id','coupon_code');
              }]);
            },
            'orderItem' => function($q){
              $q->with([
                'masterItem' => function($q1){
                  $q1->select('id','item_name');
                },
                'menuItemSelections' => function($q1){
                  $q1->select('item_selection.id as menu_item_selection_id','selection_name','selection_name_thai','selection_price');
                }
              ]);
            },
          ])
          ->orderBy('id', 'desc')
          ->where(function ($q) use ($filters, $timezone) {
             $q->where(function($q1)use($filters){
              if (isset($filters['search']) && !blank($filters['search'])) {
                $q1->whereHas('restaurant' , function($q2)use($filters){
                  $q2->whereHas('user' , function($q3)use($filters){
                    $q3->whereRaw("CONCAT(users.user_name,' ',users.last_name) LIKE '%{$filters['search']}%'");
                  });
                })
                ->orWhereHas('customer',function($q2)use($filters){
                  $q2->whereRaw("CONCAT(customers.user_name,' ',customers.last_name) LIKE '%{$filters['search']}%'");
                })
                ->orWhereRaw("CONCAT('#',orders.id) LIKE '%{$filters['search']}%'");
              }
            });
            if (isset($filters['status']) && !blank($filters['status'])) {
              if($filters['status'] == "Scheduled"){
                // $q->where('future_delivery_date', '!=','0000-00-00 00:00:00');
                $q->where('future_delivery_date', '!=','0000-00-00 00:00:00')->where('order_status','InKitchen');
              }else if($filters['status'] == "Placed"){
                $q->where('order_status','Placed');
                // $q->where('future_delivery_date', '=','0000-00-00 00:00:00')->where('order_status','Placed');
              }else if($filters['status'] == "OnTheWay"){
                $q->where('orders.order_status', 'OnTheWay')->orWhere('orders.order_status', 'Arrived')->orwhere('orders.order_status','OnPickUp');
              }elseif ($filters['status'] == "InKitchen") {
                $q->where('future_delivery_date', '=','0000-00-00 00:00:00')->where('order_status','InKitchen');
              }else if($filters['status'] == "PaymentFailure"){
                // $q->where(function($q1){
                //   $q1->where('orders.order_status', 'PaymentFailure')->orwhere('orders.order_status', 'PaymentPending')->orwhere('orders.order_status', 'AwaitForPayment');
                // });
                if (isset($filters['abandonedstatus']) && $filters['abandonedstatus'] != '') {
                  $q->where('orders.order_status', $filters['abandonedstatus']);
                } else {
                  $q->whereIn('orders.order_status', ['PaymentFailure', 'PaymentPending', 'AwaitForPayment']);
                }
              }else{
                $q->where('orders.order_status', $filters['status']);
              }
              if(isset($filters['user_id']) && !blank($filters['user_id'])){
                $q->where('user_id',$filters['user_id']);
              }
              if (isset($filters['status']) && !blank($filters['status'])) {
                if ($filters['status'] != 'Scheduled' && isset($filters['start_date']) && !blank($filters['start_date']) && isset($filters['end_date']) && !blank($filters['end_date']) ) {
                  $q->whereRaw("DATE(CONVERT_TZ(orders.created_at,'UTC','$timezone')) >= '{$filters['start_date']}' AND DATE(CONVERT_TZ(orders.created_at,'UTC','$timezone')) <= '{$filters['end_date']}'");
                }
              }
              if (isset($filters['delivery_pickup_type']) && !blank($filters['delivery_pickup_type'])) {
                $q->where("delivery_pickup_type", $filters['delivery_pickup_type']);
              }
            }
          });
          // ->get();
          if (isset($filters['status']) && !blank($filters['status']) && $filters['status'] == 'Scheduled') {
            $orders = $orders->orderBy('orders.future_delivery_date','ASC');
          } else {
            $orders = $orders->orderBy('orders.id','DESC');
          }
          $orders = $orders->get();
        return $orders;
    }


    public function getOrder($client_id, $timezone, $order_id,$with = [])
    {
      $orders = $this->order
        ->where('id',$order_id)
        ->whereHas('restaurant', function($q)use($client_id){
          $q->whereHas('user',function($q1)use($client_id){
            $q1->where('id',$client_id);
          });
        })
        ->whereHas('customer',function($q)use($client_id){
          $q->where('client_id',$client_id);
        })
        ->with($with)
        ->first();
        // dd($orders);
        if(!blank($orders)){
          $orders->turnaround_time = '';
          $orders->delivered_time = '';
          $orders->kina_approval_code = '';
          $orders->kina_reference_number = '';
  
          if ($orders->order_status == 'Delivered') {
            if ($orders->created_at != '0000-00-00 00:00:00' && $orders->created_at != null && $orders->created_at != '') {
              if ($orders->delivered_date != '0000-00-00 00:00:00' && $orders->delivered_date != null && $orders->delivered_date != '') {
                $orders->turnaround_time = $this->timeFormate($orders->created_at, $orders->delivered_date, $timezone);
              }
            }
            if ($orders->ontheway_date != '0000-00-00 00:00:00' && $orders->ontheway_date != null && $orders->ontheway_date != '') {
              if ($orders->delivered_date != '0000-00-00 00:00:00' && $orders->delivered_date != null && $orders->delivered_date != '') {
                $orders->delivered_time = $this->timeFormate($orders->ontheway_date, $orders->delivered_date, $timezone);
              }
            }
          }
  
          if ($orders && $orders->id > 0) {
            if ($orders->user) {
              if ($orders->payment_method == 'Online' && (\App\Helper::settings($orders->user->client_id, 'payment_gateway') == 'kina' || $orders->payment_type == 'kina')) {
                $kina_transaction = DB::table('orders_transaction')->where('type', 'kina')->where('status', 'Success')->where('order_id', $orders->id)->first();
                if ($kina_transaction) {
                  $orders->kina_approval_code = ($kina_transaction->val_id != '' && $kina_transaction->val_id != null) ? $kina_transaction->val_id : ''; //Client bank’s approval code
                  $orders->kina_reference_number = ($kina_transaction->bank_tran_id != '' && $kina_transaction->bank_tran_id != null) ? $kina_transaction->bank_tran_id : ''; //Merchant bank’s retrieval reference number
                }
              }
            }
          }
  
          if(!blank($orders->created_at)){
            $orders->created_date = \Carbon\Carbon::parse($orders->created_at)->timezone($timezone)->format('M d, Y h:i A');
          }else{
            $orders->created_date = '0000-00-00 00:00:00';
          }
          if(!blank($orders->confirm_date)){
            $orders->confirm_at = \Carbon\Carbon::parse($orders->confirm_date)->timezone($timezone)->format('M d, Y h:i A');
          }else{
            $orders->confirm_at = '0000-00-00 00:00:00';
          }
          if(!blank($orders->readytoserve_date)){
            $orders->readytoserve_at = \Carbon\Carbon::parse($orders->readytoserve_date)->timezone($timezone)->format('M d, Y h:i A');
          }else{
            $orders->readytoserve_at = '0000-00-00 00:00:00';
          }
          if(!blank($orders->ontheway_date)){
            $orders->ontheway_at = \Carbon\Carbon::parse($orders->ontheway_date)->timezone($timezone)->format('M d, Y h:i A');
          }else{
            $orders->ontheway_at = '0000-00-00 00:00:00';
          }
          if(!blank($orders->delivered_date)){
            $orders->delivered_at = \Carbon\Carbon::parse($orders->delivered_date)->timezone($timezone)->format('M d, Y h:i A');
          }else{
            $orders->delivered_at = '0000-00-00 00:00:00';
          }
          if(!blank($orders->cancelled_date)){
            $orders->cancelled_date = \Carbon\Carbon::parse($orders->cancelled_date)->timezone($timezone)->format('M d, Y h:i A');
          }else{
            $orders->cancelled_date = '0000-00-00 00:00:00';
          }
          if(!blank($orders->arrived_date)){
            $orders->arrived_at = \Carbon\Carbon::parse($orders->arrived_date)->timezone($timezone)->format('M d, Y h:i A');
          }else{
            $orders->arrived_at = '0000-00-00 00:00:00';
          }
  
          // $orders->created_date = \Carbon\Carbon::parse($orders->created_at)->timezone($timezone)->format('M d, Y h:i A');
          // $orders->confirm_at = \Carbon\Carbon::parse($orders->confirm_date)->timezone($timezone)->format('M d, Y h:i A');
          // $orders->readytoserve_at = \Carbon\Carbon::parse($orders->readytoserve_date)->timezone($timezone)->format('M d, Y h:i A');
          // $orders->ontheway_at = \Carbon\Carbon::parse($orders->ontheway_date)->timezone($timezone)->format('M d, Y h:i A');
          // $orders->delivered_at = \Carbon\Carbon::parse($orders->delivered_date)->timezone($timezone)->format('M d, Y h:i A');
          // $orders->cancelled_date = \Carbon\Carbon::parse($orders->cancelled_date)->timezone($timezone)->format('M d, Y h:i A');
          // $orders->arrived_at = \Carbon\Carbon::parse($orders->arrived_date)->timezone($timezone)->format('M d, Y h:i A');
          if ($orders->future_delivery_date != null && $orders->future_delivery_date != '0000-00-00 00:00:00') {
            $orders->future_delivery_date = \Carbon\Carbon::parse($orders->future_delivery_date)->format('M d, Y h:i A');
          } else {
            $orders->future_delivery_date = '0000-00-00 00:00:00';
          }
          if ($orders->awaitpayment_date != null && $orders->awaitpayment_date != '0000-00-00 00:00:00') {
            $orders->awaitpayment_date = \Carbon\Carbon::parse($orders->awaitpayment_date)->timezone($timezone)->format('M d, Y h:i A');
          } else {
            $orders->awaitpayment_date = '0000-00-00 00:00:00';
          }
  
          $orderImages = OrderImages::where('order_id', $order_id)->get();
          $images = [];
          foreach ($orderImages as $key => $value) {
            $images[] = config('constant.OrderImage').$client_id."/".$value->image;
          }
  
          $date = \Carbon\Carbon::today()->subDays(29);
          $user_id = $orders->user->user_id;
          $last_thirty_days_order = $orders->where('user_id',$user_id)->where('created_at','>=',$date)->where('order_status','Delivered')->count();
          $orders->last_thirty_days_order = $last_thirty_days_order;
          $total_number_of_order_by_customer = $orders->where('user_id',$user_id)->where('order_status','Delivered')->count();
          $orders->total_order = $total_number_of_order_by_customer;
  
          // $user_chat = Chat::where('sender_id',$user_id)->orWhere('receiver_id',$user_id)->first();
          // $orders->chat = $user_chat;
          $orders->chat = '';
          $orders->order_images = $images;
          $driver_request = DriverOrderRequest::where('orderid',$order_id)->where('order_status','1')->count();
          $driver_cancel_request = DriverOrderRequest::where('orderid',$order_id)->where('order_status','3')->count();
          $driver_not_accept = DriverOrderRequest::where('orderid',$order_id)->where('order_status','4')->count();
          $reach_to_store = DriverOrderRequest::where('orderid',$order_id)->where('order_status','6')->first();
          $item_collected = DriverOrderRequest::where('orderid',$order_id)->where('order_status','11')->orwhere('order_status','9')->orwhere('order_status','6')->orwhere('order_status','7')->first();
          $arrived = DriverOrderRequest::where('orderid',$order_id)->where('order_status','9')->first();
          $item_collecteds = DriverOrderRequest::where('orderid',$order_id)->where('order_status','11')->first();
          $orders->driver_count = $driver_request;
          $orders->driver_cancel_count = $driver_cancel_request;
          $orders->driver_not_accept = $driver_not_accept;
          $orders->reach_to_store = $reach_to_store;
          $orders->item_collected = $item_collected;
          $orders->arrived = $arrived;
          $orders->items_collected = $item_collecteds;
          $orders->vendor_accept_time = \Carbon\Carbon::parse($orders->confirm_date)->diffForHumans($orders->created_at);
        }
        return $orders;
    }

    public function getNewOrders($client_id,$timezone,$user_id = 0)
    {
      return $this->order->
      selectRaw("orders.*,CONVERT_TZ(orders.created_at,'UTC','$timezone') AS created_at")
      ->whereHas('customer',function($q)use($client_id){
        $q->where('client_id',$client_id);
      })
      // ->whereHas('restaurant',function($q)use($user_id){
      //   $q->where('is_confirm',1)
      //     ->when($user_id !== 0,function($q)use($user_id){
      //       $q->whereHas('user', function($q1)use($user_id){ $q1->where('user_id',$user_id); });
      //     });
      // })
      ->where('order_status', 'Placed')
      ->where('client_id', $client_id)
      ->orderBy('id','desc')
      ->get();
    }

    public function getOrderDunzoTask($order_id) {
      return DunzoHistory::whereNull('deleted_at')->whereNull('cancelled_at')->where('order_id', $order_id)->first();
    }

    public function cancelOrder($orderId, $refundable = 0, $cancelled_desc = '', $is_language = 'en', $vendor_id) {
      $order = $this->order->find($orderId);
      $data = ['order_status' => 'Cancelled', 'orderId'=>$orderId,'cancelled_date' => date('Y-m-d H:i:s'), 'cancelled_desc' => $cancelled_desc, 'is_refundable' => $refundable, 'vendor_id' => $vendor_id, 'order_id'=>$orderId, 'is_langauge' => $is_language];

      $url = config('constant.api_url').'cancelOrder';
      info($url);
      $data = Http::post($url, $data);
      return $data->json();
    }

    public function sendToDunzo($vendor_id, $orderId) {
      $postdata = ['vendor_id' => $vendor_id, 'order_id'=>$orderId];
      $url = config('constant.api_url').'assignto-dunzo';
      $data = Http::post($url, $postdata);
      return $data->json();
    }

    public function assignDriver($user, $order_id, $ids) {
      foreach($ids as $driver_id){
        // DriverOrder::where('orderid',$order_id)->where('driverid','!=',$driver_id)->update(['order_status'=>'4']);
        $checkdriver = DriverOrder::where('orderid', $order_id)->where('driverid', $driver_id)->whereIn('order_status', [1,2,5,6,9,11])->first();
        if (empty($checkdriver)) {
          $driverRequest = DriverOrder::updateOrCreate(
            ['orderid' => $order_id, 'driverid' => $driver_id,'order_status' => '1'],
            ['order_status' => '1' ]
          );
        }
        // $this->notificationService->sendNotification('Driver_New_Request',$driver_id,$user->setting, 'en',['orderid'=>$order_id],'driver');
        $msg = $this->commonService->getNotificationMessage('Driver_New_Request', 'en',['orderid'=>$order_id],$user->setting);
        $msg = urlencode($msg);
        $url = config('constant.api_url')."driver_notification/$msg/$driver_id/driver/{$user->setting->vendor_id}/{$order_id}";
        \Log::info($url,[config('constant.api_base_v')]);
        exec("wget -bq --no-check-certificate -O /dev/null $url");
      }
    }

    public function deliverOrder($orderId, $delivered_note, $langauge = 'en', $vendor_id) {
      $data = ['orderId' => $orderId, 'delivered_note' => $delivered_note, 'vendor_id' => $vendor_id, 'is_langauge' => $langauge];
      $url = config('constant.api_url').'deliverOrder';
      info($url);
      $data =Http::post($url, $data);
      return $data->json();
    }

    public function changeToCod($orderId, $changeto_cod_note, $langauge = 'en', $vendor_id, $user_id) {
      $data = [
        'orderId' => $orderId,
        'changeto_cod_note' => $changeto_cod_note,
        'vendor_id' => $vendor_id,
        'is_langauge' => $langauge,
        'user_id' => $user_id
      ];
      $url = config('constant.api_url').'changeToCod';
      info($url);
      $data = Http::post($url, $data);
      return $data->json();
    }

    public function timeFormate($startdatetime, $enddatetime, $timezone = '') {
      if ($timezone == '') {
        $startTime = \Carbon\Carbon::parse($startdatetime);
        $endTime = \Carbon\Carbon::parse($enddatetime);
      } else {
        $startTime = \Carbon\Carbon::parse($startdatetime)->timezone($timezone);
        $endTime = \Carbon\Carbon::parse($enddatetime)->timezone($timezone);
      }
      $seconds = $startTime->diffInSeconds($endTime);
      $minutes = $startTime->diffInMinutes($endTime);
      $hours = $startTime->diffInHours($endTime);
      $time = '';
      // if ($hours == 0 || $hours == '0') {
      //   if ($minutes == 0 || $minutes == '0') {
      //     if ($seconds == 0 || $seconds == '0') {
      //       $time = 'In 0 Second';
      //     } else {
      //       $time = ($seconds > 1) ? $seconds.' Seconds' : $seconds.' Second';
      //     }
      //   } else {
              $time = ($minutes > 1) ? $minutes.' Minutes' : $minutes.' Minute';
      //   }
      // } else {
      //   $time = ($hours > 1) ? $hours. ' Hours' : $hours. ' Hour';
      // }
      // echo 'seconds: '.$seconds.' minutes: '.$minutes.' hours: '.$hours.' TIME:- '.$time;die();
      return $time;
    }

    public function getOrderStatus($orderId) {
      $order = $this->order->find($orderId);
      $status = '';
      if ($order) {
        $status = $order->order_status;
      }
      return $status;
    }
}

