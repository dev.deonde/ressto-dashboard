<?php

namespace App\Http\Services;

use App\Models\MasterMenu;
use App\Models\MasterMenuTimeSlots;

class MenuService
{
    public function storeMenu($data)
    {
        return MasterMenu::create($data);
    }

    public function storeMenuTime($data)
    {
        return MasterMenuTimeSlots::create($data);
    }

    public function getMasterMenu($user, $skip=0, $limit=100){
        return MasterMenu::where('client_id',$user->id)->orderBy('reorder_data','ASC')->skip($skip)->paginate($limit);
    }

    public function getMenuById($user,$menu_id){
        return MasterMenu::where('id',$menu_id)->first();
    }

    public function getMenuTime($master_menu_id){
        return MasterMenuTimeSlots::where('master_menu_id', $master_menu_id)->first();
    }

    public function updateMenu($id,$data)
    {
        MasterMenu::where('id',$id)->update($data);
        return MasterMenu::where('id',$id)->first();
    }

    public function updateMasterMenuStatus($id,$data)
    {
        MasterMenu::where('id',$id)->update($data);
        return MasterMenu::where('id',$id)->first();
    }

    public function getMenuWhere($where){
        return MasterMenu::where($where)->get();
    }
}