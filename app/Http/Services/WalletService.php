<?php
namespace App\Http\Services;

use App\Models\Customer;
use App\Models\WalletHistory;
use App\Models\CustomerWallet;



class WalletService {
    protected $wallet,$walletHistory,$customer;

    public function __construct(CustomerWallet $wallet, Customer $customer, WalletHistory $walletHistory){
        $this->wallet = $wallet;
        $this->customer = $customer;
        $this->walletHistory = $walletHistory;
    }

    public function getBalance($customer_id){
        return $this->wallet->where('customer_id',$customer_id)->first();
    }

    public function changeWalletStatus($customer_id, $status)
    {
        return $this->customer->where('id',$customer_id)->update(['wallet_status' => $status]);
    }

    public function addHistory($data){
        return $this->walletHistory->create($data);
    }

    public function refundWallet($customer_id, $order_id,$amount){
        $wallet = $this->wallet->where('customer_id',$customer_id)->first();
        $wallet->wallet_balance = $wallet->wallet_balance + $amount;
        $wallet->save();
        return $this->walletHistory->create([
            'recharge_amount'   =>  $amount,
            'recharge_status'   =>  'success',
            'customer_id'       =>  $customer_id,
            'transaction_id'    =>  'refund_'.\Str::uuid(),
            'type'              => 'refund',
            'user_order_id'     =>  $order_id
        ]);
    }

}