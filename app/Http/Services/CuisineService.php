<?php

namespace App\Http\Services;

use App\Models\Cuisine;

class CuisineService
{
    public function storeCuisine($data)
    {
        return Cuisine::create($data);        
    }
}