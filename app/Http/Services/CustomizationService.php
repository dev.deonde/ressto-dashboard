<?php

namespace App\Http\Services;

use App\Models\CustomizationType;
use App\Models\ItemSelection;

class CustomizationService
{

    public function getitemCustomization($where){
        return CustomizationType::with('Menu_customization')->where($where)->get();
    }

    public function storeCustomizationType($data)
    {
        return CustomizationType::create($data);
    }

    public function getCustomization($where){
        return CustomizationType::where($where)->first();
    }

    public function updateCustomization($id,$data)
    {
        CustomizationType::where('id',$id)->update($data);
        return CustomizationType::where('id',$id)->first();
    }

    public function getItemSelection($where){
        return ItemSelection::where($where)->get();
     }

    public function storeMenuItemSelection($data){
        return ItemSelection::create($data);
    }

    public function updateMenuItemSelection($where,$data){
        ItemSelection::where($where)->update($data);
        return ItemSelection::where($where)->first();
    }

    public function deleteItemCustomization($where)
    {
        return ItemSelection::where($where)->delete();
    }

    public function getTemplate($get_item,$searchtext){

        return CustomizationType::where('is_template','1') 
            ->where(function($q) use($searchtext){
            if($searchtext != ''){   
                $q->where('template_name','LIKE','%'.$searchtext.'%'); 
            }
        })->groupby('template_name')->get();
    }

    public function deleteCustomization($where)
    {
        return CustomizationType::where($where)->delete();
    }

    public function getItemSelectionById($where){
        return ItemSelection::where($where)->first();
     }
}