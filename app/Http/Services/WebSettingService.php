<?php

namespace App\Http\Services;

use App\Models\WebSetting;

class WebSettingService
{
    protected $frontWebUrl = ".";
    public function __construct(){
        $this->frontWebUrl .= config('constant.sub_url');
    }

    public function getSetting($clientId)
    {
        $settings = WebSetting::where('client_id', $clientId)->get();
        $data = [];
        if (!blank($settings)) {
            $data['web_logo_image_path'] = $settings[0]->web_logo_image_path;
            $data['web_home_page_image_path'] = $settings[0]->web_home_page_image_path;
            $data['section_image_path'] = $settings[0]->section_image_path;
            foreach ($settings as $value) {
                if($value->settings_key == "website_url"){
                    $data = array_merge([$value->settings_key => str_replace($this->frontWebUrl,'',$value->setting_values)], $data);
                }else{
                    $data = array_merge([$value->settings_key => $value->setting_values], $data);
                }
            }
        }

        return $data;
    }

    public function creatOrUpdate($clientId, $data)
    {
        $response = [
            'status' => true,
            'message' => 'Web Setting Update Successfully',
            'setting' => []
        ];
        foreach ($data as $key => $value) {
            
            if ($key == "website_url" && $this->checkWebsiteUrl($clientId, $key, $value)) {
                $response['status'] = false;
                $response['message'] = 'Entered website URL is already in use, Please enter something else.';
                return $response;
            }
            WebSetting::updateOrCreate(
                [
                    'client_id' => $clientId,
                    'settings_key' => $key,
                ],
                [
                    'setting_values' => !blank($value)?$value:''
                ]
            );
            $response['setting'] = $this->getSetting($clientId);
        }
        return $response;
    }

    public function checkWebsiteUrl($clientId, $key, $value)
    {
        return !blank(WebSetting::whereNot('client_id', $clientId)->where('settings_key', $key)->where('setting_values', $value)->first());
    }

    public function removekey($clientId, $key){
        return WebSetting::where('client_id',$clientId)->where('settings_key',$key)->delete();
    }   
}
