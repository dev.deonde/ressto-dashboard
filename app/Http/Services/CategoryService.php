<?php

namespace App\Http\Services;

use App\Models\MasterCategory;
use App\Models\MasterItem;

class CategoryService
{
    
    public function storeCategory($data)
    {
        return MasterCategory::create($data);
    }

    public function getMasterCategory($user,$id)
    {
        return MasterCategory::where('id',$id)->first();
    }

    public function editMasterCategory($id,$data)
    {
        MasterCategory::where('id',$id)->update($data);
        return MasterCategory::where('id',$id)->first();
    }

    public function delete($id)
    {
        $master_category = MasterCategory::where('id',$id)->first();
        MasterItem::where('main_category_id',$master_category->id)->delete();
        $master_category->delete();
        return $master_category;
    }

    public function getCategory($user,$menu_id,$category_id,$search = ""){
        return MasterCategory::
            withCount(['item'=>function($q)use($search){
                if($search != ''){
                    return $q->where('item_name','LIKE','%'.$search.'%')
                    ->orWhere('item_name_thai','LIKE','%'.$search.'%')
                    ->orWhere('item_description','LIKE','%'.$search.'%');
                }
            }])->where(function ($query) use ($menu_id){
                if($menu_id != ''){
                    $query->where('menu_id',$menu_id);
                }
            })
            ->where('client_id',$user->id)->orderBy('reorder_data','ASC')->get();
    }

    public function getMasterCategoryWhere($where){
        return MasterCategory::where($where)->get();
    }
}