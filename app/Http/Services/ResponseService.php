<?php

namespace App\Http\Services;

class ResponseService
{
    public function response($data,$message,$code = 200)
    { 
        return response()->json(['code' => $code,'message' => $message ,'result' => $data]);
    }
}
?>