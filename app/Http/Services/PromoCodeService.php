<?php

namespace App\Http\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Http\Services\ResponseService;
use App\Models\PromoCode;
use App\Models\Customer;
use Carbon\Carbon;

class PromoCodeService
{
    protected $responseService;
    protected $commonService;
 
    public function __construct(ResponseService $responseService, CommonService $commonService)
    {
        $this->responseService = $responseService;
        $this->commonService = $commonService;
    }

    public function promoCodeListing($user,$search,$status,$sortBy,$orderBy,$total_record)
    { 
        $timezone = $user->setting->where('settings_key','timezone')->first()->setting_values;
        
        $promoCode =  PromoCode::where(function($q) use($search){
            if($search != '' && !blank($search)){
                $q->where('coupon_name','LIKE',"%{$search}%"); 
                $q->Orwhere('coupon_code','LIKE',"%{$search}%");
                $q->Orwhere('discount_percentage','LIKE',"%{$search}%");
                $q->Orwhere('flat_discount','LIKE',"%{$search}%");
                $q->Orwhere('max_user_count','LIKE',"%{$search}%");
            }
        })->where(function ($q) use($status,$timezone){
            if($status != '' && $status == '1' && !blank($status)){
                $q->where('status',"$status");
                $q->where('end_datetime', '>' , Carbon::now()->timezone($timezone)->format('Y-m-d H:i:s'));
            }
            if($status != '' && $status == '0' && !blank($status)){
                $q->where('status',"$status");
            }
            if($status != '' && $status == '-1' && !blank($status)){
                $q->where('status','1');
                $q->where('end_datetime', '<' , Carbon::now()->timezone($timezone)->format('Y-m-d H:i:s'));
            }
        })->where('client_id',$user->myid())
        ->orderBy($sortBy, $orderBy) 
        ->paginate($total_record);

        return [
            'data' => $promoCode,
            'promoCodeCount' => $promoCode->count()
        ];
    }

    public function customer($user,$search)
    {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '-1');
        $getcustomer = Customer::where(function($q) use($user){
            $q->where('client_id',$user->myid());
            $q->where('status','0');
            $q->where('user_role','1');
        })
        ->where(function($c) use($search){
            if($search != ''){
                $c->where('user_name','LIKE',"%{$search}%");
                $c->Orwhere('last_name','LIKE',"%{$search}%");
            }
        })->get();
        return $getcustomer;
    }

    public function storePromoCode($data)
    {
        return PromoCode::create($data);
    }

    public function syncCustomer($coupon, $customer){
        $coupon->couponCustomer()->sync($customer);
    }

    public function getPromoCodeWhere($where){
        return PromoCode::where($where)->get();
    }

    public function getPromoCode($user,$id)
    {   
        return PromoCode::with('couponCustomer','getCustomer')->where('id',base64_decode($id))->where('client_id',$user->myid())->first();            
    }

    public function updatePromoCode($id,$data){
        $coupon = PromoCode::where('id', $id)->first();
        return tap($coupon)->update($data);
    }

}
?>