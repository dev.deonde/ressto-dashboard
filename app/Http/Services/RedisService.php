<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Redis;

class RedisService
{
    public function get($key=null){
        $data = null;
        if(!blank($key)){
            $data = json_decode(Redis::get($key.':'.auth()->user()->myid()));
        }
        return $data;

    }
    
    public function set($key, $value=''){
        if(!blank(env('REDIS_DEFAULT_EXPIRATION'))){
            return Redis::setex($key.':'.auth()->user()->myid(), env('REDIS_DEFAULT_EXPIRATION') ,json_encode($value));
        }else{
            return Redis::set($key.':'.auth()->user()->myid(),json_encode($value));
        }

    }

    public function del($key=null){
        if(!blank($key)){
            Redis::del($key.':'.auth()->user()->myid());
        }

    }
}
?>