<?php

namespace App\Http\Services;

use DB;
use Carbon\Carbon;
use App\Http\Models\Order;
use App\Models\Customer;


class CustomerService
{
    protected $commonService = null;

    public function __construct(CommonService $commonService)
    {
        $this->commonService = $commonService;

    }
    public function customerListing($user, $search, $status, $sortBy, $orderBy, $total_record){
        
        $prefix = $this->commonService->getVendorByBusinessCode($user->setting->where('settings_key','app_name')->first()->setting_values);
        $customercount = Customer::
        withCount('orders')
        ->where(function($q) use($search,$prefix){
            if($search != ''){
                $q->where(DB::raw("CONCAT('{$prefix}C', unique_code)"),'LIKE','%'.$search.'%');
                $q->Orwhere('user_name','LIKE','%'.$search.'%');
                $q->Orwhere('last_name','LIKE','%'.$search.'%');
                $q->Orwhere(DB::raw('CONCAT(user_name, " ", last_name)'),'LIKE','%'.$search.'%');
                $q->Orwhere('mobile_number','LIKE','%'.$search.'%');
                $q->Orwhere('email','LIKE','%'.$search.'%');   
            }
        })->where(function ($q) use($status){
            if($status != ''){
                $q->where('status',$status);
            }
        })
        ->where('client_id',$user->myid())
        ->where('user_role', '1')
        ->orderBy($sortBy, $orderBy)
        ->count();
        
        $customer = Customer::
        where(function($q) use($search,$prefix){
            if($search != ''){
                $q->where(DB::raw('CONCAT("'.$prefix.'C", unique_code)'),'LIKE','%'.$search.'%');
                $q->Orwhere('user_name','LIKE','%'.$search.'%');
                $q->Orwhere('last_name','LIKE','%'.$search.'%');
                $q->Orwhere(DB::raw('CONCAT(user_name, " ", last_name)'),'LIKE','%'.$search.'%');
                $q->Orwhere('mobile_number','LIKE','%'.$search.'%');
                $q->Orwhere('email','LIKE','%'.$search.'%');   
            }
        })->where(function ($q) use($status){
            if($status != ''){
                $q->where('status',$status);
            }
        })
        ->selectRaw('customers.id,customers.user_name,customers.last_name, customers.email, CONCAT(customers.country_code," ",customers.mobile_number) as mobile_number , customers.unique_code , customers.created_at, customers.status,SUM(IF(orders.order_status = "Delivered",1,0)) as order_count, IF(shipment_address.is_primary_address = "1",shipment_address.address,"") as address')
        ->leftjoin('orders','orders.user_id','=','customers.id')
        ->leftjoin('shipment_address','shipment_address.user_id','=','customers.id')
        ->where('customers.client_id',auth()->guard('client')->user()->myid())
        //     ->where('shipment_address.is_primary_address',1)   
        ->groupBy('customers.id')
        ->orderBy($sortBy, $orderBy)
        ->paginate($total_record);
        $app_name = $user->setting->where('settings_key','app_name')->first()->setting_values;
        foreach($customer as $key => $cus){
            $customer[$key]['unique'] =  $this->commonService->getVendorUniqueCode($app_name,$cus->unique_code,$type="customer");
        }
        return [
            'data' => $customer,
            'customerCount' => $customercount
        ];
    }

    public function exportToCsv($user, $status, $sortBy='user_id', $orderBy="asc")
    {
        $app_name = $user->setting->where('settings_key','app_name')->first()->setting_values;
        $prefix = $this->commonService->getVendorByBusinessCode($app_name);
        $app_name = $app_name;
        $customer =  Customer::
        where(function ($q) use($status){
            if($status != ''){
                $q->where('customers.status',$status);
            }
        })
        ->selectRaw('customers.id,customers.user_name,customers.last_name, customers.email, CONCAT(customers.country_code," ",customers.mobile_number) as mobile_number , customers.unique_code , customers.created_at, customers.status,SUM(IF(orders.order_status = "Delivered",1,0)) as order_count,shipment_address.address')
        ->leftjoin('orders','orders.user_id','=','customers.id')
        ->leftjoin('shipment_address','shipment_address.user_id','=','customers.id')
       // ->where('shipment_address.is_primary_address',1)
        ->where('customers.client_id',auth()->guard('client')->user()->myid())
        ->groupBy('customers.id')
        ->orderBy($sortBy, $orderBy)
        ->get();

        foreach($customer as $key => $cus){
            $customer[$key]['unique'] =  $this->commonService->getVendorUniqueCode($app_name,$cus->unique_code,$type="customer");
        }

        return $customer;
    }

    public function customerDetail($user_id){
        $get_customer_profile =  Customer::where('id',base64_decode($user_id))->first();
        // // foreach($get_customer_profile as $key => $value)
        // // {
        //     // dd($value);
        //     $get_customer_profile->mobile_number = $get_customer_profile->mobile_number;
        // // }
        return $get_customer_profile;
    }

    public function updateCustomer($id, $data)
    {
        $update = Customer::where('id',$id)->update($data);
    }

    public function updateProfile($user_id, $user_name, $last_name, $email, $mobile_number, $status, $is_cod_enable){
        $update = Customer::where('user_id',$user_id)->update([
            'user_name' => $user_name, 
            'last_name' => $last_name, 
            'email' => $email, 
            'mobile_number' => $mobile_number,
            'status' => $status,
            'is_cod_enable' => $is_cod_enable
        ]);
        if($update){
            return $update;
        }else{
            return '';
        }
    }

    public function orderListing($user_id, $order_status='Delivered', $search, $sortby, $orderby, $total_record){

        $orders = Order::with('restaurant:fa_restaurants.restaurant_id,name')->where(function($q) use($search){
            if($search != ''){
                $q->where(DB::raw('CONCAT(user_name, " ", last_name)'),'LIKE','%'.$search.'%');
                $q->Orwhere('mobile_number','LIKE','%'.$search.'%');
                $q->Orwhere('email','LIKE','%'.$search.'%');   
            }
        })->whereHas('customer',function ($r) use($user_id){
            $r->where('user_id',base64_decode($user_id));
        })->whereHas('driver_order',function ($qur){
            $qur->where('order_status','7')->whereHas('driver')->with('driver');
        })->with(['driver_order'=>function ($dr) {
            $dr->where('order_status','7')->whereHas('driver')->with('driver');
        }])
        ->where('order_status', $order_status)
        ->orderBy($sortby, $orderby)
        ->paginate($total_record);

        return $orders;
    }
}
