<?php

namespace App\Http\Services;

use App\Models\VendorSetting;

class VendorSettingService
{
    public function storeVendorSettings($data)
    {
        return VendorSetting::create($data);
    }

    public function clientVendorSettings($key,$value)
    {
        return VendorSetting::updateOrCreate(
            [
                'client_id'=>auth()->user()->myid(),
                'settings_key'=>$key,                
            ],
            [
                'setting_values'=>$value,
            ]
        );
    }
    
    public function deleteVendorSettingKey($key){
        return VendorSetting::where('client_id',auth()->user()->myid())->where('settings_key',$key)->delete();
    }
    
}
