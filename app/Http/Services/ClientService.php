<?php

namespace App\Http\Services;

use App\Models\Client;
use App\Models\Restaurant;
use App\Models\WebSetting;

class ClientService
{
    public function storeClient($data)
    {
        return Client::create($data);
    }

    public function login($email,$password,$client_id, $guard = "client") {
        $token = "";
        $data = ["email" => $email, "password" => $password];
        if ($client_id != 1 && $client_id != null && $client_id != '') {
            $data = ["email" => $email, "password" => $password, 'id' => $client_id];
        }
        if ($guard == 'client') {
            $get_client = Client::where('email',$email)->first();

            if ($get_client) {
                if ($get_client->parent_client_id != null) {
                    $data = ["email" => $email, "password" => $password, 'parent_client_id' => $get_client->parent_client_id];
                }
                if ($get_client->status == 2) {
                    if ($token = auth($guard)->attempt($data)) {
                        return ['user'=>auth($guard)->user(), 'token'=> $token,'is_login'=> 1];
                    } else {
                        return ['user'=>(object)[], 'token'=> $token,'is_login'=>3];
                    }
                } else if ($get_client->status == 0) {
                    return ['user'=>(object)[], 'token'=> $token,'is_login'=>0];    
                } else {
                    return ['user'=>(object)[], 'token'=> $token,'is_login'=>2];
                }                
            } else {
                return ['user'=>(object)[], 'token'=> $token,'is_login'=>3];
            }
        } else {
            if ($token = auth($guard)->attempt($data + ['status' => '0'])) {
                return ['user'=>auth($guard)->user(), 'token'=> $token,'is_login'=> 1];
            } elseif(Restaurant::where([['email','=',$email],['status','=','1']])->count() > 0){
                return ['user'=>(object)[], 'token'=> $token,'is_login'=> 0];
            }
            return ['user'=>(object)[], 'token'=> $token,'is_login'=>3];
        }
    }

    public function logout(){
        session()->flush();
        return auth()->logout();
    }

    public function updatedClient($where, $updat){
        return Client::where($where)->update($updat);
    }

    public function getClientWithWhere($where){
        return Client::where($where)->first();
    }


    public function getClientFromHost($clientHost) {
        return Client::where('project_name', $clientHost)->first();
    }

    public function clientlogin($email,$client_id, $guard = "client") {
        /*
        0 - blocked 
        1 - login success
        2 - not active user ( NOT VERIFIED)
        3 - not authenticated (email password wrong)
        */
        $token = "";
        $getVendor = Client::where('email', $email)->where('id', $client_id)->whereNull('parent_client_id')->first();
        if (!blank($getVendor)) {             
            if ($token = auth($guard)->login($getVendor)) {                
                return ['user'=>auth($guard)->user(), 'token'=> $token,'is_login'=> 1];
            } else {                
                return ['user'=>(object)[], 'token'=> $token,'is_login'=>3];
            }            
        } else {
            return ['user'=>(object)[], 'token'=> $token,'is_login'=>3];
        }
    }
    
    public function storeURLSetting($data){
        return WebSetting::create($data);
    }
}