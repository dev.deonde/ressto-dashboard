<?php

namespace App\Http\Services;

use App\Enums\RedisKeysEnum;
use Illuminate\Support\Facades\Http;

class ResstoApiService extends RedisService
{
    // public function __construct(RedisService $redisService){
    //     $this->redisService = $redisService;
    // }
    public function setMenuItems(){
        $this->del(RedisKeysEnum::API_MENU_ITEMS->value);        
        $menuItems = Http::post(config('constant.api_url').'master_menus',[
            'client_id' => auth()->user()->myid(),
            'is_langauge' => 'en',
            'date' => '',
            'starttime' => '' ,
        ]);
    }

    public function setMasterItems($item_id){
        $this->del(RedisKeysEnum::API_MENU_ITEMS_by_id->value.':'.$item_id);        
        $menuItems = Http::post(config('constant.api_url').'master_item_details',[
            'client_id' =>auth()->user()->myid(),
            'item_id' =>$item_id,
            'is_langauge' =>'en',
            'date' =>'',
            'starttime' =>'',
        ]);         
    }
}