<?php

namespace App\Http\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Http\Services\ResponseService;
use App\Models\RestaurantMenu;
use App\Models\MasterItem;
use App\Models\RestaurantMenuItemImage;
use App\Models\OrderItem;
use App\Models\User;
use App\Models\Restaurant;
use App\Models\Order;
use App\Models\Driver;
use App\Models\Customer;
use App\Models\Vendor;
use App\Models\Coupon;
use Carbon\Carbon;
use DB,File;

class DashboardService
{
    protected $responseService;
    protected $commonService;
 
    public function __construct(ResponseService $responseService, CommonService $commonService)
    {
        $this->responseService = $responseService;
        $this->commonService = $commonService;
    }

    public function getDashboardList($timezone) {
        $items = [];
        $items_menu = [];        
        $total_delivered = 0;
        $totalVendor = 0;
        $totalDriver = 0;
        $totalUsers = 0;
        $totalZone = 0;
        $thirty_vendor = [];
        $thirty_driver = [];
        $ninty_customer = [];
        $total_store = 0;
        $total_rider = 0;
        $total_customer = 0;
        $total_delivery_zone = 0;
        $total_order_delivered = 0;        
        $total = 0;
        $total_order_amount = 0;
        $total_earning = 0;
        $total_pending = 0;
        $active_coupon_count = 0;
        $top_customers = [];
        if(auth()->guard('client')->user()) {
            $client_id = auth()->guard('client')->user()->myid();
            $timezone = \App\Helper::timezone($client_id);
            $country = Country::where([['client_id',$client_id],['status',1]])->pluck('id')->toArray();
            $state = State::where([['client_id',$client_id],['status',1]])->pluck('id')->toArray();
            $city = City::where('client_id',$client_id)->pluck('id')->toArray();
            if (auth()->guard('client')->user()->client_role == 2) {
                $total = DB::table('orders')
                ->join('restaurant', 'restaurant.id', '=', 'orders.restaurant_id')
                ->join('client', 'client.id', '=', 'restaurant.client_id')
                ->where('client.id',auth()->guard('client')->user()->myid())                
                ->where('orders.order_status', 'Delivered');                
                $total_delivered = $total->count();                
                $total_store = Restaurant::count();
                $total_rider = Driver::where('is_block', '0')->count();
                $total_customer = Customer::where('client_id',$client_id)->where('status','0')->count();
                // $total_delivery_zone = User::where([['user_role','3'],['status','0']])->count();
                // $total_order_delivered = Order::where('order_status','Delivered')->count();
                // $total_delivered = Order::whereHas('customer',function($q)use($client_id){
                //     $q->where('vendor_id',$client_id);
                // })->whereHas('restaurant',function($q){
                //     $q->where('is_confirm','1');
                // })
                // ->where('order_status','Delivered')->count();

                // $total = Order::whereHas('customer',function($q)use($client_id){
                //     $q->where('vendor_id',$client_id);
                // })->whereHas('restaurant',function($q){
                //     $q->where('is_confirm','1');
                // })
                // ->where('order_status','Delivered');

                $total_order_amount = $total->sum('total_amount');
                $total_earning = $total->sum('orders.admin_commision');
                // $totalUsers = Customer::where('vendor_id',auth()->guard('vendor')->user()->myid())->where('status','0')->count();
                // $totalVendor = Restaurant::join('users', 'users.user_id', '=', 'fa_restaurants.user_id')
                //     ->where('users.vendor_id',auth()->guard('vendor')->user()->myid())
                //     ->where('is_confirm',1)->where('users.user_id', '!=', 1)->count();
                // $totalZone = User::where('vendor_id',auth()->guard('vendor')->user()->myid())->where('user_role','3')->where('status','0')->count();
                
                // $frenchiseIds = User::where('vendor_id',auth()->guard('vendor')->user()->myid())
                //     ->where('user_role','3')
                //     ->where('users.status','0')
                //     ->pluck('user_id');

                // $totalZone = User::where('vendor_id',auth()->guard('vendor')->user()->myid())->where('user_role','3')->where('status','0')->count();
                // $totalZone = User::where('vendor_id',auth()->guard('vendor')->user()->myid())
                //             ->where('user_role','3')
                //             ->where('status','0')
                //             ->where(function ($q) use ($country){
                //                 if($country[0] != '0'){
                //                     $q->where('country',$country[0]);
                //                 }
                //             })
                //             ->count();
                // $thirty_vendor = Order::
                // join('fa_restaurants', 'fa_restaurants.restaurant_id', '=', 'fa_order.restaurant_id')
                // ->join('users', 'users.user_id', '=', 'fa_restaurants.user_id')
                // ->where('order_status', 'Delivered')
                // ->where('users.vendor_id',auth()->guard('vendor')->user()->myid())
                // ->whereDate('delivered_date', '>', Carbon::now()->subDays(30))
                // ->groupBy('fa_order.restaurant_id')
                // ->select('fa_order.*','fa_restaurants.*', DB::raw('count(fa_order.restaurant_id) as vendor_order_count'))
                // ->orderby('vendor_order_count', 'desc')
                // ->take(5)
                // ->get();

                // $thirty_driver = Driver::
                // join('fa_driver_order_request','fa_driver_order_request.driverid','=','fa_driver.driverid')
                // ->join('users', 'users.user_id', '=', 'fa_driver.frenchise_id')
                // ->where('users.status','0')
                // ->where('users.vendor_id',auth()->guard('vendor')->user()->myid())
                // ->where('fa_driver_order_request.order_status','7')
                // ->whereDate('fa_driver_order_request.createddate', '>', Carbon::now()->subDays(30))
                // ->groupBy('fa_driver_order_request.driverid')
                // ->select('fa_driver.driverid', 'fa_driver.firstname', 'fa_driver.lastname','fa_driver.profileimage',  
                // DB::raw('count(DISTINCT(fa_driver_order_request.orderid)) as driver_order_count'))
                // ->orderby('driver_order_count', 'desc')
                // ->take(5)
                // ->get();
                // $active_coupon_count = Coupon::where('vendor_id',auth()->guard('vendor')->user()->myid())
                //  ->where('status','1')
                //  ->where('end_datetime', '>' , Carbon::now()->timezone($timezone)->format('Y-m-d H:i:s'))
                //  ->count();
                $get_restaurant = Restaurant::where('client_id',$client_id)->first();                
                $items = [];
                $get_item = Order::where('restaurant_id',$get_restaurant->id)->whereDate('delivered_date', '>', Carbon::now()->subDays(180))->pluck('id');                
                $order_item = OrderItem::whereIn('order_id',$get_item)->groupby('master_menu_item_id')->take(5)->get();
                
                $top_customers = Order::selectRaw("orders.user_id,sum(orders.total_amount) as ttlAmt,customers.user_name,customers.id,count(orders.id) as totalOrder")
                    ->leftjoin('customers','customers.id','=','orders.user_id')
                    ->where('orders.client_id',$client_id)
                    // ->where('orders.order_status','Delivered')
                    ->where('orders.created_at','>=',Carbon::now()->subMonth()->toDateTimeString())
                    ->groupBy('orders.user_id')
                    ->orderBY('ttlAmt','desc')
                    ->limit(10)
                    ->get();
                
                if(!empty($order_item))
                {
                    foreach($order_item as $key => $value)
                    {                        
                        $get_count = OrderItem::where('master_menu_item_id',$value->master_menu_item_id)->count();                        
                        if(!$get_count){
                            $get_count = 0;
                        }                        
                        $get_restaurant_item  = MasterItem::with('customization','item_image')->where('id',$value->master_menu_item_id)->get();                        
                        foreach($get_restaurant_item as $menu_name)
                        {                            
                            $items[$key]['count'] = $get_count;
                            $items[$key]['item_name'] = $menu_name->item_name;
                            if(!blank($menu_name->image_name))
                            {
                                $items[$key]['image'] = $menu_name->image[0]->image_name;
                            }
                        }
                    }
                }
            }
        }
        return [
            'total_delivered' => $total_order_delivered,
            'total_order_delivered' => $total_delivered,
            'total_order_pending' => $total_pending,
            'total_earning' => number_format($total_earning,2),
            'items' => $items,
            'items_menu' => $items_menu,
            'total_vendor' => $totalVendor,
            'total_delivery_boy' => $totalDriver,
            'total_customer' => $totalUsers,
            'total_zone' => $totalZone,
            'thirty_vendor'=>$thirty_vendor,
            'thirty_driver' => $thirty_driver,
            'ninty_customer' => $ninty_customer,
            'total_order_amount'=>  number_format($total_order_amount,2),
            'average_order_price'=>  number_format($total_order_amount/(($total_delivered == 0) ? 1 : $total_delivered),2),
            'total_store' => $total_store,
            'total_rider' => $total_rider,
            'total_customers' => $total_customer,
            'total_delivery_zone' => $total_delivery_zone,
            'active_coupon_count' => $active_coupon_count,
            'top_customers'=>$top_customers
        ];
    }
    
    public function getMonthwiseDeliveredOrder($client_id,$timezone,$user_id = 0) {
        $monthDates = [];
        // for ($i = 5; $i >= 0; $i--) {
        //     $month = Carbon::today()->startOfMonth()->subMonth($i)->format('m');            
        //     $start_of_month = Carbon::now($timezone)->startOfMonth($i)->subMonth($i);            
        //     $months = Carbon::now($timezone)->startOfMonth($i)->subMonth($i);
        //     $end_of_month = $months->endOfMonth($i);
        //     $year = Carbon::today()->startOfMonth()->subMonth($i)->format('y');
        //     $monthDates[] = [
        //         'x' => $month.'/'.$year,
        //         'start_date' =>  Carbon::parse($start_of_month)->format('Y-m-d H:i:s'),
        //         'end_date' => Carbon::parse($end_of_month)->format('Y-m-d H:i:s'),
        //     ];
        // }
        // if(blank($timezone)) {
        //     $timezone = '+05:30';
        // }
        // foreach($monthDates as $key => $monthDate)
        // {
        //     $startDate = $monthDate['start_date'];
        //     $endDate = $monthDate['end_date'];
        //     $order = Order::selectRaw("orders.id")
        //         ->join('customers','customers.client_id','=','orders.client_id')
        //         ->join('restaurant','restaurant.id','=','orders.restaurant_id')
        //         ->where('orders.order_status','Delivered')
        //         ->when($client_id !== 0,function($q)use($client_id){
        //             $q->where('restaurant.client_id',$client_id);
        //         })
        //         ->whereRaw("DATE(CONVERT_TZ(orders.created_at,'UTC','$timezone')) >= '{$startDate}' AND DATE(CONVERT_TZ(orders.created_at,'UTC','$timezone')) <= '{$endDate}'")
        //         ->groupby('orders.id')
        //         ->get();
        //     //$monthDates[$key]['y'] = $order->delivered_orders;
        //     $monthDates[$key]['y'] = count($order);
        // }                
            $order = Order::selectRaw("DATE(orders.created_at) as order_day, COUNT(*) as order_count")
            // ->where('orders.order_status','Delivered')
            ->where('orders.created_at','>=', Carbon::now()->subDays(30))
            ->when($client_id !== 0,function($q)use($client_id){
                $q->where('client_id',$client_id);
            })            
            ->groupby('order_day')
            ->orderby('order_day','DESC')
            ->get();             
            $newArray = $order->toArray();
            $currentData = Carbon::parse(Carbon::now($timezone)->format('Y-m-d'));
            $startData = Carbon::parse(Carbon::now($timezone)->subDays(31)->format('Y-m-d'));
           
            while ($startData <= $currentData) {
                if (in_array($startData->toDateString(), array_column($newArray, "order_day"))) {
                    $monthDates[] = [
                        'x' => Carbon::parse($startData->toDateString())->format('m/d/y'),
                        'y' => $newArray[array_search($startData->toDateString(), array_column($newArray, 'order_day'))]['order_count']
                    ];
                }else {
                    $monthDates[] = [
                        'x' => Carbon::parse($startData->toDateString())->format('m/d/y'),
                        'y' => 0
                    ];
                }                
                $startData->addDay();
            }
        return $monthDates;
    }

    public function getMonthwiseSales($client_id,$timezone = '+05:30',$chart,$user_id = 0, $month = 11)
    {
        $timezone = '+05:30';
        $monthDates = [];
        if($chart == 'sales')
        {
            if(auth()->guard('client')->user())
            {
                for ($i = $month; $i >= 0; $i--) {
                    $month = Carbon::today()->startOfMonth()->subMonth($i)->format('m');
                    $year = Carbon::today()->startOfMonth()->subMonth($i)->format('y');

                    $monthDates[] = [
                        'x' => $month.'/'.$year,
                        'start_date' =>  Carbon::create($year, $month)->startOfMonth()->format('y-m-d H:i:s'),
                        'end_date' => Carbon::create($year, $month)->lastOfMonth()->format('y-m-d H:i:s'),
                    ];
                }
                foreach($monthDates as $key => $monthDate)
                {
                    $startDate = $monthDate['start_date'];
                    $endDate = $monthDate['end_date'];
                    
                    $sales = Order::
                    selectRaw("
                        SUM(orders.total_amount) as amount,
                        DATE_FORMAT(CONVERT_TZ(orders.delivered_date,'UTC','$timezone'),'%M %Y') as month,
                        CONCAT(DATE_FORMAT(CONVERT_TZ(orders.delivered_date,'UTC','$timezone'),'%M %Y')) as month")
                    ->leftJoin('user_redeemed_coupons AS c','c.order_id','=','orders.id')
                    ->when($user_id !== 0,function($q)use($user_id) {
                        $q->join('restaurants','restaurants.restaurant_id','=','orders.restaurant_id')
                            ->where('restaurants.user_id',$user_id);
                    })
                    ->join('customers AS u','u.client_id','=','orders.client_id')
                    ->whereNotNull('orders.delivered_date')
                    ->whereRaw("DATE(CONVERT_TZ(orders.delivered_date,'UTC','$timezone')) >= '{$startDate}' AND DATE(CONVERT_TZ(orders.delivered_date,'UTC','$timezone')) <= '{$endDate}'")
                    // ->where('order.order_status','Delivered')
                    ->where('u.client_id',$client_id)
                    ->groupBy('month')
                    ->first();
                    if (!empty($sales)) {
                        $monthDates[$key]['y'] = sprintf("%0.2f",$sales->amount);
                    } else {
                        $monthDates[$key]['y'] = sprintf("%0.2f",0);
                    }
                }
            }
        }
        return $monthDates;
    }

}
?>