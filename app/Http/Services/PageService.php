<?php 

namespace App\Http\Services;

use App\Models\Page;

class PageService {

  public function pageList($clientid,$search,$sortby,$orderby = "desc", $total_record)
  {
    $page = Page::where(function($q) use($search){
      if($search != ''){
        $q->where('page_name','LIKE',"%{$search}%");
      }
    })->where('client_id',$clientid)
    ->orderBy($sortby, $orderby)
    ->paginate($total_record);
    return $page;
  }

  public function storeContentPage($data){
    return Page::create($data);
  }

  public function updateOldPageIdentifier($page_id,$client_id,$category){
    // 1: Terms & Condition , 2: Privacy policy, 3:Other
    if($category == 1 || $category == 2){
      return Page::where('id','!=',$page_id)->where('client_id',$client_id)->where('identifier',$category)->update(['identifier'=>3]);
    }
    return [];
  }

  public function getContentPage($where){
    return Page::where($where)->first();
  }

  public function updateContentPage($id,$data){
    Page::where('id',$id)->update($data);
    return Page::where('id',$id)->first();
  }
  
  public function deleteContentPage($id) {
    return Page::where('id',$id)->delete();
  }

}
