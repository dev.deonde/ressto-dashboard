<?php

namespace App\Http\Services;

use App\Models\Mail;
use App\Models\Order;
use App\Models\DriverOrder;
use App\Models\Restaurant;
use App\Models\Customer;
use App\Models\UserRedeemedCoupons;
use App\Models\Coupon;
use App\Http\Services\CommonService;

class MailService
{
    public function __construct(CommonService $commonService)  {
        $this->commonService = $commonService;
    }

    public function mailList($user,$search,$status,$sortby,$orderby = "desc", $total_record)
    {
        $mail = Mail::where(function($q) use($search,$status){
            if($search != ''){
                $q->where('subject','LIKE',"%{$search}%");
            }
            if($status != ''){
                $q->where('status',$status);
            }
        })->where('client_id',$user->myid())
        ->orderBy($sortby, $orderby)
        ->paginate($total_record);
        return $mail;
    }

    public function storeMailTemplate($data){        
        return Mail::create($data);
    }

    public function getMailById($where){
        return Mail::where($where)->first();
    }

    public function updateMailTemplate($id,$data){
        Mail::where('id',$id)->update($data);
        return Mail::where('id',$id)->first();
    }

    public function getOrder($where)
    {
        return Order::where($where)->first();
    }

    public function getDriverOrderRequest($where){
        return DriverOrder::where($where)->first();
    }

    public function getUserRestaurant($where)
    {
        return Restaurant::where($where)->first();
    }
    public function getCustomer($where)
    {
        return Customer::where($where)->first();
    }

    // public function getCoupon($where){
    //     return UserRedeemedCoupons::where($where)->first();
    // }

    public function getOrderResItem($order_id){
        return \DB::select("select mt.price,ot.quantity,amount,item_name,item_price from fa_order_item ot join fa_restaurant_menu_item mt on mt.restaurant_menu_item_id=ot.restaurant_menu_item_id where order_id='" . $order_id . "' and is_bravges='0' union(select mt.price,quantity,amount,item_name,item_price from fa_order_item ot join fa_braveges_menu mt on mt.braveges_menu_item_id=ot.restaurant_menu_item_id where order_id='" . $order_id . "' and is_bravges='1')");
    }
    public function getMasterCoupon($where){
        return Coupon::where($where)->first();
    }

    public function sendMail($to, $fromemail, $data, $subject) {
        //sendgrid code
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($fromemail, $data['app_name']);
        $email->setSubject($subject);
        $email->addTo($to, $subject);
        $email->addContent(
            "text/html", $data
        );
        try {
            $sendgrid = new \SendGrid(config('constant.SENDGRID_API_KEY'));
            $response = $sendgrid->send($email);
          } catch (\Exception $e) {
              Log::info($subject.' mail fail');
        }
    }

    public function sendForgotPasswordMail($email,$client_id) {
        //all logic
        $where = [['email','=', $email]];

        $user_exists = $this->commonService->emailCheckExists('User',$where);

        if (!empty($user_exists)) {
            $client_id = $user_exists->client_id;
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            $securecode = substr(str_shuffle($chars), 0, 20);

            $delete_password = $this->commonService->deleteResetPassword(['user_id' => $client_id]);

            $data = ['user_id' => $client_id, 'token' => $securecode, 'created_date' => date('Y-m-d H:i:s')];
            $password_reset = $this->commonService->storeResetPassword($data);
            $logo = (config('constant.storage_path').'media/foodapp/original/').\App\Helper::applogo($client_id);
            $link = config('constant.Base_V')."api/resetPassword/".base64_encode($client_id)."/".$securecode."/".base64_encode($user_exists->user_id);
            $date =date('Y-m-d H:i:s');
            $mail = Mail::where('slug','forgot-password')->where('client_id',$client_id)->first();
            if(!blank($mail)){
                $mail_content = html_entity_decode($mail['content']);
                $mail_content = str_replace('{user_name}',$user_exists->user_name,$mail_content);
                $mail_content = str_replace('{forgot_password_link}',"<a href='$link'>".$link."</a>",$mail_content);
                $mail_content = str_replace('{app_name}',\App\Helper::appname($client_id),$mail_content);
                $footer ="copyright <?php echo date('Y'); ?>  &copy; ".\App\Helper::appname($client_id)."";
                $content = $mail_content;
            }
        }

        $this->sendMail($to,$fromemail,$data, $subject);
        //call sendmail function
    }

    public function viewMail($user,$id){                        
        return Mail::where('client_id',$user->myid())->where('id',$id)->first();
    }
    
    public function staticSendMail($user,$slug) {
        return Mail::where('client_id',1)->where('slug',$slug)->first();
    }
}
