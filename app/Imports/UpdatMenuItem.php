<?php

namespace App\Imports;

use App\Enums\RedisKeysEnum;
use App\Http\Services\MasterItemService;
use App\Http\Services\RedisService;
use App\Models\MasterItem;
use App\Models\MasterMenu;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Validators\Failure;

class UpdatMenuItem implements ToCollection
{
    public function  __construct($client_id)
    {
        $this->client_id = $client_id;
        $this->redisService = new RedisService;
        $this->masterItemService = new MasterItemService;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $result = [];
    
        $client_id = $this->client_id;
        $result = $rows->toArray();
        $keys = [];
        $newArray = [];
        $error = [];
        foreach ($result as $key => $value) {
            if(count($keys) > 0){
                foreach ($value as $key1 => $value1) {
                    $newArray[$key][$keys[$key1]] = $value1;
                }		
            }else{
                foreach ($value as $key1 => $value1) {
                    $keys[] = $this->getKey($value1);
                }
            
            }
        }
        $results = array_values($newArray);
        foreach($results as $key => $value){
            $line = $key+2;
            if(!blank($value['id'])){
                $menu = MasterMenu::where('id',$value['id'])->where('client_id', $this->client_id)->first();
                if(!blank($menu)){
                    $menu->menu_name = $value['menuname'] ;
                    $menu->menu_name_thai = $value['menunamesecondarylanguage'];
                    $menu->status = (string)(int)$value['menustatus'];
                    $menu->save();

                }else{
                    $error[] = 'Master Menu Note Found ';
                }
                if(!blank($value['itemid'])){
                    $menuItem = MasterItem::where('id', $value['itemid'])->where('client_id',$this->client_id)->first();
                    if(!blank($menuItem)){
                        $menuItem->item_name = $value['itemname'];
                        $menuItem->item_name_thai = $value['itemnamesecondarylanguage'];
                        $menuItem->item_description = $value['itemdescription'];
                        $menuItem->item_description_thai = $value['itemdescriptionsecondarylanguage'];
                        $menuItem->price = $value['itemprice'];
                        $menuItem->mrp = $value['itemmrp'];
                        $menuItem->quantity = $value['itemquantity'];
                        $menuItem->item_packaging_charge = $value['itempackagingcharge'];
                        $menuItem->item_type = (string)(int)$value['type'];
                        $menuItem->is_available = (string)(int)$value['status'];
                        $menuItem->save();
                        if($menuItem){
                            $get_master_item_redis = $this->redisService->get(RedisKeysEnum::MASTER_ITEM->value.':'.$menuItem->main_category_id);
                            if(!blank($get_master_item_redis)){
                                $get_master_item = $this->masterItemService->getMasterItemByID(auth()->user(),$client_id,$menuItem->main_category_id,'',50);
                                $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$menuItem->main_category_id, $get_master_item);
                            }

                        }
                    }else{
                        $error[] = 'Master Ttem Note Found';
                    }                
                }
            }else{
                if(!blank($value['itemid']) && blank($value['id'])){
                    $menuItem = MasterItem::where('id', $value['itemid'])->where('client_id',$this->client_id)->first();
                    if(!blank($menuItem)){
                        $menuItem->item_name = $value['itemname'];
                        $menuItem->item_name_thai = $value['itemnamesecondarylanguage'];
                        $menuItem->item_description = $value['itemdescription'];
                        $menuItem->item_description_thai = $value['itemdescriptionsecondarylanguage'];
                        $menuItem->price = $value['itemprice'];
                        $menuItem->mrp = $value['itemmrp'];
                        $menuItem->quantity = $value['itemquantity'];
                        $menuItem->item_packaging_charge = $value['itempackagingcharge'];
                        $menuItem->item_type = (string)(int)$value['type'];
                        $menuItem->is_available = (string)(int)$value['status'];
                        $menuItem->save();
                        if($menuItem){
                            $get_master_item_redis = $this->redisService->get(RedisKeysEnum::MASTER_ITEM->value.':'.$menuItem->main_category_id);
                            if(!blank($get_master_item_redis)){
                                $get_master_item = $this->masterItemService->getMasterItemByID(auth()->user(),$client_id,$menuItem->main_category_id,'',50);
                                $this->redisService->set(RedisKeysEnum::MASTER_ITEM->value.':'.$menuItem->main_category_id, $get_master_item);
                            }

                        }

                    }else{
                        $error[] = 'Master Ttem Note Found ';
                    }

                }
            }

        }
        if(sizeof($error) > 0){
            $failures[] = new Failure($line, 'error', $error);
            throw new \Maatwebsite\Excel\Validators\ValidationException(\Illuminate\Validation\ValidationException::withMessages($error), $failures);
        }
    }

    public function getKey($text){
        $tagOne = "(";
        $tagTwo = ")";
        $replacement = "";
        $startTagPos = strrpos($text, $tagOne);
        if($startTagPos !== false){
            $endTagPos = strrpos($text, $tagTwo);
            $tagLength = $endTagPos - $startTagPos + 1;
            $newText = strtolower(str_replace(' ','',(trim(substr_replace($text, $replacement, $startTagPos, $tagLength)))));
        }else{
            $newText = strtolower(str_replace(' ','',(trim($text))));
        }
        return $newText;
    }
}
