<?php

namespace App\Imports;

use File, DB;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;

use App\Models\VendorSetting;
use App\Models\SampleImport;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;

class SampleExport implements FromCollection {

	public function  __construct($client_id) {
        $this->client_id = $client_id;
    }

    public function collection() {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '-1');
        $client_id = $this->client_id;

        $headers = SampleImport::where('id',1)->get();
  		// $vendor_setting = VendorSetting::where('client_id',$client_id)->get();
  		// if ($vendor_setting) {
		// 	$arr = [];
		// 	foreach($vendor_setting as $key => $value){
        //         $arr = array_merge($arr, [$value['settings_key'] => $value['setting_values']]);
        //     }
		// 	if ($arr['custom_item']) {
		// 		foreach (json_decode($arr['custom_item']) as $key => $value) {
		// 			$keyval = strtoupper($value->name);
		// 			$headers[0]->{$keyval} = 'CUSTOM '.$keyval;
		// 		}
		// 	}
		// }
		return $headers;
    }
}