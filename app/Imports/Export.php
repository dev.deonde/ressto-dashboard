<?php

namespace App\Imports;

use File;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use App\Models\MasterCategory;
use App\Models\MasterMenu;
use App\Models\MasterItem;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;

class Export implements FromCollection,WithHeadings
{
    public function  __construct($menu_id,$type,$client_id)
    {
        $this->menu_id = $menu_id;
        $this->type = $type;
        $this->client_id = $client_id;
    }

    public function collection()
    {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '-1');
        $menu_id = $this->menu_id;
        $type = $this->type;
        $client_id = $this->client_id;
        if(base64_decode($type) =='item'){
            return MasterItem::select('id','item_name','item_name_thai','item_description','item_description_thai','price','mrp','quantity','item_type','is_available','item_packaging_charge')->where('is_menu',base64_decode($menu_id))->whereNull('deleted_at')->get();
        }else{
            if(!blank($menu_id)){
                $res_menus =  MasterMenu::select(
                    'master_menu.id',
                    'menu_name',
                    'menu_name_thai',
                    'master_menu.image',
                    'master_menu.status',
                    'master_item.id as item_id',
                    'item_name',
                    'item_name_thai',
                    'item_description',
                    'item_description_thai',
                    'price',
                    'mrp',
                    'quantity',
                    'item_type',
                    'is_available',
                    'item_packaging_charge',
                )
                ->where('master_menu.client_id',base64_decode($client_id))
                ->where('master_menu.id',base64_decode($menu_id))
                ->leftJoin('master_item',function($q){
                    $q->on('master_item.main_category_id','=','master_menu.id')->whereNull('master_item.deleted_at');
                })
                
                ->orderby('item_id','ASC')
                ->get();
            }else{
                $res_menus =  MasterMenu::select(
                    'master_menu.id',
                    'menu_name',
                    'menu_name_thai',
                    'master_menu.image',
                    'master_menu.status',
                    'master_item.id as item_id',
                    'item_name',
                    'item_name_thai',
                    'item_description',
                    'item_description_thai',
                    'price',
                    'mrp',
                    'quantity',
                    'item_type',
                    'is_available',
                    'item_packaging_charge',
                )
                ->where('master_menu.client_id',base64_decode($client_id))
                ->leftJoin('master_item',function($q){
                    $q->on('master_item.main_category_id','=','master_menu.id')->whereNull('master_item.deleted_at');
                })
                ->orderby('item_id','ASC')
                ->get();
            }
            $acc = collect();
            $item_id = collect();
            $r = $res_menus->map(function($q)use($acc){
                $data = collect($q->toArray())->except('menu_image_path', 'menu_media_path');

                if($acc->contains($q->id)){
                    $data['id']="";
                    $data['menu_name']="";
                    $data['menu_name_thai']="";
                    $data['image']="";
                    $data['status']="";
                }
                $acc->push($q->id);
                //dump($data);
                return $data;
            });
            // dd($acc->contains(7885),$r);
            // dd($r);
            return $r;
        }
        
    }

    public function headings(): array
    {
        $type = $this->type;
        if(base64_decode($type) =='item'){
            return [
                'CLIENT ITEM ID',
                'ITEM NAME',
                'ITEM NAME SECONDARY LANGUAGE',
                'ITEM DESCRIPTION',
                'ITEM DESCRIPTION SECONDARY LANGUAGE',
                'ITEM PRICE',
                'ITEM MRP',
                'ITEM QUANTITY',
                'TAX CODE',
                'TYPE (VEG - 0, Non Veg - 1, None - 2)',
                'STATUS (Available - 1,Not Available-0)',
                'ITEM PACKAGING CHARGE',
                'IMAGE1 (Image name with extension of jpg,png)',
                'IMAGE2 (Image name with extension of jpg,png)',
                'IMAGE3 (Image name with extension of jpg,png)',
                'IMAGE4 (Image name with extension of jpg,png)',
                'IMAGE5 (Image name with extension of jpg,png)',
            ];
        }else{
            return [
                'ID',
                'MENU NAME',
                'MENU NAME SECONDARY LANGUAGE',
                'MENU IMAGE',
                'MENU STATUS',
                'ITEM ID',
                'ITEM NAME',
                'ITEM NAME SECONDARY LANGUAGE',
                'ITEM DESCRIPTION',
                'ITEM DESCRIPTION SECONDARY LANGUAGE',
                'ITEM PRICE',
                'ITEM MRP',
                'ITEM QUANTITY',
                // 'TAX CODE',
                'TYPE (VEG - 0, Non Veg - 1, None - 2)',
                'STATUS (Available - 1,Not Available-0)',
                'ITEM PACKAGING CHARGE',
                'IMAGE1 (Image name with extension of jpg,png)',
                'IMAGE2 (Image name with extension of jpg,png)',
                'IMAGE3 (Image name with extension of jpg,png)',
                'IMAGE4 (Image name with extension of jpg,png)',
                'IMAGE5 (Image name with extension of jpg,png)',
            ];
        }
    }
}