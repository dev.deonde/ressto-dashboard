<?php
namespace App\Imports;

use File;
use Illuminate\Support\Str;

use App\Models\MasterMenu;
use App\Models\MasterCategory;
use App\Models\MasterItem;
use App\Models\MasterItemImage;
use App\Models\VendorSetting;

use Illuminate\Support\Collection;
use App\Http\Services\CommonService;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Http\File as MyFile;


class MenuImport implements ToCollection
{
    protected $privateKeyFile, $bucketName, $storage_type;

    public function  __construct($client_id)
    {
        $this->client_id = $client_id;
        $this->commonService = new CommonService;
        $this->privateKeyFile = base_path('google_storage.json');;
        $this->bucketName = config('constant.storage_bucket');
        $this->storage_type = config('constant.storage_type');
        
    }

    public function collection(Collection $rows)
    {   
        $result = [];
    
        $client_id = $this->client_id;
        $result = $rows->toArray();
        
        $keys = [];
        $newArray = [];
        foreach ($result as $key => $value) {
            if(count($keys) > 0){
                foreach ($value as $key1 => $value1) {
                    $newArray[$key][$keys[$key1]] = $value1;
                }		
            }else{
                foreach ($value as $key1 => $value1) {
                    $keys[] = $this->getKey($value1);
                }
            
            }
        }

        $path = 'media/Master_Item_image/'.$client_id.'/';

        if (!is_dir($path)) {
            mkdir($path, 0777,true);
           // chmod($path, 0777);
           exec ("find $path -type d -exec chmod 0777 {} +");//for sub directory
        }else{
          //  chmod($path, 0777);
          exec ("find $path -type d -exec chmod 0777 {} +");//for sub directory

        }
        
        $results = array_values($newArray);
        
        // $vendor_setting = VendorSetting::where('vendor_id', $this->vendor_id)->first();
        // $custom_item = [];
        // if ($vendor_setting) {
        //     if (isset($vendor_setting->settings['custom_item'])) {
        //         foreach (json_decode($vendor_setting->settings['custom_item']) as $key => $value) {
        //             $custom_item[] = $value->name;
        //         }
        //     }
        // }

        // $kys = array_keys(array_values($newArray)[0]);
        // $custom_keys = array_filter($kys,function($q){return strpos($q,'custom') !== false; });
        // $custom_keys_res = array_values(array_map(function($q){return str_replace('custom','',$q); },$custom_keys));

        // dd($results);
        foreach ($results as $key => $row) {
           
            $line = $key+2;
            // if ($this->vendor_id == 40818) {
            //     dd($key);
            // }
            if(!isset($row['clientitemid']) && isset($row['menuname']) && !blank($row['menuname'])){
                $restaurantMenu = MasterMenu::where('client_id',$client_id)->where('menu_name',$row['menuname'])->first();
               
                if(!$restaurantMenu){
                    // dd('ifif');
                    if($row['menuname'] != null){

                        $new_menu=new MasterMenu;
                        $new_menu->client_id = $client_id;
                        $new_menu->menu_name = $row['menuname'];
                        $new_menu->menu_name_thai = isset($row['menunamesecondarylanguage']) ? $row['menunamesecondarylanguage'] : '';
                        $new_menu->menu_description="";
                        $new_menu->image = isset($row['menuimage']) ? $this->movedFile($row['menuimage'],'media/Master_Menu_image/original/','Menu') : '';
                        $new_menu->status = '1';
                        $new_menu->parent_menu_id = "0";
                        $new_menu->created_at = date('Y-m-d H:i:s');
                        $new_menu->updated_at = date('Y-m-d H:i:s');
                        $new_menu->save();
                    }else{
                        $errors[] = 'Menu name is empty at row '.$line.'.';
                    }
                }else{
                    // dd('else');
                    $restaurantMenuItem = MasterItem::where('main_category_id',$restaurantMenu->id)->where('item_name',$row['itemname'])->first();
                  
                    if(!$restaurantMenuItem){
                        if($row['itemname'] != null){
                            //($row['itemprice'] != null && $row['itemprice'] > 0) &&
                            if(($row['itemquantity'] != null && $row['itemquantity'] > 0)){
                                $data['item_name'] = isset($row['itemname']) ? $row['itemname'] : '';
                                $data['item_name_thai']= isset($row['itemnamesecondarylanguage']) ? $row['itemnamesecondarylanguage'] : '';
                                $data['item_description'] = isset($row['itemdescription']) ? $row['itemdescription'] : '';
                                $data['item_description_thai']= isset($row['itemdescriptionsecondarylanguage']) ? $row['itemdescriptionsecondarylanguage'] : '';
                                $data['item_price'] = isset($row['itemprice']) ? $row['itemprice'] : 0;
                                $data['item_quantity'] = isset($row['itemquantity']) ? $row['itemquantity'] : 0;
                                $data['item_mrp'] = isset($row['itemmrp']) ? $row['itemmrp'] : (isset($row['itemprice']) ? $row['itemprice'] : 0);
                                $data['type'] = isset($row['type']) ? (int)$row['type'] : 0;
                                $data['status'] = isset($row['status']) ? (int)$row['status'] : 0;
                                $data['item_packaging_charge'] = isset($row['itempackagingcharge']) ? (float)$row['itempackagingcharge'] : 0.0;
                                $data['taxslab'] = 0;
                                // if (isset($row['taxcode']) && $row['taxcode'] != '' && $row['taxcode'] != null) {
                                //     $checkSlab = $this->getSlabIDFromCode($client_id, $row['taxcode']);
                                //     if ($checkSlab['status']) {
                                //         $data['taxslab'] = $checkSlab['slabid'];
                                //     }
                                // }
                                $data['image1'] = (isset($row['image1'])) ? $row['image1'] : '';
                                $data['image2'] = (isset($row['image2'])) ? $row['image2'] : '';
                                $data['image3'] = (isset($row['image3'])) ? $row['image3'] : '';
                                $data['image4'] = (isset($row['image4'])) ? $row['image4'] : '';
                                $data['image5'] = (isset($row['image5'])) ? $row['image5'] : '';

                                $new_menu_item=new MasterItem;
                                $new_menu_item->main_category_id = $restaurantMenu->id;
                                $new_menu_item->is_menu = 1;
                                $new_menu_item->client_id = $client_id;
                                $new_menu_item->item_name= $data['item_name'];
                                $new_menu_item->item_name_thai = $data['item_name_thai'];
                                $new_menu_item->item_description = $data['item_description'];
                                $new_menu_item->item_description_thai = $data['item_description_thai'];
                                $new_menu_item->is_available = ($data['status'] == 0.0) ? '0' : '1';
                                $new_menu_item->price = $data['item_price'];
                                $new_menu_item->item_type = (string)$data['type'];
                                $new_menu_item->created_at = date('Y-m-d H:i:s');
                                $new_menu_item->updated_at = date('Y-m-d H:i:s');
                                $new_menu_item->qty_reset = 'Never';
                                $new_menu_item->quantity = $data['item_quantity'];
                                $new_menu_item->mrp = $data['item_mrp'];
                                $new_menu_item->item_packaging_charge = $data['item_packaging_charge'];
                                $new_menu_item->save(); 

                                // if ($new_menu_item->id) {
                                //     foreach ($custom_item as $customkey => $customvalue) {
                                //         $t = strtolower(str_replace(' ','',$customvalue));
                                //         if (in_array( $t , $custom_keys_res)) {
                                //             $towerKey = strtolower($t);
                                //             if ($towerKey != '' && $towerKey != null) {
                                //                 $this->addItemCustomDetails($new_menu_item->restaurant_menu_item_id, $customvalue, $row["custom$towerKey"]);
                                //             }
                                //         }
                                //     }
                                // }

                                if($data['image1']!= null){
                                    $filename = $this->movedFile($data['image1'],$path,\App\Helper::app_name_snake_case().'_menu');
                                    $this->saveFile($new_menu_item->id,$filename,$client_id);
                                }
                                if($data['image2']!= null){
                                    $filename = $this->movedFile($data['image2'],$path,\App\Helper::app_name_snake_case().'_menu');
                                    $this->saveFile($new_menu_item->id,$filename,$client_id);
                                }
                                if($data['image3']!= null){
                                    $filename = $this->movedFile($data['image3'],$path,\App\Helper::app_name_snake_case().'_menu');
                                    $this->saveFile($new_menu_item->id,$filename,$client_id);
                                }
                                if($data['image4']!= null){
                                    $filename = $this->movedFile($data['image4'],$path,\App\Helper::app_name_snake_case().'_menu');
                                    $this->saveFile($new_menu_item->id,$filename,$client_id);
                                }
                                if($data['image5']!= null){
                                    $filename = $this->movedFile($data['image5'],$path,\App\Helper::app_name_snake_case().'_menu');
                                    $this->saveFile($new_menu_item->id,$filename,$client_id);
                                }
                            }else{
                                $errors[] = 'Item item price Or Item Quntity is not proper at row '.$line.'.';
                            }
                        }else{
                            if($row['itemprice'] != null && $row['itemquantity'] != null){
                                $errors[] = 'Item name is empty at row '.$line.'.';
                            }
                        }
                    }else{
                        $errors[] = 'Duplicate Item name at row '.$line.'.';
                    }
                }
            }elseif(isset($row['id']) && $row['id'] != null && isset($row['clientitemid']) && $row['clientitemid'] != null){
                // dd('else if');
                $restaurantMenu = MasterMenu::where('id',$row['id'])->first();
                
                if($restaurantMenu && $row['menuname'] != null){
                    // $restaurantMenu = RestaurantMenu::where('restaurant_id',$restaurant_id)->where('id','!=',$row['id'])->where('menu_name',$row['categoryname'])->get();
                    if(!blank($restaurantMenu)){
                        if($row['menustatus'] == 1){
                            $status = '1';
                        }else{
                            $status = '0';
                        }
                        $menuUpadte = MasterMenu::where('id',$row['id'])->update(['menu_name' => isset($row['menuname']) ? $row['menuname'] : $restaurantMenu->menu_name , 'menu_name_thai' => isset($row['menunamesecondarylanguage']) ? $row['menunamesecondarylanguage'] : $restaurantMenu->menu_name_thai , 'menu_description' => isset($row['menudescription']) ? $row['menudescription'] : $restaurantMenu->menu_description , 'image' => isset($restaurantMenu->image) ? $restaurantMenu->image : $restaurantMenu->image ,'status' => isset($status) ? $status : $restaurantMenu->status, 'updated_at' => date('Y-m-d H:i:s')]);
                        
                    }else{
                        $errors[] = 'Menu already exists';
                    }
                }else{
                    $errors[] = 'Menu not exists';
                }

                $restaurantMenuItem = MasterItem::where('id',$row['clientitemid'])->first();
                if($restaurantMenuItem){
                    if($row['itemname'] != null){
                        //($row['itemprice'] != null && $row['itemprice'] > 0) &&
                        if( ($row['itemquantity'] != null && $row['itemquantity'] > 0)){
                            $data['item_name'] = isset($row['itemname']) ? $row['itemname'] : '';
                            $data['item_name_thai']= isset($row['itemnamesecondarylanguage']) ? $row['itemnamesecondarylanguage'] : '';
                            $data['item_description'] = isset($row['itemdescription']) ? $row['itemdescription'] : '';
                            $data['item_description_thai']= isset($row['itemdescriptionsecondarylanguage']) ? $row['itemdescriptionsecondarylanguage'] : '';
                            $data['item_price'] = isset($row['itemprice']) ? $row['itemprice'] : 0;
                            $data['item_quantity'] = isset($row['itemquantity']) ? $row['itemquantity'] : 0;
                            $data['item_mrp'] = isset($row['itemmrp']) ? $row['itemmrp'] : (isset($row['itemprice']) ? $row['itemprice'] : 0);
                            $data['type'] = isset($row['type']) ? (int)$row['type'] : 0;
                            $data['status'] = isset($row['status']) ? (int)$row['status'] : 0;
                            $data['taxslab'] = 0;
                            $data['item_packaging_charge'] = isset($row['itempackagingcharge']) ? (float)$row['itempackagingcharge'] : 0.0;
                            // if (isset($row['taxcode']) && $row['taxcode'] != '' && $row['taxcode'] != null) {
                            //     $checkSlab = $this->getSlabIDFromCode($client_id, $row['taxcode']);
                            //     if ($checkSlab['status']) {
                            //         $data['taxslab'] = $checkSlab['slabid'];
                            //     }
                            // }
                            $new_menu_item = MasterItem::find($row['clientitemid']);
                            $new_menu_item->item_name= $data['item_name'];
                            $new_menu_item->item_name_thai = $data['item_name_thai'];
                            $new_menu_item->item_description = $data['item_description'];
                            $new_menu_item->item_description_thai = $data['item_description_thai'];
                            $new_menu_item->is_available = ($data['status'] == 0.0) ? '0' : '1';
                            $new_menu_item->price = $data['item_price'];
                            $new_menu_item->item_type = (string)$data['type'];
                            $new_menu_item->updated_at = date('Y-m-d H:i:s');
                            $new_menu_item->quantity = $data['item_quantity'];
                            $new_menu_item->mrp = $data['item_mrp'];
                            $new_menu_item->item_packaging_charge = $data['item_packaging_charge'];
                            $new_menu_item->save();

                            // if ($new_menu_item->restaurant_menu_item_id) {
                            //     foreach ($custom_item as $customkey => $customvalue) {
                            //         $t = strtolower(str_replace(' ','',$customvalue));
                            //         if (in_array( $t , $custom_keys_res)) {
                            //             $towerKey = strtolower($t);
                            //             if ($towerKey != '' && $towerKey != null) {
                            //                 $this->addItemCustomDetails($new_menu_item->restaurant_menu_item_id, $customvalue, $row["custom$towerKey"]);
                            //             }
                            //         }
                            //     }
                            // }

                            $data['image1'] = (isset($row['image1'])) ? $row['image1'] : '';
                            $data['image2'] = (isset($row['image2'])) ? $row['image2'] : '';
                            $data['image3'] = (isset($row['image3'])) ? $row['image3'] : '';
                            $data['image4'] = (isset($row['image4'])) ? $row['image4'] : '';
                            $data['image5'] = (isset($row['image5'])) ? $row['image5'] : '';

                            if($data['image1']!= null){
                                $filename = $this->movedFile($data['image1'],$path,'_menu');
                                $this->saveFile($new_menu_item->id,$filename,$client_id);
                            }
                            if($data['image2']!= null){
                                $filename = $this->movedFile($data['image2'],$path,\App\Helper::app_name_snake_case().'_menu');
                                $this->saveFile($new_menu_item->id,$filename,$client_id);
                            }
                            if($data['image3']!= null){
                                $filename = $this->movedFile($data['image3'],$path,\App\Helper::app_name_snake_case().'_menu');
                                $this->saveFile($new_menu_item->id,$filename,$client_id);
                            }
                            if($data['image4']!= null){
                                $filename = $this->movedFile($data['image4'],$path,\App\Helper::app_name_snake_case().'_menu');
                                $this->saveFile($new_menu_item->id,$filename,$client_id);
                            }
                            if($data['image5']!= null){
                                $filename = $this->movedFile($data['image5'],$path,\App\Helper::app_name_snake_case().'_menu');
                                $this->saveFile($new_menu_item->id,$filename,$client_id);
                            }
                            
                        }else{
                            $errors[] = 'Item item price Or Item Quntity is not proper at row '.$line.'.';
                        }
                    }else{
                        if($row['itemprice'] != null && $row['itemquantity'] != null){
                            $errors[] = 'Item name is empty at row '.$line.'.';
                        }
                    }
                }else{
                    $errors[] = 'Item not exists';
                }
            }else{
                // dd('else');
                if (isset($row['clientitemid'])) {
                    $restaurantMenuItem = MasterItem::where('id',$row['clientitemid'])->first();
                    if($restaurantMenuItem){
                        if($row['itemname'] != null){
                            //($row['itemprice'] != null && $row['itemprice'] > 0) &&
                            if( ($row['itemquantity'] != null && $row['itemquantity'] > 0)){
                                $data['item_name'] = isset($row['itemname']) ? $row['itemname'] : '';
                                $data['item_name_thai']= isset($row['itemnamesecondarylanguage']) ? $row['itemnamesecondarylanguage'] : '';
                                $data['item_description'] = isset($row['itemdescription']) ? $row['itemdescription'] : '';
                                $data['item_description_thai']= isset($row['itemdescriptionsecondarylanguage']) ? $row['itemdescriptionsecondarylanguage'] : '';
                                $data['item_price'] = isset($row['itemprice']) ? $row['itemprice'] : 0;
                                $data['item_quantity'] = isset($row['itemquantity']) ? $row['itemquantity'] : 0;
                                $data['item_mrp'] = isset($row['itemmrp']) ? $row['itemmrp'] : (isset($row['itemprice']) ? $row['itemprice'] : 0);
                                $data['type'] = isset($row['type']) ? (int)$row['type'] : 0;
                                $data['status'] = isset($row['status']) ? (int)$row['status'] : 0;
                                $data['taxslab'] = 0;
                                $data['item_packaging_charge'] = isset($row['itempackagingcharge']) ? (float)$row['itempackagingcharge'] : 0.0;
                                // if (isset($row['taxcode']) && $row['taxcode'] != '' && $row['taxcode'] != null) {
                                //     $checkSlab = $this->getSlabIDFromCode($client_id, $row['taxcode']);
                                //     if ($checkSlab['status']) {
                                //         $data['taxslab'] = $checkSlab['slabid'];
                                //     }
                                // }
                                $data['image1'] = (isset($row['image1'])) ? $row['image1'] : '';
                                $data['image2'] = (isset($row['image2'])) ? $row['image2'] : '';
                                $data['image3'] = (isset($row['image3'])) ? $row['image3'] : '';
                                $data['image4'] = (isset($row['image4'])) ? $row['image4'] : '';
                                $data['image5'] = (isset($row['image5'])) ? $row['image5'] : '';

                                $new_menu_item = MasterItem::find($row['clientitemid']);
                                $new_menu_item->item_name= $data['item_name'];
                                $new_menu_item->item_name_thai = $data['item_name_thai'];
                                $new_menu_item->item_description = $data['item_description'];
                                $new_menu_item->item_description_thai = $data['item_description_thai'];
                                $new_menu_item->is_available = ($data['status'] == 0.0) ? '0' : '1';
                                $new_menu_item->price = $data['item_price'];
                                $new_menu_item->item_type = (string)$data['type'];
                                $new_menu_item->updated_at = date('Y-m-d H:i:s');
                                $new_menu_item->quantity = $data['item_quantity'];
                                $new_menu_item->mrp = $data['item_mrp'];
                                $new_menu_item->item_packaging_charge = $data['item_packaging_charge'];
                                $new_menu_item->save();
                                
                                // if ($new_menu_item->restaurant_menu_item_id) {
                                //     foreach ($custom_item as $customkey => $customvalue) {
                                //         $t = strtolower(str_replace(' ','',$customvalue));
                                //         if (in_array( $t , $custom_keys_res)) {
                                //             $towerKey = strtolower($t);
                                //             if ($towerKey != '' && $towerKey != null) {
                                //                 $this->addItemCustomDetails($new_menu_item->restaurant_menu_item_id, $customvalue, $row["custom$towerKey"]);
                                //             }
                                //         }
                                //     }
                                // }
                                
                                if($data['image1']!= null){
                                    $filename = $this->movedFile($data['image1'],$path,'_menu');
                                    $this->saveFile($new_menu_item->id,$filename,$client_id);
                                }
                                if($data['image2']!= null){
                                    $filename = $this->movedFile($data['image2'],$path,\App\Helper::app_name_snake_case().'_menu');
                                    $this->saveFile($new_menu_item->id,$filename,$client_id);
                                }
                                if($data['image3']!= null){
                                    $filename = $this->movedFile($data['image3'],$path,\App\Helper::app_name_snake_case().'_menu');
                                    $this->saveFile($new_menu_item->id,$filename,$client_id);
                                }
                                if($data['image4']!= null){
                                    $filename = $this->movedFile($data['image4'],$path,\App\Helper::app_name_snake_case().'_menu');
                                    $this->saveFile($new_menu_item->id,$filename,$client_id);
                                }
                                if($data['image5']!= null){
                                    $filename = $this->movedFile($data['image5'],$path,\App\Helper::app_name_snake_case().'_menu');
                                    $this->saveFile($new_menu_item->id,$filename,$client_id);
                                }
                            }else{
                                $errors[] = 'Item item price Or Item Quntity is not proper at row '.$line.'.';
                            }
                        }else{
                            if($row['itemprice'] != null && $row['itemquantity'] != null){
                                $errors[] = 'Item name is empty at row '.$line.'.';
                            }
                        }
                    }else{
                        $errors[] = 'Item not exists';
                    }
                }
            }
        }
    }
    public function getKey($text){
        $tagOne = "(";
        $tagTwo = ")";
        $replacement = "";
        $startTagPos = strrpos($text, $tagOne);
        if($startTagPos !== false){
            $endTagPos = strrpos($text, $tagTwo);
            $tagLength = $endTagPos - $startTagPos + 1;
            $newText = strtolower(str_replace(' ','',(trim(substr_replace($text, $replacement, $startTagPos, $tagLength)))));
        }else{
            $newText = strtolower(str_replace(' ','',(trim($text))));
        }
        return $newText;
    }

    public function movedFile($file,$path,$folder){

        if(config('constant.storage_type') == 'local'){
            $filename = $this->getFileName($file,$folder);
            $tmp_path = public_path('media/Temp/original/'.$this->vendor_id.'/'.$this->restaurant_id.'/'.$file);
            if(File::exists($tmp_path)){      
                rename($tmp_path,$path.$filename);
            }
            return $filename;
        }
        elseif(config('constant.storage_type') == 'google'){
            try {
                $storage = new StorageClient([
                    'keyFilePath' => $this->privateKeyFile
                ]);
            
            } catch (Exception $e) {
                return false;
            }
            $bucketName = $this->bucketName;
            $filename = $this->getFileName($file,$folder);
            $bucket = $storage->bucket($bucketName);
            $tmp_path = public_path('media/Temp/original/'.$this->vendor_id.'/'.$this->restaurant_id.'/'.$file);
            
            if(File::exists($tmp_path)){
                $bucket->upload(
                    fopen($tmp_path,'r'),
                    [
                        'predefinedAcl' => 'publicRead',
                        'name' => $path.$filename
                    ]
                    
                );
                $new_tmp_path = 'media/Temp/original/'.$this->vendor_id.'/'.$this->restaurant_id.'/'.$file;
            //     $storageObject = $bucket->upload(
            //         $new_tmp_path,
            //         ['name' => $path.$filename]
            //         // if $cloudPath is existed then will be overwrite without confirmation
            //         // NOTE: 
            //         // a. do not put prefix '/', '/' is a separate folder name  !!
            //         // b. private key MUST have 'storage.objects.delete' permission if want to replace file !
            // );
            //    $object = $bucket->object($new_tmp_path);
            //    $object->copy($new_tmp_path,['name' => $path.$filename]);
                
            //    $object->delete();
                // rename($tmp_path,$path.$filename);
            }
            return $filename;
        } elseif (config('constant.storage_type') == 'aws') {

            $filename = $this->getFileName($file,$folder);
            $tmp_path = public_path('media/Temp/original/'.$this->client_id.'/'.$file);
            if (File::exists($tmp_path)) {
                $s3 = Storage::disk('s3');
                $s3->putFileAs(
                    $path,
                    new MyFile($tmp_path),
                    $filename
                );
            }
            return $filename;
        }
        
    }

    public function getFileName($file, $folder){
        $extension = array_reverse(explode('.',$file))[0];
        $createfilename = str_replace(' ','_',$folder).'_'.time().rand(0000, 9999);
        $filename  = $createfilename.'.'.$extension;
        return $filename;
    }

    public function saveFile($id,$filename,$client_id){
        $Restaurant_menu_item_images = new MasterItemImage();
        $Restaurant_menu_item_images->master_item_id = $id;
        $Restaurant_menu_item_images->image_name = $filename;
        $Restaurant_menu_item_images->client_id = $client_id;
        $Restaurant_menu_item_images->created_at = date('Y-m-d H:i:s');
        $Restaurant_menu_item_images->updated_at = date('Y-m-d H:i:s');
        $Restaurant_menu_item_images->save();
    }

    // public function getSlabIDFromCode($client_id, $taxcode) {
    //     $response = [
    //         'status' => false,
    //         'slabid' => 0
    //     ];
    //     $slabtax = TaxSlab::where('client_id',$client_id)->where('tax_uniquecode', $taxcode)->first();
    //     if ($slabtax) {
    //         $response = [
    //             'status' => true,
    //             'slabid' => $slabtax->id
    //         ];
    //     }
    //     return $response;
    // }

    // public function addItemCustomDetails($restaurant_menu_item_id, $custom_key, $custom_value) {
    //     if ($custom_key != "" && $custom_value != "") {
    //         ItemCustomDetails::updateOrCreate(
    //             ["restaurant_menu_item_id" => $restaurant_menu_item_id,'custom_key'=>$custom_key],
    //             ["custom_value" => $custom_value]
    //         );
    //     }
    // }
}
