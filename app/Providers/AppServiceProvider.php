<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use KgBot\LaravelLocalization\Facades\ExportLocalizations;
use View;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
        View::composer('layouts.app', function ($view) {
            return $view->with([
                'messages' => ExportLocalizations::export()->toFlat(),
            ]);
        });
        // \Log::info('Working',[true]);
        if(true) {
            DB::listen(function($query) {
                $qa = str_replace(array('?'), array('\'%s\''), $query->sql);
                // $q = vsprintf($qa, $query->bindings);
                // $this->addLog($qa, $query->bindings);
                // File::append(
                //     storage_path('/logs/query.log'),
                //     date('Y-m-d H:i:s') .' => '. $q .PHP_EOL
                // );
            });
        }
    }
}
