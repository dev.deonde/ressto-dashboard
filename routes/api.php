<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
use App\Models\Client;
use App\Http\Controllers\Api\{
    ClientController,
    MenuController,
    CategoryController,
    CustomerController,
    MasterItemController,
    CustomizationController,
    GeneralSettingController,
    OrderController,
    OutletController,
    PaymentGatewayController,
    PromoCodeController,
    RestaurantSettingController,
    WalletController,
    WebSettingController,
    MailController,
    StaticPageController,
    DashboardController
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/give-permissions', function () {
    $clients = Client::get();
    // $clients = Client::where('email','abidhusain.icoderz@gmail.com')->get();
    // $vendors = Vendor::whereIn('vendor_id',[40818, 40969, 40967, 41191, 40940])->get();
    foreach($clients as $client){
        $client->givePermissionTo(Permission::all());
    }
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function(){

    //Signup
    Route::post('signup', [ClientController::class,'signup']);

    Route::post('resend-otp', [ClientController::class,'resendOtp']);

    //confir-email-clientApi
    Route::post('confir-email-client', [ ClientController::class, 'confirEmailClient']);

    // forgot-passwor
    Route::post('forgot-passwor', [ClientController::class, 'forgotPasswor']);
    
    // reset-password
    Route::post('reset-password', [ClientController::class,'resetPassword']);

    //Login
    Route::any('login', [ClientController::class,'login'])->name('login');

    //Logout
    Route::post('logout', [ClientController::class,'logout']);

    //temp images
    Route::post('upload-temp-image', [ClientController::class,'uploadTmpImage']);
    Route::post('remove-temp-image', [ClientController::class,'removeTmpImage']);
    Route::post('remove-ios-pem', [GeneralSettingController::class,'removeIosPem']);

    //Vendor
    Route::post('get-all-client', [ClientController::class,'getAllClient']);
    Route::post('get-qr-code',[OutletController::class,'getQrCode']);    
    
    Route::get('get-language', [ClientController::class,'getLanguage']);   

    //content
    Route::post('get-content-page',[StaticPageController::class,'getContentPage']);
    Route::post('add-content-page',[StaticPageController::class,'addContentPage']);
    Route::post('get-content-page-by-id',[StaticPageController::class,'getContentPageById']);
    Route::post('edit-content-page',[StaticPageController::class,'editContentPage']);
    Route::post('view-content-page',[StaticPageController::class,'viewContentPage']);
    Route::post('delete-content-page',[StaticPageController::class,'deleteContentPage']);
});

Route::group(['namespace' => 'Api','middleware' => 'auth:client'], function(){

    //UserDetail
    Route::get('user-detail', [ClientController::class,'userDetail']);

    //client-detaile
    Route::get('get-client-detaile',[ClientController::class,'userDetail']);
    Route::post('update-client-detaile',[ClientController::class,'updateClientDetaile']);
    Route::post('update-client-password',[ClientController::class,'updateClientPassword']);

    //Menu
    Route::post('add-menu', [MenuController::class,'addMenu']);
    Route::post('master/menu', [MenuController::class,'getMasterMenu']);
    Route::post('delete-menu',[MenuController::class,'deleteMenu']);
    Route::any('get-menu-by-id',[MenuController::class,'getMenuById']);
    Route::post('edit-menu', [MenuController::class,'editMenu']);
    Route::post('update-master-menu-status', [MenuController::class,'updateMasterMenuStatus']);
    Route::any('delete-menu-image',[MenuController::class,'deleteMenuImage']);
    Route::any('update-menu-status',[MenuController::class,'updateMenuStatus']);
    Route::post('update-menu-order',[MenuController::class,'updateMenuOrder']);    

    //Category
    Route::post('add-category',[CategoryController::class,'addMenuCategory']);
    Route::post('get-categrory-list',[CategoryController::class,'getMasterCategories']);
    Route::post('get-master-category-by-id',[CategoryController::class,'getMasterCategoryById']);
    Route::post('update-master-category',[CategoryController::class,'updateMasterCategory']);
    Route::post('delete-master-category',[CategoryController::class,'deleteMasterCategory']);
    Route::post('delete-category-image-by-name', [CategoryController::class,'deleteCategoryImage']);

    //MasterItem
    Route::post('add-master-item',[MasterItemController::class,'addMasterItem']);
    Route::post('get-master-item',[MasterItemController::class,'getMenuItem']);
    Route::post('master/item', [MasterItemController::class,'getMasterItemByID']);
    Route::post('delete-menu-item',[MasterItemController::class,'deleteMenuItem']);
    Route::post('delete-master-with-item',[MasterItemController::class,'deleteMasterWithItem']);
    Route::post('get-master-item-image',[MasterItemController::class,'getMasterItemImage']);
    Route::post('add-master-image',[MasterItemController::class,'addMasterImage']);
    Route::post('delete-master-image-by-id',[MasterItemController::class,'deleteMasterImageById']);
    Route::post('import-master-item',[MasterItemController::class,'importMenuItem']);
    Route::post('uplod-updet-master-item',[MasterItemController::class,'uplodUpdetMasterItem']);
    Route::post('update-master-item',[MasterItemController::class,'updateMasterItem']);
    Route::post('update-item-availability',[MasterItemController::class,'updateItemAvailability']);
    Route::post('update-item-order',[MasterItemController::class,'updateItemOrder']);
    Route::get('item-types',[MasterItemController::class,'getItemType']);

    //Customization
    Route::post('add-customization',[CustomizationController::class,'addItemCustomization']);
    Route::post('get-item-customization',[CustomizationController::class,'getItemCustomization']);
    Route::post('get-customization-type',[CustomizationController::class,'getCustomizationType']);
    Route::post('update-customization-type',[CustomizationController::class,'updateCustomizationType']);
    Route::post('add-menu-selection',[CustomizationController::class,'addMenuSelection']);
    Route::post('delete-item-customization',[CustomizationController::class,'deleteItemCustomization']);
    Route::post('save-template',[CustomizationController::class,'saveTemplate']);
    Route::post('add-template',[CustomizationController::class,'addTemplate']);
    Route::post('get-template',[CustomizationController::class,'getTemplate']);
    Route::post('delete-customization-type',[CustomizationController::class,'deleteCustomizationType']);

    //SubCustomization
    Route::post('get-sub-customization',[CustomizationController::class,'getSubCustomization']);
    Route::post('add-sub-menu-selection',[CustomizationController::class,'addSubMenuSelection']);
    Route::post('min-selection',[CustomizationController::class,'minSelection']);
    Route::post('max-selection',[CustomizationController::class,'maxSelection']);

    //Customer
    Route::post('get-customer', [CustomerController::class,'getCustomer']);
    Route::post('customer-export-csv', [CustomerController::class,'customerExportCsv']);
    Route::any('get-customer-profile', [CustomerController::class,'getCustomerProfile']);
    Route::post('update-customer-profile', [CustomerController::class,'updateCustomerProfile']);
    Route::post('get-customer-order', [CustomerController::class,'getCustomerOrder']);
    Route::post('change-cust-password',[CustomerController::class,'ChangeCustPassword']);

    //PromoCode
    Route::post('get-promocode', [PromoCodeController::class,'getPromocode']);
    Route::post('add-promocode',[PromoCodeController::class,'addPromoCode']);
    Route::post('get-customer-listing',[PromoCodeController::class,'getPromoCodeCustomer']);
    Route::post('get-promocode-by-id',[PromoCodeController::class,'getPromoCodeById']);
    Route::post('update-promocode',[PromoCodeController::class,'editPromoCode']);
    Route::post('promocode/delete-promocode-by-name',[PromoCodeController::class,'deletePromoCodeImage']);

    //Orders
    Route::post('orders/get', [OrderController::class,'getOrders']);
    Route::post('orders/new/get', [OrderController::class,'getNewOrders']);
    Route::post('orders/export', [OrderController::class,'exportOrders']);
    Route::post('order/get', [OrderController::class,'getOrder']);
    Route::post('order/get/count', [OrderController::class,'getOrdersCount']);
    Route::post('order/cancel', [OrderController::class,'cancelOrder']);
    Route::post('order/assign_driver', [OrderController::class,'assignDriver']);
    Route::post('order_status/{url}', [OrderController::class,'orderStatusChange']);
    Route::post('order/deliver', [OrderController::class,'deliverOrder']);
    Route::post('order/change-to-cod', [OrderController::class,'changeToCod']);

    //wallet
    Route::get('wallet/get-history', [WalletController::class,'getWalletHistory']);
    Route::get('wallet/get-balance', [WalletController::class,'getWalletBalance']);
    Route::post('wallet/change-status', [WalletController::class,'changeStatus']);

    // Outlet 
    Route::get('get-restaurant',[OutletController::class,'getRestaurant']);
    Route::post('updated-restaurant',[OutletController::class,'updatedtRestaurant']);
    Route::get('get-restaurant-cuisines',[OutletController::class,'getRestaurantCuisines']);
    Route::post('updated-restaurant-cuisines',[OutletController::class,'updatedRestaurantCuisines']);
    
    // service offering
    Route::post('get-reataurant-setting',[RestaurantSettingController::class,'getReataurantSetting']);
    Route::post('get-service-offering',[OutletController::class,'getServiceOffering']);
    Route::post('save-service-offering',[OutletController::class,'saveServiceOffering']);
    
    // tax setting
    Route::get('get-tax-setting',[OutletController::class,'getTaxSetting']);
    Route::post('tax-setting-store-updat',[OutletController::class,'taxSettingStoreUpdat']);

    // outher setting
    Route::get('get-other-setting',[OutletController::class,'getOtherSetting']);
    Route::post('other-setting-store-update',[OutletController::class,'otherSettingStoreUpdate']);

    // Web Layout Setting
    Route::get('get-web-settings',[WebSettingController::class,'getWebSettings']);
    Route::post('update-web-settings',[WebSettingController::class,'updateWebSetting']);
    Route::post('delete-web-logo',[WebSettingController::class,'deleteWebLogo']);
    Route::post('delet-home-page-image',[WebSettingController::class,'deleteHomePageLogo']);
    Route::post('delete-section-image',[WebSettingController::class,'deleteSectionImage']);
    Route::post('delete-front-image',[WebSettingController::class,'deleteFrontImage']);

    // setting
    Route::post('update-setting',[GeneralSettingController::class,'updateSetting']);
    Route::post('delete-general-setting-by-key',[GeneralSettingController::class,'deleteGeneralSettingByKey']);
    Route::post('get-setting',[GeneralSettingController::class,'getSetting']);

    // payment-gateways
    Route::get('payment-gateways',[PaymentGatewayController::class,'getPaymentgateways']);
    Route::post('store-client-payment-cateway',[PaymentGatewayController::class,'storeClientPaymentGateway']);
    Route::post('get-client-payment-cateway',[PaymentGatewayController::class,'getClientPaymentGateway']);
    Route::post('client-gatewey-deactive',[PaymentGatewayController::class,'clientGateweyDeactive']);

    //Mail Template
    Route::post('get-mail-list',[MailController::class,'getMailList']);
    Route::post('add-mail',[MailController::class,'addMailTemplate']);
    Route::post('view-mail',[MailController::class,'viewMail']);
    Route::post('get-mail-by-id',[MailController::class,'getMailById']);
    Route::post('update-mail',[MailController::class,'editMailTemplate']);

    // Dashboard
    Route::get('get-dashboard', [DashboardController::class,'getDashboard']);
    Route::post('get-month-wise-delivered-order',[DashboardController::class,'monthWiseDeliveredOrders']);
    Route::post('get-month-wise-sales',[DashboardController::class,'monthWiseSales']);

});