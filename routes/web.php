<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\{
  Controller
};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


//New changes static pages
Route::get('page/{id}',[Controller::class,'page']);

Route::get('export/{menu_id}/{client_id}/{type}',[Controller::class,'export']);
Route::get('blank-sample-menu-item/{client_id}',[Controller::class,'blankSampleMenuItemExport']);

Route::middleware('checkURL')->get('/{any}', function () {
  return view('layouts.app');
})->where('any', '.*');
