<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\DefaultSetting;

class DefaultSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $defaultSetting = [
            ['settings_key' => 'system_start_time', 'setting_values' => '10:00:00'],
            ['settings_key' => 'system_end_time', 'setting_values' => '21:30:00'],
            ['settings_key' => 'driver_assign_km', 'setting_values' => '1000'],
            ['settings_key' => 'primary_lang', 'setting_values' => 'en'],
            ['settings_key' => 'secondary_lang', 'setting_values' => ''],
            ['settings_key' => 'restaurant_name', 'setting_values' => 'Restaurant'],
            ['settings_key' => 'driver_name', 'setting_values' => 'Driver'],
            ['settings_key' => 'timezone', 'setting_values' => 'Asia/Kolkata'],
            ['settings_key' => 'currency', 'setting_values' => '&#x20B9;'],
            ['settings_key' => 'country_code', 'setting_values' => '+91'],
            ['settings_key' => 'app_name', 'setting_values' => 'Ressto'],
            ['settings_key' => 'app_logo', 'setting_values' => 'food_logo_1691499050988.png'],
            ['settings_key' => 'flat_icon', 'setting_values' => 'food_flat_icon_16904421299610.png'],
            ['settings_key' => 'is_system_available', 'setting_values' => 'Yes'],
            ['settings_key' => 'sendgrid_key', 'setting_values' => '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'],
            ['settings_key' => 'is_referral_enable', 'setting_values' => 'No'],
            ['settings_key' => 'referral_amount', 'setting_values' => '0'],
            ['settings_key' => 'referee_amount', 'setting_values' => '0']
        ];

        foreach($defaultSetting as $key => $def){
            if(blank(DefaultSetting::where('settings_key',$def['settings_key'])->first())){
                DefaultSetting::create($def);
            }
        }
    }
}
