<?php

namespace Database\Seeders;

use App\Models\RestaurantSetting as ModelsRestaurantSetting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RestaurantSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $restaurantSetting = [
            ['restaurant_id' => 1, 'settings_key' => 'is_delivery', 'setting_values' => 'true'],
            ['restaurant_id' => 1, 'settings_key' => 'is_pickup', 'setting_values' => 'true'],
            ['restaurant_id' => 1, 'settings_key' => 'is_table_reservation', 'setting_values' => 'true'],
            ['restaurant_id' => 1, 'settings_key' => 'is_dine_in', 'setting_values' => 'true'],
            ['restaurant_id' => 1, 'settings_key' => 'is_menu_based_timing', 'setting_values' => 'true'],
        ];

        foreach($restaurantSetting as $key => $res){
            if(blank(ModelsRestaurantSetting::where('settings_key',$res['settings_key'])->where('restaurant_id',$res['restaurant_id'])->first())){
                ModelsRestaurantSetting::create($res);
            }
        }
    }
}
