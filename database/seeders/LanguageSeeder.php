<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $languages = [
            ['abbrivation' => 'en', 'name' => 'English' , 'status' => '1'],
            ['abbrivation' => 'ar', 'name' => 'Arabic' , 'status' => '1'],
            ['abbrivation' => 'es', 'name' => 'Spanish' , 'status' => '1'],
            ['abbrivation' => '', 'name' => 'No Secondary Language' , 'status' => '0'],
            ['abbrivation' => 'th', 'name' => 'Thai' , 'status' => '1'],
            ['abbrivation' => 'de', 'name' => 'German' , 'status' => '1'],
            ['abbrivation' => 'ru', 'name' => 'Russian' , 'status' => '1'],
            ['abbrivation' => 'fr', 'name' => 'France' , 'status' => '1']
        ];

        foreach($languages as $key => $lan){
            if(blank(Language::where('abbrivation',$lan['abbrivation'])->first())){
                Language::create($lan);
            }
        }
    }
}
