<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ApiTextMessage;

class ApiTextMessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $apitextmsg = [
            ["key" => "Setting","message" => "Settings Details","message_another_lang" => "સેટિંગ્સ વિગતો","client_id" => 1],
            ["key" => "Register","message" => "User register","message_another_lang" => "વપરાશકર્તા રજીસ્ટર","client_id" => 1],
            ["key" => "Not Found","message" => "No user found","message_another_lang" => "કોઈ વપરાશકર્તા મળ્યો નથી","client_id" => 1],
            ["key" => "404","message" => "Not Found","message_another_lang" => "મળ્યું નથી","client_id" => 1],
            ["key" => "Params Missing","message" => "Missing parameters","message_another_lang" => "ગુમ થયેલ પરિમાણો","client_id" => 1],
            ["key" => "Validation","message" => "Email or password invalid","message_another_lang" => "ઇમેઇલ અથવા પાસવર્ડ અમાન્ય છે","client_id" => 1],
            ["key" => "User Account Blocked","message" => "Your account is blocked for some reasons. Please contact support to know the further details","message_another_lang" => "તમારું એકાઉન્ટ કેટલાક કારણોસર અવરોધિત છે. વધુ વિગતો જાણવા માટે એડમિનનો સંપર્ક કરો","client_id" => 1],
            ["key" => "Login Successfully","message" => "Login successfully","message_another_lang" => "લ Loginગિન સફળતાપૂર્વક","client_id" => 1],
            ["key" => "Wrong Password","message" => "Invalid Password","message_another_lang" => "અમાન્ય પાસવર્ડ","client_id" => 1],
            ["key" => "Check Email","message" => "This Email address is already in use","message_another_lang" => "આ ઈ - મેઈલ અડ્રેસ્સ પેહલે થીજ ઉપયોગ માં છે","client_id" => 1],
            ["key" => "Mobile Number Already Register","message" => "Mobile Number Already registered","message_another_lang" => "મોબાઇલ નંબર પહેલેથી જ નોંધાયેલ છે","client_id" => 1],
            ["key" => "User Created","message" => "User created successfully","message_another_lang" => "વપરાશકર્તા સફળતાપૂર્વક બનાવેલ છે","client_id" => 1],
            ["key" => "Failed","message" => "fail to create user","message_another_lang" => "વપરાશકર્તા બનાવવામાં નિષ્ફળ","client_id" => 1],
            ["key" => "Details","message" => "Restaurant details","message_another_lang" => "રેસ્ટોરન્ટ વિગતો","client_id" => 1],
            ["key" => "Rating","message" => "No rating found","message_another_lang" => "કોઈ રેટિંગ મળી નથી","client_id" => 1],
            ["key" => "Listing","message" => "Master menu and item list","message_another_lang" => "રેસ્ટ restaurantરન્ટ મેનૂ સૂચિ","client_id" => 1],
            ["key" => "Menu","message" => "No menu found","message_another_lang" => "કોઈ મેનૂ મળ્યું નથી","client_id" => 1],
            ["key" => "User Not Registered","message" => "User not registered","message_another_lang" => "વપરાશકર્તા નોંધાયેલ નથી","client_id" => 1],
            ["key" => "Referral code invalid","message" => "Referral code invalid","message_another_lang" => "","client_id" => 1],
            ["key" => "Email Exists","message" => "User with this email already exists","message_another_lang" => "આ ઇમેઇલનો વપરાશકર્તા પહેલાથી અસ્તિત્વમાં છે","client_id" => 1]
        ];

        foreach($apitextmsg as $key => $apitext){
            if(blank(ApiTextMessage::where('key',$apitext['key'])->first())){
                ApiTextMessage::create($apitext);
            }
        }
    }
}
