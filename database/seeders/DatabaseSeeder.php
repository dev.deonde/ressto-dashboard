<?php

namespace Database\Seeders;
use Database\Seeders\DefaultSettingSeeder;
use Database\Seeders\LanguageSeeder;
use Database\Seeders\PermissionSeeder;
use Database\Seeders\RoleSeeder;
use Database\Seeders\SampleImportSeeder;
use Database\Seeders\TagSeeder;
use Database\Seeders\ApiTextMessageSeeder;
use Database\Seeders\FaqSeeder;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            DefaultSettingSeeder::class,
            LanguageSeeder::class,
            PermissionSeeder::class,
            RoleSeeder::class,
            SampleImportSeeder::class,
            TagSeeder::class,
            ApiTextMessageSeeder::class,
            RestaurantSettingSeeder::class,
            FaqSeeder::class,
            PaymentGateways::class
        ]);
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
