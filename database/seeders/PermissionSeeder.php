<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $permissions = [
            ['name' => 'Order Read', 'guard_name' => 'client'],
            ['name' => 'Order Write', 'guard_name' => 'client'],
            ['name' => 'Delivery Zone Read', 'guard_name' => 'client'],
            ['name' => 'Delivery Zone Write', 'guard_name' => 'client'],
            ['name' => 'Promo Code Read', 'guard_name' => 'client'],
            ['name' => 'Promo Code Write', 'guard_name' => 'client'],
            ['name' => 'Content Pages Read', 'guard_name' => 'client'],
            ['name' => 'Content Pages Write', 'guard_name' => 'client'],
            ['name' => 'Cuisine Read', 'guard_name' => 'client'],
            ['name' => 'Cuisine Write', 'guard_name' => 'client'],
            ['name' => 'Reports Read', 'guard_name' => 'client'],
            ['name' => 'Reports Write', 'guard_name' => 'client'],
            ['name' => 'Eagle view Read', 'guard_name' => 'client'],
            ['name' => 'Banner Read', 'guard_name' => 'client'],
            ['name' => 'Banner Write', 'guard_name' => 'client'],
            ['name' => 'Location Read', 'guard_name' => 'client'],
            ['name' => 'Location Write', 'guard_name' => 'client'],
            ['name' => 'Category Read', 'guard_name' => 'client'],
            ['name' => 'Category Write', 'guard_name' => 'client'],
            ['name' => 'User & Access Read', 'guard_name' => 'client'],
            ['name' => 'User & Access Write', 'guard_name' => 'client'],
            ['name' => 'Dashboard Read', 'guard_name' => 'client'],
            ['name' => 'Dashboard Driver Read', 'guard_name' => 'client'],
            ['name' => 'Merchant Read', 'guard_name' => 'client'],
            ['name' => 'Merchant Write', 'guard_name' => 'client'],
            ['name' => 'Driver Read', 'guard_name' => 'client'],
            ['name' => 'Driver Write', 'guard_name' => 'client'],
            ['name' => 'Customer Read', 'guard_name' => 'client'],
            ['name' => 'Customer Write', 'guard_name' => 'client'],
            ['name' => 'Vendor Finance Read', 'guard_name' => 'client'],
            ['name' => 'Vendor Finance Write', 'guard_name' => 'client'],
            ['name' => 'Driver Finance Read', 'guard_name' => 'client'],
            ['name' => 'Driver Finance Write', 'guard_name' => 'client'],
            ['name' => 'FAQ Read', 'guard_name' => 'client'],
            ['name' => 'FAQ Write', 'guard_name' => 'client'],
            ['name' => 'Configuration Read', 'guard_name'  => 'client'],
            ['name' => 'Configuration Write', 'guard_name'  => 'client'],
            ['name' => 'Layout Read', 'guard_name'  => 'client'],
            ['name' => 'Layout Write', 'guard_name'  => 'client'],
            ['name' => 'Terminology Read', 'guard_name'  => 'client'],
            ['name' => 'Terminology Write', 'guard_name' => 'client'],
            ['name' => 'Logs Read', 'guard_name' => 'client'],
            ['name' => 'Logs Write', 'guard_name' => 'client'],
            ['name' => 'Call Center Read', 'guard_name' => 'client'],
            ['name' => 'Call Center Write', 'guard_name' => 'client'],
            ['name' => 'Web Layout Read', 'guard_name' => 'client'],
            ['name' => 'Web Layout Write', 'guard_name' => 'client'],
            ['name' => 'Email Template Read', 'guard_name' => 'client'],
            ['name' => 'Email Template Write', 'guard_name' => 'client'],
            ['name' => 'Chat Read', 'guard_name' => 'client'],
            ['name' => 'Chat Write', 'guard_name' => 'client'],
            ['name' => 'Sales Channel Read', 'guard_name' => 'client'],
            ['name' => 'Sales Channel Write', 'guard_name' => 'client'],
            ['name' => 'Brand Managment Read', 'guard_name' => 'client'],
            ['name' => 'Brand Managment Write', 'guard_name' => 'client'],
            ['name' => 'Tag Read', 'guard_name' => 'client'],
            ['name' => 'Tag Write', 'guard_name' => 'client'],
            ['name' => 'Send Notification Read', 'guard_name' =>'client'],
            ['name' => 'Send Notification Write', 'guard_name' => 'client'],
            ['name' => 'Menu Read', 'guard_name' =>'client'],
            ['name' => 'Menu Write', 'guard_name' => 'client'],
            ['name' => 'Outlet Read', 'guard_name' =>'client'],
            ['name' => 'Outlet Write', 'guard_name' => 'client'],
            ['name' => 'Setting Read', 'guard_name' => 'client'],
            ['name' => 'Setting Write', 'guard_name' => 'client'],
            ['name' => 'Addon Integration Read', 'guard_name' => 'client'],
            ['name' => 'Addon Integration Write', 'guard_name' => 'client'],
        ];

        foreach($permissions as $key => $per){
            if(blank(Permission::where('name',$per['name'])->first())){
                Permission::create($per);
            }
        }
    }
}
