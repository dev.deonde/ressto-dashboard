<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\SampleImport;

class SampleImportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $sample_import = [
            [
                'MENUNAME' => 'MENU NAME', 
                'MENUNAMESECONDARYLANGUAGE' => 'MENU NAME SECONDARY LANGUAGE',
                'MENUIMAGE' => 'MENU IMAGE',
                'ITEMNAME' => 'ITEM NAME',
                'ITEMNAMESECONDARYLANGUAGE' => 'ITEM NAME SECONDARY LANGUAGE',
                'ITEMDESCRIPTION' => 'ITEM DESCRIPTION',
                'ITEMDESCRIPTIONSECONDARYLANGUAGE' => 'ITEM DESCRIPTION SECONDARY LANGUAGE',
                'ITEMPRICE' => 'ITEM PRICE',
                'ITEMMRP' => 'ITEM MRP',
                'ITEMQUANTITY' => 'ITEM QUANTITY',
                'TAXCODE' => 'TAX CODE',
                'TYPE' => 'TYPE (VEG - 0, Non Veg - 1, None - 2)',
                'STATUS' => 'STATUS (Available - 1,Not Available-0)',
                'ITEMPACKAGINGCHARGE' => 'ITEM PACKAGING CHARGE',
                'IMAGE1' => 'IMAGE1 (Image name with extension of jpg,png)',
                'IMAGE2' => 'IMAGE2 (Image name with extension of jpg,png)',
                'IMAGE3' => 'IMAGE3 (Image name with extension of jpg,png)',
                'IMAGE4' => 'IMAGE4 (Image name with extension of jpg,png)',
                'IMAGE5' => 'IMAGE5 (Image name with extension of jpg,png)'
            ],
        ];

        foreach($sample_import as $key => $sample){
            if(blank(SampleImport::where('MENUNAME',$sample['MENUNAME'])->first())){
                SampleImport::create($sample);
            }
        }
    }
}
