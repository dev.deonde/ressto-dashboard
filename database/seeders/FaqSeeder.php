<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\FAQ;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faq = [
            ['client_id' => '1', 'question' => 'How to use Customer APP' , 'answer' => 'Testing Cust', 'type' => 'customer', 'status' => '1']
        ];

        foreach($faq as $key => $fa){
            if(blank(FAQ::where('question',$fa['question'])->first())){
                FAQ::create($fa);
            }
        }
    }
}
