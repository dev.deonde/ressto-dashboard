<?php

namespace Database\Seeders;

use App\Models\PaymentGateway;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PaymentGateways extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $permissions = [
            [
                'gateway_name' => 'razorpay', 
                'gateway_price' => '500', 
                'required_key'=>[
                    ['name'=>'razorpay merchant id', 'id'=>'razorpay_merchant_id'],
                    ['name'=>'razorpay key id', 'id'=>'razorpay_key_id'], 
                    ['name'=>'razorpay key secret', 'id'=>'razorpay_key_secret'] 
                ], 
                'gateway_icone'=>'razorpay.svg', 
                'successurl'=>'razorpay-success',
                'failurl' => 'razorpay-fail', 
                'walletrechargeurl'=>'razorpay-recharge-wallet', 
                'is_webview' => '1'
            ],
            [
                'gateway_name' => 'stripe', 
                'gateway_price' => '500', 
                'required_key'=>[
                    ['name'=>'stripe publishable key', 'id'=>'stripe_publishable_key'],
                    ['name'=>'stripe key secret', 'id'=>'stripe_key_secret'] 
                ], 
                'gateway_icone'=>'stripe.png', 
                'successurl'=>'stripe-success',
                'failurl' => 'stripe-fail', 
                'walletrechargeurl'=>'stripe-rechargewallet', 
                'is_webview' => '1'
            ],
            [
                'gateway_name' => 'paytm', 
                'gateway_price' => '500', 
                'required_key'=>[
                    ['name'=>'paytm merchant id', 'id'=>'paytm_merchant_id'],
                    ['name'=>'paytm key id', 'id'=>'paytm_key_id']
                ],  
                'gateway_icone'=>'paytm.png', 
                'successurl'=>'paytm-success',
                'failurl' => 'paytm-fail', 
                'walletrechargeurl'=>'paytm-recharge-wallet', 
                'is_webview' => '1'
            ],
            [
                'gateway_name' => 'cashfree', 
                'gateway_price' => '500', 
                'required_key'=>[
                    ['name'=>'cashfree client id', 'id'=>'cashfree_client_id'],
                    ['name'=>'cashfree client secret', 'id'=>'cashfree_client_secret']
                ],   
                'gateway_icone'=>'cashfree.png', 
                'successurl'=>'success',
                'failurl' => 'cancel', 
                'walletrechargeurl'=>'cashfree-recharge-wallet', 
                'is_webview' => '1'
            ],
        ];

        foreach($permissions as $key => $per){
            if(blank(PaymentGateway::where('gateway_name',$per['gateway_name'])->first())){
                PaymentGateway::create($per);
            }
        }
    }
}
