<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Tag;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $tags = [
            ['client_id' => 1, 'name' => 'Sweet' ,'text_color' => '#000000', 'background_color' => 'f2d9dc'],
            ['client_id' => 1, 'name' => 'Spicy', 'text_color' => '#d81313', 'background_color' => 'd3cfcf'],
            ['client_id' => 1, 'name' => 'Normal' ,'text_color' => '#032530', 'background_color' => 'f1f2f3'],
            ['client_id' => 1, 'name' => 'Hot', 'text_color' => '#032530', 'background_color' => 'f1f2f3'],
            ['client_id' => 1, 'name' => 'Chilled', 'text_color' => '#0c98df', 'background_color' => 'deeeff']
        ];

        foreach($tags as $key => $tag){
            if(blank(Tag::where('name',$tag['name'])->first())){
                Tag::create($tag);
            }
        }
    }
}
