<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customize_type', function (Blueprint $table) {
            $table->id();
            $table->string('type_name');
            $table->string('type_name_thai')->nullable();
            $table->integer('master_item_id');
            $table->enum('is_selection',['0','1'])->default('0')->comment('0 : single ,1 : multi selection');
            $table->string('is_min_selection');
            $table->float('selection_price')->nullable();
            $table->enum('is_veg_non',['0','1','2'])->default('0')->comment('0 : veg ,1 : non veg, 2: none');
            $table->enum('is_template',['0','1'])->default('0')->comment('0 : Not template ,1 : template');
            $table->string('template_name')->nullable();
            $table->enum('status',['0','1'])->default('0')->comment('0 : active ,1 : inactive');
            $table->integer('parent_id')->default('0');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customize_type');
    }
};
