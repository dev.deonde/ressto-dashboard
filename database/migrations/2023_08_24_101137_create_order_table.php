<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('restaurant_id');
            $table->bigInteger('client_id');
            $table->bigInteger('table_id')->nullable();
            $table->bigInteger('address_id');
            $table->string('payment_method',100);
            $table->string('transaction_id',100)->nullable();
            $table->string('order_status',50);
            $table->integer('kot_count')->default('0');
            $table->float('service_fee');
            $table->float('SGST');
            $table->float('CGST');
            $table->float('delivery_fee');
            $table->text('suggestion');
            $table->float('sub_total');
            $table->integer('single_selection');
            $table->string('multiple_selection',100);
            $table->float('total_amount');
            $table->float('braveges_amount');
            $table->text('shipping_address');
            $table->string('locality',400)->nullable();
            $table->string('area',150)->nullable();
            $table->string('city',150)->nullable();
            $table->string('zip',150)->nullable();
            $table->string('adress_clarification',50)->nullable();
            $table->string('delivery_latitude',500)->nullable();
            $table->string('delivery_longitude',250)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('confirm_date')->nullable();
            $table->timestamp('ontheway_date')->nullable();
            $table->timestamp('delivered_date')->nullable();
            $table->timestamp('arrived_date')->nullable();
            $table->enum('delivery_type',['0','1'])->default('0')->comment('0 : Right now assp, 1 : futuredate');
            $table->datetime('future_delivery_date')->nullable();
            $table->float('admin_commision')->default('0');
            $table->text('notes');
            $table->string('restaurant_service_fee',20)->default('0');
            $table->integer('driver_tip')->default('0');
            $table->timestamp('cancelled_date')->nullable();
            $table->integer('is_same_apartment')->default('0');
            $table->float('last_cancelled_charge')->default('0');
            $table->integer('last_cancelled_orderid')->default('0');
            $table->timestamp('transaction_paid_date')->nullable();
            $table->integer('finance_transaction_id')->nullable();
            $table->float('chef_penalty_charge')->default('0');
            $table->text('cancelled_desc');
            $table->enum('is_rating_display', ['0','1','2'])->default('0')->comment('0: pending,1:rating given .2: Not given');
            $table->integer('is_refundable')->default('0')->comment('0 - Non Refundable 1-Refundable');
            $table->string('delivery_pickup_types',50)->default('Delivery');
            $table->time('delivery_pickup_start_time')->nullable();
            $table->time('delivery_pickup_end_time')->nullable();
            $table->timestamp('driver_transaction_paid_date')->nullable();
            $table->integer('driver_finance_transaction_id')->nullable();
            $table->string('device_type',15)->default('');
            $table->string('app_version_code',50)->default('');
            $table->timestamp('readytoserve_date')->useCurrent();
            $table->string('distance_km',50)->default('');
            $table->string('distance_mintue',50)->default('');
            $table->string('driver_earning',50)->default('0');
            $table->integer('contact_less_delivery')->default('0');
            $table->text('delivered_note');
            $table->timestamp('awaitpayment_date')->nullable();
            $table->integer('changeto_cod_by')->nullable();
            $table->timestamp('changeto_cod_at')->nullable();
            $table->text('changeto_cod_note');
            $table->string('wallet_amount')->default('0');
            $table->float('amount')->default('0');
            $table->float('itemtaxslab')->default('0');
            $table->string('payment_type',100)->nullable();
            $table->integer('user_access_id')->nullable();
            $table->float('saved_amount')->default('0');
            $table->integer('delivery_zone')->nullable();
            $table->enum('is_dunzo_driver',['0','1'])->default('0');
            $table->float('packaging_charge')->default('0');
            $table->float('convince_charge')->default('0');
            $table->enum('is_load_sharing_driver',['0','1'])->default('0')->comment('0: not load sharing order,1: load sharing order');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
