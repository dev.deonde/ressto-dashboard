<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('faq', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id');
            $table->text('question')->nullable();
            $table->text('answer')->nullable();
            $table->string('type')->default('customer');
            $table->integer('status')->default('1');
            $table->text('question_another_lang')->nullable();
            $table->text('answer_another_lang')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('faq');
    }
};
