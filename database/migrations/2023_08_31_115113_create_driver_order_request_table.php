<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('driver_order_request', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('driverid');
            $table->bigInteger('orderid');
            $table->integer('order_type')->default('1');
            $table->string('order_status');
            $table->text('reject_order_reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('driver_order_request');
    }
};
