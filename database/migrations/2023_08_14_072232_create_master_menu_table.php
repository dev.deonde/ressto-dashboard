<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('master_menu', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id');
            $table->string('menu_name');
            $table->string('menu_name_thai')->nullable();
            $table->text('menu_description')->nullable();
            $table->string('image')->nullable();
            $table->integer('reorder_data')->nullable();
            $table->enum('status',['0', '1'])->default('0')->comment('0 : active , 1 : inactive');
            $table->integer('is_selling_time_slot')->nullable();
            $table->integer('parent_menu_id')->default('0');
            $table->enum('is_display_image',['0', '1'])->default('1')->comment('0 : No , 1 : Yes');
            $table->string('selling_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('master_menu');
    }
};
