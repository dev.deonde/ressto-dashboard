<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders_transaction', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_id');
            $table->text('transaction_id');
            $table->text('val_id')->nullable();
            $table->text('bank_tran_id')->nullable();
            $table->float('amount');
            $table->string('status')->nullable();
            $table->string('currency')->nullable();
            $table->string('type')->nullable();
            $table->text('refund_ref_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders_transaction');
    }
};
