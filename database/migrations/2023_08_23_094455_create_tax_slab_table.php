<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tax_slab', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id');
            $table->float('tax_per');
            $table->text('tax_name')->nullable();
            $table->string('tax_uniquecode')->nullable();
            $table->text('tax_name_other')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tax_slab');
    }
};
