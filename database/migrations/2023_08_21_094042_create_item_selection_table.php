<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('item_selection', function (Blueprint $table) {
            $table->id();
            $table->integer('customize_type_id');
            $table->integer('parent_item_id')->default('0');
            $table->string('selection_name');
            $table->string('selection_name_thai')->nullable();
            $table->float('selection_price');
            $table->enum('is_default',['0','1'])->default('0')->comment('0 : default ,1 : not default');
            $table->string('is_selection')->default('0');
            $table->integer('is_min_selection')->default('0');
            $table->enum('status',['0','1'])->default('1')->comment('0 : Inactive ,1 : active');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('item_selection');
    }
};
