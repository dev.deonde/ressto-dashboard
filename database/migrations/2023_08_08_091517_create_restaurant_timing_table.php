<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('restaurant_timing', function (Blueprint $table) {
            $table->id();
            $table->integer('restaurant_id');
            $table->text('days');
            $table->time('start_time');
            $table->time('end_time');
            $table->enum('types',['is_delivery', 'is_pickup', 'is_table_reservation','is_dine_in','is_menu_based_timing'])->default('is_delivery')->comment('is_delivery : Delivery, is_pickup : Pickup/Takeaway, is_table_reservation : Table Reservation, is_dine_in : Dine in, is_menu_based_timing : Menu Based Timing');
            $table->enum('status',['0', '1'])->default('0')->comment('0 : active , 1 : inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('restaurant_timing');
    }
};
