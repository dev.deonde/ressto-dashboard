<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('client', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_client_id')->nullable();
            $table->string('client_name');
            $table->string('business_name');
            $table->string('email')->unique();
            $table->string('contact_number');
            $table->string('password');
            $table->enum('is_email_verified',['0', '1'])->default('0')->comment('0 : not verified , 1 : verified');
            $table->dateTime('subscription_date')->nullable();
            $table->date('trial_expried_date');
            $table->enum('platform',['1', '2'])->default('1')->comment('1 : signup , 2 : master admin');
            $table->enum('client_role',['1', '2','3'])->default('2')->comment('1 : Main User , 2 : client, 3 : sub user');
            $table->string('plan_notes')->nullable();
            $table->string('ip_address');
            $table->enum('status',['0', '1','2'])->default('2')->comment('0 : InActive ,1 : Active, 2 : Trail');
            $table->text('slug');
            $table->string('project_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('client');
    }
};
