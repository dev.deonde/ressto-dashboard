<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('frenchise_id')->default('0');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email');
            $table->string('password');
            $table->string('profileimage');
            $table->string('country_code')->default('+91');
            $table->string('contactno');
            $table->string('alternate_contactno')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->enum('availbility_status',['0','1'])->comment('1: avilable,0:not available');
            $table->enum('is_block',['1','0','2'])->default('2')->comment('1- yes, 0-no, 2-waiting for approval');
            $table->enum('is_login',['0','1'])->comment('1:login,0:not login');
            $table->string('is_language',20)->default('T');
            $table->string('accesstoken',50)->default('');
            $table->string('user_notification_sound',250)->nullable();
            $table->string('firstname_other_lang')->nullable();
            $table->string('lastname_other_lang')->nullable();
            $table->integer('is_test_user')->default('0')->comment('0:No,1:Yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('drivers');
    }
};
