<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('promo_code', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id');
            $table->string('coupon_name');
            $table->string('coupon_name_other_lang')->nullable();
            $table->string('coupon_code');
            $table->text('description');
            $table->text('description_other_lang')->nullable();
            $table->float('discount_percentage');
            $table->float('maximum_discount_amount')->nullable();
            $table->float('minimum_order_amount')->nullable();
            $table->datetime('start_datetime');
            $table->datetime('end_datetime');
            $table->string('per_user_usage')->default('1');
            $table->string('coupon_image');
            $table->tinyInteger('is_flat_discount')->default('0');
            $table->float('flat_discount')->nullable();
            $table->enum('status',['0','1'])->default('1')->comment('0-InActive,1-Active');
            $table->integer('is_admin_coupon')->default('0');
            $table->integer('max_user_count')->nullable();
            $table->string('orders_valid_for_first')->nullable();
            $table->integer('is_show_customer_app')->default('1');
            $table->string('coupon_type')->nullable();
            $table->text('show_display_text')->nullable();
            $table->text('show_display_text_another_lang')->nullable();
            $table->enum('promo_code_type',['1','2','3','4'])->default('1')->comment('1 - Percentage Discount, 2 - Flat Discount, 3 - Flat Cashback, 4 - Percentage Cashback');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('promo_code');
    }
};
