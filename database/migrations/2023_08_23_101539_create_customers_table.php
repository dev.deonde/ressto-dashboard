<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('client_id');
            $table->string('customer_id')->nullable();
            $table->string('unique_code',50)->nullable();
            $table->string('facebookid',70)->default('-1');
            $table->string('user_name');
            $table->string('last_name',100)->nullable();
            $table->string('email')->nullable();
            $table->string('birth_date',50)->nullable();
            $table->string('mobile_number',20)->nullable();
            $table->string('country_code',10)->nullable();
            $table->string('password',255);
            $table->string('remember_token',255);
            $table->string('country',20)->nullable();
            $table->string('city',50)->nullable();
            $table->string('area',50)->nullable();
            $table->integer('apartment')->nullable();
            $table->mediumText('address');
            $table->string('user_profile');
            $table->enum('status',['0','1','2'])->comment('0-Active,1-Inactive,2-pending');
            $table->string('time_zone',10);
            $table->string('os',);
            $table->string('model',);
            $table->string('app_version',10);
            $table->enum('user_role',['0','1','2','3','4'])->comment('0-admin,1-app_user, 2-restaurant_owner, 3-franchise,4-sub_admin');
            $table->timestamp('last_login_date')->useCurrentOnUpdate()->useCurrent();
            $table->enum('is_language',['T','E'])->default('T');
            $table->string('verify_alcohol_image',200)->default('');
            $table->string('alcohol_status',50)->default('Accept');
            $table->string('referral_code',100)->nullable();
            $table->string('reference_code',100)->nullable();
            $table->string('user_notification_sound',500)->nullable();
            $table->string('gplusid',250)->default('');
            $table->integer('is_cod_enable')->default('1');
            $table->string('firebase_localId')->nullable();
            $table->mediumText('notes');
            $table->enum('wallet_status',['active','inactive'])->default('active');
            $table->string('customise',20)->default('0');
            $table->integer('wallet_recharge_limit')->default('0');
            $table->integer('maximum_wallet_amount')->default('0');
            $table->string('forgot_password_otp');
            $table->integer('is_test_user')->default('0')->comment('0:No,1:Yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customers');
    }
};
