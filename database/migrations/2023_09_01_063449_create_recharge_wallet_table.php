<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('recharge_wallet', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_id');
            $table->string('recharge_amount',20);
            $table->string('recharge_status',50)->default('pending');
            $table->string('transaction_id',50)->nullable();
            $table->enum('type',['recharge','spent','refund','cashback','referral'])->default('recharge');
            $table->string('ip_address',100)->nullable();
            $table->bigInteger('user_order_id')->nullable();
            $table->integer('device_type')->default('0')->comment('0-Mobile, 1-Web');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('recharge_wallet');
    }
};
