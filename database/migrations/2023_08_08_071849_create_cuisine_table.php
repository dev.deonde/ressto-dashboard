<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cuisine', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id');
            $table->string('cuisine_name');
            $table->string('cuisine_name_thai')->nullable();
            $table->enum('cuisine_active',['0', '1'])->default('0')->comment('0 : active , 1 : inactive');
            $table->text('cuisine_description')->nullable();
            $table->text('cuisine_description_thai')->nullable();
            $table->text('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cuisine');
    }
};
