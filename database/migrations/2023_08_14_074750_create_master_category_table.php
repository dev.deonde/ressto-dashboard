<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('master_category', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id');
            $table->integer('menu_id')->nullable();
            $table->string('name');
            $table->string('name_another_lang')->nullable();
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->enum('status',['0', '1'])->default('0')->comment('0 : active , 1 : inactive');
            $table->integer('reorder_data')->nullable();
            $table->string('color_code')->nullable();
            $table->string('text_color_code')->nullable();
            $table->enum('is_display_image',['0', '1'])->default('0')->comment('0 : no need to show image , 1 : show image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('master_category');
    }
};
