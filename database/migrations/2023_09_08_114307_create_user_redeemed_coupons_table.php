<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_redeemed_coupons', function (Blueprint $table) {
            $table->id();
            $table->integer('coupon_id');
            $table->integer('user_id');
            $table->unsignedBigInteger('order_id');
            $table->integer('client_id')->default('0');
            $table->float('original_price');
            $table->float('discount_price');
            $table->timestamp('redeem_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamps();
            $table->float('cashback')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_redeemed_coupons');
    }
};
