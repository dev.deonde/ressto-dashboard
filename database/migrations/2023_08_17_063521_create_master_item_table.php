<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('master_item', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id');
            $table->integer('main_category_id')->nullable();
            $table->integer('is_menu');
            $table->string('item_name');
            $table->string('item_name_thai')->nullable();
            $table->integer('quantity');
            $table->float('price');
            $table->float('mrp');
            $table->string('image')->default('');
            $table->float('item_packaging_charge')->nullable();
            $table->text('item_description')->nullable();
            $table->text('item_description_thai')->nullable();
            $table->enum('is_available',['0','1'])->default('0')->comment('0 : Active ,1 : InActive');
            $table->enum('is_featured',['0','1'])->default('0')->comment('0 : Active ,1 : InActive');
            $table->string('qty_reset')->default('Daily');
            $table->enum('item_type',['0','1','2'])->default('0')->comment('0 : VEG ,1 : NonVeg, 2 : None');
            $table->integer('reorder')->nullable();
            $table->integer('is_selling_time_slot')->default('0');
            $table->enum('is_customizable',['0','1'])->comment('0 : No ,1 : Yes');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('master_item');
    }
};
