<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('master_item_time_slots', function (Blueprint $table) {
            $table->id();
            $table->integer('master_item_id')->nullable();
            $table->integer('is_wholeday')->nullable();
            $table->date('selling_start_date')->nullable();
            $table->date('selling_end_date')->nullable();
            $table->time('selling_start_time')->nullable();
            $table->time('selling_end_time')->nullable();
            $table->integer('timeslot_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('master_item_time_slots');
    }
};
