<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer_referral', function (Blueprint $table) {
            $table->id();
            $table->integer('register_user_id');
            $table->integer('referral_user_id');
            $table->integer('referral_amount');
            $table->enum('is_referee_user_get_amount',['0','1'])->default('0')->comment('0: not get ref amount,1:get ref amount');
            $table->integer('user_order_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customer_referral');
    }
};
