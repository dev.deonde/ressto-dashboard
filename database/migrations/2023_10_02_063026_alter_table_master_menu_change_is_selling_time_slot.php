<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('master_menu', function (Blueprint $table) {
            $table->integer('is_selling_time_slot')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('master_menu', function (Blueprint $table) {
            $table->integer('is_selling_time_slot')->nullable()->change();
            
        });
    }
};
