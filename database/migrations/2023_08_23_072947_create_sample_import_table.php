<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sample_import', function (Blueprint $table) {
            $table->id();
            $table->text('MENUNAME');
            $table->text('MENUNAMESECONDARYLANGUAGE');
            $table->text('MENUIMAGE');
            $table->text('ITEMNAME');
            $table->text('ITEMNAMESECONDARYLANGUAGE');
            $table->text('ITEMDESCRIPTION');
            $table->text('ITEMDESCRIPTIONSECONDARYLANGUAGE');
            $table->text('ITEMPRICE');
            $table->text('ITEMMRP');
            $table->text('ITEMQUANTITY');
            $table->text('TAXCODE');
            $table->text('TYPE');
            $table->text('STATUS');
            $table->text('ITEMPACKAGINGCHARGE');
            $table->text('IMAGE1');
            $table->text('IMAGE2');
            $table->text('IMAGE3');
            $table->text('IMAGE4');
            $table->text('IMAGE5');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sample_import');
    }
};
