<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('shipment_address', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('apartmentid');
            $table->bigInteger('user_id');
            $table->string('address',400)->default('');
            $table->string('locality',400)->default('');
            $table->string('area',150)->default('');
            $table->string('city',255)->nullable();
            $table->string('zip',255)->default('');
            $table->string('city_name',250)->nullable();
            $table->string('state',250)->nullable();
            $table->string('adress_clarification',50)->default('');
            $table->tinyInteger('is_primary_address')->default('1')->comment('0-InActive,1-Active');
            $table->string('latitude');
            $table->string('longitude');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('shipment_address');
    }
};
