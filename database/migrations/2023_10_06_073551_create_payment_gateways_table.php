<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payment_gateways', function (Blueprint $table) {
            $table->id();
            $table->text('gateway_name');
            $table->float('gateway_price')->default('0');
            $table->string('gateway_icone')->nullable();
            $table->text('successurl');
            $table->text('failurl');
            $table->text('walletrechargeurl');
            $table->integer('is_webview')->default('1');
            $table->string('required_key')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payment_gateways');
    }
};
