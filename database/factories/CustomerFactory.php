<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Customer>
 */
class CustomerFactory extends Factory
{
    protected $model = Customer::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "client_id" =>  1,
            "user_name" =>  fake()->name(),
            "email" =>  fake()->unique()->safeEmail(),
            "password" =>  "$2y$10$06Sbv.rXO7FSezCKrSvHIuK1Mv58oLSMPrKEYryFeBTywzToj.TAu",
            "remember_token" =>  Str::random(10),
            "address" => fake()->address(),
            "user_profile" =>  fake()->imageUrl(),
            "status" => '0',
            "time_zone" =>  fake()->timezone(),
            "os" =>  'os',
            "model" => 'iphone',
            "app_version" =>  '4.5.1',
            "user_role" =>  '1',
            "last_login_date" =>  fake()->dateTime(),
            "is_language" =>  'T',
            "notes" =>  fake()->text(),           

        ];
    }
}
