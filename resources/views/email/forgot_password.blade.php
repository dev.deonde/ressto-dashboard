<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8" />
  </head>
  <body>
    <h4>Hello {{ $data['name'] }},</h4>
    <h5>Seems like you have forgotten your password. Please follow the link below to update your password.</h5>
    <div>
        <p>Link:
            <a href="{{ $data['link'] }}">{{ $data['link'] }}</a>
        </p>
    </div>
  </body>
</html>
