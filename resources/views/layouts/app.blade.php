<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@php
$layout = isset($_COOKIE['layout']) ? $_COOKIE['layout'] : 'light';
$css_file = '';
switch ($layout) {
    case 'dark':
    $css_file = 'css/app-dark.css';
    break;
    case 'rtl':
    $css_file = 'css/app-rtl.css';
    break;
    default:
    $css_file = 'css/app.css';
    break;
}
$client_id = session()->get('client_id');
$client_id_login = session()->get('client_id_login');
if(blank($client_id) && blank($client_id_login)){
    $client = App\Models\Client::where('email','admin@ressto.com')->first();
    if(!blank($client)){
        $client_id = $client->id;
        $client_id_login = $client->id;
    }
}
if (is_null($client_id_login) || $client_id == $client_id_login) {
    $settings = App\Models\VendorSetting::where('client_id', $client_id)->get();
    $google_map_key = App\Models\GeneralSetting::where('client_id', $client_id)->where('settings_key','google_map_key')->first();
    if(!blank($google_map_key)){
        $google_map_key = $google_map_key->setting_values;
    }
} else {
    $google_map_key = App\Models\GeneralSetting::where('client_id', $client_id_login)->where('settings_key','google_map_key')->first();
    if(!blank($google_map_key)){
        $google_map_key = $google_map_key->setting_values;

    }
    $settings = App\Models\VendorSetting::where('client_id', $client_id_login)->get();
}

@endphp

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex, nofollow">
    <meta name="googlebot" content="noindex, nofollow">

    @if ($client_id)
        <title>
            @yield('title', config('constant.APP_NAME'))
        </title>
    @else
        <title>
            @yield('title', config('constant.APP_NAME'))
        </title>
    @endif

    <link href="{{ url('/') }}/css/app.css" rel="stylesheet" id="layout-css">
    <link href="{{ url('/') }}/css/custom-new.css" rel="stylesheet" id="layout-css">

</head>

<body>
    <div id="app">
        <app></app>
    </div>
    <!-- built files will be auto injected -->

    @stack('scripts')
    {{-- <script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> --}}
    <script src="{{ config('constant.Jquery') }}jquery-3.5.1.slim.min.js" defer></script>
    <script type="text/javascript" src="{{ config('constant.JS') }}script.js"></script>
    <script>
        window.default_locale = "en";
        window.fallback_locale = "en";
        window.messages = @json($messages);
        // must include slash ("/") at end

        window.applogo = "{{isset($settings->settings) ? $settings->settings['flat_icon'] : ''}}";
        window.folderName = "";
        window.google_map_key = "{{$google_map_key}}";
    </script>
    @if(!blank($google_map_key))

        <script async type="text/javascript" id="google_key" src="https://maps.googleapis.com/maps/api/js?key={{$google_map_key}}&libraries=drawing,places"></script>
    @endif

    <script src="{{ config('constant.JS') }}manifest.js" defer></script>
    <script src="{{ config('constant.JS') }}vendor.js" defer></script>
    <script src="{{ config('constant.JS') }}app.js" defer></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

</body>

</html>
