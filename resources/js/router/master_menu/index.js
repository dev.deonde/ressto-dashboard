const master_menu = [
    {
        path: '/menu',
        name: 'menu',
        component: () => import('../../views/master_menu/menu.vue')
    },
    {
        path: '/menu/customization/:master_item_id',
        name: 'menu-customization',
        component: () => import('../../views/master_menu/add-customization')
    },
    {
        path: '/menu/sub-customization/:id',
        name: 'sub-customization',
        component: () => import('../../views/master_menu/sub-customization')
    }
]

export default master_menu;