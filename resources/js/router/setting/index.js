const setting = [
    {
        path: "/settings/web-layout-setting",
        name: "web-layout-setting",
        component: () => import('../../views/settings/web-layout-setting.vue')
    },
    {
        path: "/settings/setting",
        name: "setting",
        component: () => import('../../views/settings/setting.vue')
    },
    {
        path: "/settings/addon-integration",
        name: "addon-integration",
        component: () => import('../../views/settings/addon-integration.vue')
    }
]

export default setting;