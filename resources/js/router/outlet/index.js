const outlet = [
    {
        path:"/outlet",
        name:"outlet",
        component: () => import('../../views/restaurant_outlet/list')
    }
]

export default outlet;