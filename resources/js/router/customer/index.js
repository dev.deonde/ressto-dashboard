const customer = [
    {
        path: '/customer',
        name: 'customer-list',
        component: () => import('../../views/customer/list.vue')
    },
    {
      path: '/customer/details/:user_id',
      name: 'customer-detail',
      component: () => import ('../../views/customer/details.vue')
    },
]

export default customer;