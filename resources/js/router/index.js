import Vue from 'vue'
import VueRouter from 'vue-router'
import Auth from './auth/index'
import App from '../views/App.vue'
import Dashboard from './dashboard/index'
import { menuItems } from '../components/menu';
import MasterMenu from './master_menu/index'
import Customer from './customer/index'
import PromoCode from './promo_code/index'
import Outlet from './outlet/index'
import Order from './order/index'
import Mail from './mail/index'
import Setting from './setting/index'
import SalesChannel from './sales_channel/index'
import ContentPage from './content_page/index'

Vue.use(VueRouter)

const urls = menuItems.reduce((m,i)=> i.subItems?m.concat(i.subItems):m.concat(i),[])

const routes = [
    ...Auth,
    ...Dashboard,
    ...MasterMenu,
    ...Customer,
    ...PromoCode,
    ...Outlet,
    ...Order,
    ...Setting,
    ...Mail,
    ...SalesChannel,
    ...ContentPage
]

const router = new VueRouter({
    mode: 'history',
    routes: [{
        // base: '',
        component: App,
        path: '/',
        children: routes
    },
    {
        path: '/',
        redirect: '/',
    }],
    scrollBehavior() {
        return {x: 0, y: 0}
    }
});

router.beforeEach((to, from, next) => {    
    $('body').addClass('sidebar-disable').removeClass('sidebar-enable');
    var noauth = ['login','signup','confirm-email','forgot-password','reset-password',];
    let nosideMenuRoute = [];
    const menu = routes.find(u => u.name == to.name);
    const sidemenu = urls.find(u => menu && ((menu.path.includes(u.link) && menu.path.startsWith(u.link)) || ( menu.link == u.link)));
    if (!noauth.includes(to.name) ){
        if(!Vue.prototype.$auth || !Vue.prototype.$auth.setting){
            Vue.prototype.$auth.setUser((type)=> {
            if(noauth.includes(type)) next({name: type})
            else {                
                if(Vue.prototype.$auth.user.login_type != 'client' || menu.access || Vue.prototype.$auth.user.access.some(s => sidemenu.permissions.some(p=>p.includes(s)))){
                next()
                }else{
                // error_message('Permission required')
                if (from.name) {
                    next({name: from.name});
                } else {
                    next({name: 'permission-page'});
                }
                }
            }
            });
        }
        else {
            let menuAccess = menu ? menu.access : (nosideMenuRoute.includes(to.name) ? true : undefined);
            if(Vue.prototype.$auth.user.login_type != 'client' || menuAccess || (sidemenu && Vue.prototype.$auth.user.access.some(s => sidemenu.permissions.some(p=>p.includes(s))))){
                next()
            }else{
            // error_message('Permission required')
            if (from.name) {

                next({name: from.name});
            } else {
                next({name: 'login'});
            }
            }
        }
    }
    else {
        next();
    }
  });

export default router
