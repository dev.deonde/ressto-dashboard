const promo_code = [
    {
        path: '/promocode',
        name: 'promocode',
        component: () => import('../../views/promo_code/index.vue')
    },
    {
        path: '/promocode/add',
        name: 'promocode-add',
        component: () => import('../../views/promo_code/add.vue')
    },
    {
        path: '/promocode/edit/:id',
        name: 'promocode-edit',
        component: () => import('../../views/promo_code/edit.vue')
    }
]

export default promo_code;