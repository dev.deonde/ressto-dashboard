const sales_channel = [
    {
        path: '/sales-channel',
        name: "sales-channel",
        component: () => import('../../views/sales_channel/list.vue')        
    }
]

export default sales_channel;