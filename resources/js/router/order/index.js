const order = [
    {
        path:'/orders/list',
        name:'orders-list',
        component: () => import('../../views/orders/main.vue')
    }
]

export default order;