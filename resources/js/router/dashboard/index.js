const dashboard = [
    {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import('../../views/dashboard.vue')
    }
]

export default dashboard;
