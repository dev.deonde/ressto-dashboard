
const auth = [
    {
        path: '/',
        name: 'login',
        component: () => import('../../views/auth/login.vue')
    },
    // {
    //     path: '/login',
    //     name: 'login',
    //     component: () => import('../../views/auth/login.vue')
    // },
    {
        path: '/signup',
        name: 'signup',
        component: () => import('../../views/auth/signup.vue')
    },
    {
        path: '/confirm-email/:client_id',
        name: 'confirm-email',
        component: () => import('../../views/auth/confirm_email.vue')
    },
    {
        path:'/forgot-password',
        name:'forgot-password',
        component: () => import('../../views/auth/forgot_password.vue')
    },
    {
        path:'/reset-password/:token',
        name:'reset-password',
        component: () => import('../../views/auth/reset_password.vue')
    },
    {
        path:'/profile',
        name:'profile',
        component: () => import('../../views/profile/index.vue'),
        access:true
    }



]

export default auth;
