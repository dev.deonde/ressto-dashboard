const content_page = [
  {
    path: '/content-page',
    name: 'content-page',
    component: () => import('../../views/content_page/list.vue')
  },
  {
    path: '/content-page/add',
    name: 'content-page-add',
    component: () => import('../../views/content_page/add.vue')
  },
  {
    path: '/content-page/edit/:id',
    name: 'content-page-edit',
    component: () => import('../../views/content_page/edit.vue')
  }
]
export default content_page;