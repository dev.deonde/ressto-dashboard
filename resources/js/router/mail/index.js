const mail = [
  {
    path:"/mail",
    name:"mail-list",
    component: () => import('../../views/mail/list.vue')
  },
  {
    path:"/mail/add",
    name:"add-mail",
    component: () => import('../../views/mail/add.vue')
  },
  {
    path: '/mail/edit/:id',
    name: 'edit-mail',
    component: () => import('../../views/mail/edit.vue')
  },
  {
    path: '/mail/view-page/:id',
    name: 'view-mail',
    component: () => import('../../views/mail/view-page.vue')
  },
]

export default mail;