const customersData = [
    {
        id: 1,
        font: 'D',
        username: 'Stephen Rash',
        phone: '325-250-1106',
        address: 'East',
        status: 'Active'
    },
    {
        id: 2,
        image: 'https://dev.deonde.co/deonde/public/images/users/avatar-2.jpg',
        username: 'Juan Mays',
        phone: '443-523-4726',
        address: 'West',
        status: 'Inactive',
    },
    {
        id: 3,
        image: 'https://dev.deonde.co/deonde/public/images/users/avatar-3.jpg',
        username: 'Scott Henry',
        phone: '704-629-9535',
        email: 'ScottHenry@jourrapide.com',
        address: 'North',
        status: 'Inactive'
    },
    {
        id: 4,
        font: 'M',
        username: 'Cody Menendez',
        phone: '701-832-5838',
        address: 'West',
        status: 'Inactive'
    },
    {
        id: 5,
        image: 'https://dev.deonde.co/deonde/public/images/users/avatar-4.jpg',
        username: 'Jason Merino',
        phone: '706-219-4095',
        address: 'East',
        status: 'Active'
    },
    {
        id: 6,
        image: 'https://dev.deonde.co/deonde/public/images/users/avatar-5.jpg',
        username: 'Kyle Aquino',
        phone: '415-232-5443',
        address: 'North',
        status: 'Active'
    },
    {
        id: 7,
        font: 'J',
        username: 'David Gaul',
        phone: '314-483-4679',
        address: 'West',
        status: 'Inactive'
    },
    {
        id: 8,
        image: 'https://dev.deonde.co/deonde/public/images/users/avatar-5.jpg',
        username: 'John McCray',
        phone: '253-661-7551',
        address: 'East',
        status: 'Inactive'
    }
];

export { customersData };