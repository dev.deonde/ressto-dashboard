import './bootstrap';
import Vue from 'vue';
window.Vue = Vue;
import App from './views/App.vue';
import router from './router';
import { BootstrapVue } from 'bootstrap-vue';
import Lang from 'lang.js';
import Vuelidate from "vuelidate";
import AuthPlugin from './plugins/authPlugin';
import VueLazyLoad from 'vue-lazyload';
import Toasted from 'vue-toasted';
import pagination from "laravel-vue-pagination";
import Moment from 'moment';
import VueApexCharts from "vue-apexcharts";

import config from "./config"

Vue.use(BootstrapVue);
Vue.use(Vuelidate);

Vue.use(pagination);

Vue.use(AuthPlugin)
Vue.component("apexchart", VueApexCharts);
Vue.use(VueLazyLoad, {
    preLoad: 1.3,
    error: config.no_image,
    // loading: ,
    attempt: 1
  })

Vue.use(Toasted, {
    duration: 4000,
    position: 'top-right',
    action : {
      onClick : (e, toastObject) => {
          toastObject.goAway(0);
      }
    }
});

const default_locale = window.default_locale;
const fallback_locale = window.fallback_locale;
const messages = window.messages;

Vue.prototype.trans = new Lang( { messages, locale: default_locale, fallback: fallback_locale } );
Vue.prototype.moment = Moment;

Vue.use(Moment);

const app = new Vue({
    el: "#app",
    router: router,
    components: { App }
});
