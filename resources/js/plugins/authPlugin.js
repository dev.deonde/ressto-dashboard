
let instance;
import {error_message, set_token, set_axios_defaults, get_token, remove_token,set_partner_login,set_partner_token } from '../Helper/helper';

import config from './../config'
import { authService } from './../services';
import Vue from 'vue';

export const useAuth = (options = {}) => {
  if (instance) return instance;
  instance = new Vue({
    data() {
      return {
        user: null,
        logging_done: false,
        languages: [],
        orders: [],
        partner_login:false
      }
    },
    methods: {
      login(request, callback) {
        authService.loginClient(request).then((response) => {
          let token = response.data.result.token;
          // if (this.partner_login) {
          //   set_partner_login();
          // }
          if (token) {
            this.user = response.data.result.user;
            this.setting = response.data.result.userDetail;
            this.logging_done = true;
            if(response.data.result.languages){
              this.setLanguages(response.data.result.languages)
            }
            // if (this.partner_login) {
            //   set_partner_token(token)
            // } else {
            //   set_token(token)
            // }
            set_token(token)
            set_axios_defaults(token)
            let menuname = 'dashboard';
            let checkdashboard = this.user.access.find(d => d == "Dashboard Read");
            if (!checkdashboard) {
              let notinMenu = [];
              let firstuseraccessread = this.user.access.filter(p => (p.includes('Read') && !notinMenu.includes(p)));
              if (firstuseraccessread) {
                let menu = menuItems.reduce((a, m) => {
                  if (m.subItems != undefined) {
                    var firstaccess = m.subItems.find(si => si.permissions.includes(firstuseraccessread[0]));
                  } else {
                    var firstaccess = m.permissions.includes(firstuseraccessread[0]);
                    if (firstaccess) {
                      firstaccess = m;
                    }
                  }
                  if (firstaccess) {
                    return firstaccess.link;
                  }
                  return a;
                }, null);
                if (menu) {
                  menuname = router.options.routes.find(r => r.path == menu).name;
                }
              }
            }
            callback(menuname)
          } else {
            error_message(response.data.message);
          }          
        });
      },

      checkLoginType() {
        let path = window.location.href;
        let token = get_token();
        let type = path.replace(config.appurl, '').split('/')[0];        
        set_axios_defaults(token);
        // if(type == 'client') {
          this.partner_login = true;
          return !!token
        // }
        // return !!token
      },

      setUser(callback, token = "") {
        if (token == "") {
          this.checkLoginType()
        }        
        if (this.checkLoginType()) {
          this.logging_done = true;
          authService.getUserDetail()
            .then(response => {
              this.user = response.data.users;
              this.setting = response.data.userDetail;
              if(response.data.languages){
                this.setLanguages(response.data.languages)
              }
              callback('dashboard')
            })
        } else {
          this.logging_done = false;
          callback('login')
        }
      },

      setLanguages(languages){
        this.langauges = languages;
      },

      getClientId(){
        return !this.user.parent_client_id?this.user.id:this.user.parent_client_id;
      },

      getBusinessName(){
        return this.user.business_name;
      },
      updateOrders(orders){
        this.orders = orders
      },
      hasPermission(permission){
        if(this.user.login_type == 'client')
        {
           return this.user && this.user.access.includes(permission);
        }
        else
        {
           return true;
        }
      },

      logout(callback) {
        authService.logout()
        .then(res => {
          this.logging_done = false;
          if (this.partner_login) {
            remove_partner_token();
            callback('vendor.login')
          } else {
            remove_token();
            callback('login')
          }
            
          // remove_token();
          //   callback('login')
        });
      },

      isWalletActive(){
        return (this.setting && this.user.wallet_status == 'active' && this.setting.wallet_status == 'active')
      },
      walletActive(){
        return (this.setting && this.setting.wallet_status == 'active')
      },
      getRestaurantName(){
        return this.setting && this.setting.restaurant_name || 'Restaurant';
      },

      getDriverName(){
        return this.setting && this.setting.driver_name || 'Driver';
      },
      isDriverAllocationOnConfirmed() {
        if (this.setting && this.setting.is_driver_allocation) {
          if (this.setting.is_driver_allocation == "true") {
            return true;
          }
        }
        return false;
      },
      
      getVendorDistanceMetric() {
        let distanceMetric = 'Km';
        if (this.setting && this.setting.distance_metric) {
          if (this.setting.distance_metric == 'kilometers') {
            distanceMetric = 'Km';
          } else if (this.setting.distance_metric == 'miles') {
            distanceMetric = 'Miles';
          }
        }
        return distanceMetric;
      },

      clientlogin(response) {
        let token = response.data.result.token;
        if (token) {
          this.user = response.data.result.user;
          this.setting = response.data.result.userDetail;
          this.logging_done = true;
          if(response.data.result.languages){
            this.setLanguages(response.data.result.languages)
          }
          set_token(token)
          set_axios_defaults(token)

          let menuname = 'dashboard';
          let checkdashboard = this.user.access.find(d => d == "Dashboard Read");
          if (!checkdashboard) {
            let notinMenu = [];
            let firstuseraccessread = this.user.access.filter(p => (p.includes('Read') && !notinMenu.includes(p)));
            if (firstuseraccessread) {
              let menu = menuItems.reduce((a, m) => {
                if (m.subItems != undefined) {
                  var firstaccess = m.subItems.find(si => si.permissions.includes(firstuseraccessread[0]));
                } else {
                  var firstaccess = m.permissions.includes(firstuseraccessread[0]);
                  if (firstaccess) {
                    firstaccess = m;
                  }
                }
                if (firstaccess) {
                  return firstaccess.link;
                }
                return a;
              }, null);
              if (menu) {
                menuname = router.options.routes.find(r => r.path == menu).name;
              }
            }
          }
          return menuname;
        } else {
          return 'login';
        }
      },
    }
  });
  return instance;

}

export default {
  install(Vue, options = {}) {
    Vue.prototype.$auth = useAuth(options);
  }
};
