import config from "../config";
import router from "../router"
import moment from 'moment';

const debounce = (func, delay) => {
  let debounceTimer
  return function() {
      const context = this
      const args = arguments
          clearTimeout(debounceTimer)
              debounceTimer
          = setTimeout(() => func.apply(context, args), delay)
  }
}

const set_axios_defaults = (token) => {
    window.axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    window.axios.defaults.headers.common["Accept"] = "application/json";
    window.axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
    window.axios.interceptors.response.use(
      res => res,
      debounce(err => {
        if (err.response.status === 401) {
          if(window.axios.defaults.headers.common.Authorization !== 'Bearer null'){
            Vue.toasted.error('Session Expired. Try login again',{
              duration: 5000
            });
          }
          remove_token()
          router.push({name: 'login'}).catch(()=>{})
          // router.push({name: 'login'}).catch(()=>{})
          // throw new Error(`${err.config.url} not found`);
        }
        else if (err.response.status === 500) {
          Vue.toasted.error('Server error',{
            duration: 5000
          });
          // throw new Error(`${err.config.url} not found`);
        }
        else if (err.response.status === 422) {
          var message = Object.values(err.response.data.errors)[0][0]
          Vue.toasted.error(message,{
            duration: 5000
          });
          //throw new Error(`${err.config.url} not found`);
        }
        throw err;
      },500)
    );
  }

  const get_user_detail = () => {
    return Vue.prototype.$auth.setting;
  }

  const get_user = () => {
    return Vue.prototype.$auth.user;    
  } 

  const set_token = (token) => localStorage.setItem('token',token)
  const get_token = () => localStorage.getItem('token')
  const remove_token = () => localStorage.removeItem('token');

   // error messages
  const error_message = (message) => Vue.toasted.error(message,{duration: 5000});
  const success_message = (message) => Vue.toasted.success(message,{duration: 5000});
  const toastr_message = (message, type) => Vue.toasted[type](message,{duration: 5000});

  const replaceByDefault = (event) => (event.target.src = config.no_image)

  const multipart_headers = () => ({'Content-Type': 'multipart/form-data'})

  const get_currency = () => {
    const setting = get_user_detail();
    if(setting) {
      return setting.currency;
    }
    return '&#x20B9;';
  }

  const order_statuses = {
    placed: "Placed",
    inkitchen: "InKitchen",
    readytoserve: "ReadyToServe",
    ontheway: "OnTheWay",
    arrived: "Arrived",
    delivered: "Delivered",
    cancelled: "Cancelled",
    scheduled: "Scheduled",
    paymentpending: "PaymentPending",
    paymentfailure: "PaymentFailure",
  }

  const date_ranges = () =>{
    let today = new Date()
    today.setHours(0, 0, 0, 0)

    return {
      'Today': [moment().startOf('day').toDate(), moment().startOf('day').toDate()],
      'Yesterday': [moment().subtract(1,'day').startOf('day').toDate(), moment().subtract(1,'day').startOf('day').toDate()],
      'Last 7 days': [moment().subtract(1,'week').startOf('day').toDate(), moment().startOf('day').toDate()],
      'Last 30 Days': [moment().subtract(30,'days').startOf('day').toDate(),moment().startOf('day').toDate()],
      'This year': [new Date(today.getFullYear(), 0, 1), new Date(today.getFullYear(), 11, 31)],
      'Last month': [new Date(today.getFullYear(), today.getMonth() - 1, 1), new Date(today.getFullYear(), today.getMonth(), 0)],
    }
  }

  const get_decimal = () => {
    const setting = get_user_detail();
    if(setting) {
      return setting.decimal || 2;
    }
    return '&#x20B9;';
  }

  const set_partner_login = () => localStorage.setItem('partner_login','1')
  const set_partner_token = (partner_token) => localStorage.setItem('partner_token',partner_token)
  const remove_partner_token = () => localStorage.removeItem('partner_token')

  export {
    set_axios_defaults,
    set_token,
    get_token,
    remove_token,
    get_user_detail,
    replaceByDefault,
    get_currency,
    multipart_headers,
    error_message,
    success_message,
    toastr_message,
    debounce,
    order_statuses,
    date_ranges,
    get_decimal,
    get_user,
    set_partner_login,
    set_partner_token,
    remove_partner_token
  };

