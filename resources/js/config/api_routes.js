import config from './index';

const routes = {

    //login
    login: config.api_url+'login',

    //UserDetail
    userDetailApi: config.api_url+'user-detail',

    //logout
    logout: config.api_url+'logout',

    // signupClientApi
    signupClientApi: config.api_url+'signup',
    confirEmailClientApi: config.api_url+'confir-email-client',
    resendOtpApi: config.api_url+'resend-otp',


    // forgot password
    forgotPasswordApi:config.api_url+'forgot-passwor',

    // resetPassword 
    resetPasswordApi:config.api_url+'reset-password',

    //Menu
    addMenuApi: config.api_url+'add-menu',
    getMenuMasterApi: config.api_url+'master/menu',
    getMasterMenuByIdApi: config.api_url+'get-menu-by-id',
    deleteMasterMenuApi: config.api_url+'delete-menu',
    editMasterMenuApi: config.api_url+'edit-menu',
    updateMasterMenuStatusApi : config.api_url+'update-master-menu-status',
    menuImageDeleteUrl: config.api_url+'delete-menu-image',
    updateMenuStatusApi: config.api_url+'update-menu-status',
    updateMenuOrderApi: config.api_url+'update-menu-order',
    getItemTypes: config.api_url+'item-types',

    //Category
    addMasterCategoryApi: config.api_url+'add-category',
    getMasterCategoriesApi: config.api_url+'get-categrory-list',
    getMasterCategoriesByIdApi: config.api_url+'get-master-category-by-id',
    updateMasterCategoryByIdApi: config.api_url+'update-master-category',
    deleteMasterCategoryApi: config.api_url+'delete-master-category',
    categroyImageDeleteUrl: config.api_url+'delete-category-image-by-name',

    //MasterItem
    addMasterItemApi: config.api_url+'add-master-item',
    getMasterItemApi: config.api_url+'get-master-item',
    getMastersItemByIdApi:config.api_url+'master/item',
    deleteMenuItemApi: config.api_url+'delete-menu-item',
    deleteMasterWithItemApi: config.api_url+'delete-master-with-item',
    getMasterItemImageApi: config.api_url+'get-master-item-image',
    addMasterImageApi :  config.api_url+'add-master-image',
    masterImageDeleteUrl: config.api_url+'delete-master-image-by-id',
    importMasterItemApi: config.api_url+'import-master-item',
    menuItemUpdateUplodApi: config.api_url+'uplod-updet-master-item',
    masterItemEditApi: config.api_url+'update-master-item',
    updateItemAvailabilityApi: config.api_url+'update-item-availability',
    updateItemOrderApi: config.api_url+'update-item-order',

     //upload-image
     uploadTmpImageApi: config.api_url+'upload-temp-image',
     removeTmpImageApi: config.api_url+'remove-temp-image',

    //  get ClientDetaile
    getClientDetaileApi:config.api_url+'get-client-detaile',

    // update ClientDetaile
    updateClientDetaileApi:config.api_url+'update-client-detaile',

    // updateClientPassword
    updateClientPasswordApi:config.api_url+'update-client-password',

    //Customer
    getCustomerApi: config.api_url+'get-customer',
    customerExportToCsvApi: config.api_url+'customer-export-csv',
    getCustomerProfileApi: config.api_url+'get-customer-profile',
    updateCustomerProfileApi: config.api_url+'update-customer-profile',
    getCustomerOrderApi: config.api_url+'get-customer-order',
    changeCusPasswordApi: config.api_url+'change-cust-password',

    //upload-image
    uploadTmpImageApi: config.api_url+'upload-temp-image',
    removeTmpImageApi: config.api_url+'remove-temp-image',

    //Customization
    getItemCustmizationApi : config.api_url+'get-item-customization',
    addCustomizationApi: config.api_url+'add-customization',
    getCustmizationTypeApi : config.api_url+'get-customization-type',
    customizationEditApi : config.api_url+'update-customization-type',
    addMenuSelectionApi : config.api_url+'add-menu-selection',
    deleteItemCustmizationApi : config.api_url+'delete-item-customization',
    saveTemplateApi : config.api_url+'save-template',
    addTemplateApi : config.api_url+'add-template',
    getTemplateApi : config.api_url+'get-template',
    deleteCustmizationTypeApi : config.api_url+'delete-customization-type',

    //SubCustomization
    getSubCustomizationApi : config.api_url+'get-sub-customization',
    addSubMenuSelectionApi : config.api_url+'add-sub-menu-selection',
    minSelectionApi : config.api_url+'min-selection',
    maxSelectionApi : config.api_url+'max-selection',

    //PromoCode
    getPromoCodeApi: config.api_url+'get-promocode',
    addPromoCodeApi: config.api_url+'add-promocode',
    getCustomerLisingApi: config.api_url+'get-customer-listing',
    getPromoCodeByIdApi: config.api_url+'get-promocode-by-id',
    promoCodeImageDeleteUrl: config.api_url+'promocode/delete-promocode-by-name',
    updatePromoCodeApi: config.api_url+'update-promocode',

    //Orders
    getOrdersApi: config.api_url+'orders/get',
    getNewOrdersApi: config.api_url+'orders/new/get',
    exportOrdersApi: config.api_url+'orders/export',
    getOrderApi: config.api_url+'order/get',
    getOrdersCountApi: config.api_url+'order/get/count',
    checkAvailableDriversApi: config.api_url+'delivery_zone/drivers',
    assignDriverApi: config.api_url+'order/assign_driver',

    //wallet
    getHistoryApi: config.api_url+'wallet/get-history',
    getBalanceApi: config.api_url+'wallet/get-balance',
    changeWalletStatusApi: config.api_url+'wallet/change-status',

    // Outlet
    getRestaurantApi : config.api_url+'get-restaurant',
    updatedRestaurantApi : config.api_url+'updated-restaurant',

    getRestaurantCuisinesApi : config.api_url+'get-restaurant-cuisines',
    updatedRestaurantCuisinesApi : config.api_url+'updated-restaurant-cuisines',
    
    // service offering
    getReataurantSettingApi : config.api_url+'get-reataurant-setting',
    getServiceOfferingApi : config.api_url+'get-service-offering',
    saveServiceOfferingApi : config.api_url+'save-service-offering',
    
    // taxt setting
    getTaxSettingApi : config.api_url+'get-tax-setting',
    taxSettingStoreUpdatedApi : config.api_url+'tax-setting-store-updat',
    
    // outher Setting
    getOtherSettingApi : config.api_url+'get-other-setting',
    otherSettingStoreUpdateApi : config.api_url+'other-setting-store-update',

    //WebLayoutSetting
    getWebSettingApi :  config.api_url+'get-web-settings',
    updateWebSettingApi : config.api_url+'update-web-settings',
    deleteWebLogoApi :  config.api_url+'delete-web-logo',
    deleteWebHomePageApi: config.api_url+'delet-home-page-image',
    deleteSectionImageApi: config.api_url+'delete-section-image',
    deleteFrontImageApi: config.api_url+'delete-front-image',
    
    // setting
    updateSettingApi:config.api_url+'update-setting',
    getSettingApi:config.api_url+'get-setting',
    deleteGeneralSettingByKeyApi:config.api_url+'delete-general-setting-by-key',

    // remove-ios-pem
    removeIOSPemApi: config.api_url+'remove-ios-pem',

    // PaymentGateways
    getPaymentGatewaysApi : config.api_url+'payment-gateways',
    storeClientPaymentGatewayApi : config.api_url+'store-client-payment-cateway',
    getClientPaymentGatewayApi : config.api_url+'get-client-payment-cateway',
    clientGateweyDeactiveApi : config.api_url+'client-gatewey-deactive',
    

    // Email Template    
    getMailApi : config.api_url+'get-mail-list',
    addMailApi : config.api_url+'add-mail',
    viewMailApi : config.api_url+'view-mail',
    getMailByIdApi : config.api_url+'get-mail-by-id',
    updateMailByIdApi : config.api_url+'update-mail',    

    getAllClientApi:config.api_url+'get-all-client',
    getQrCodeApi : config.api_url+'get-qr-code',
    getClientSettingByIdApi: config.api_url+'get-client-setting-by-id',

    getLanguageApi: config.api_url+'get-language',

    //ContectPage
    getContentPageApi : config.api_url+'get-content-page',
    addContentPageApi : config.api_url+'add-content-page',
    getContentPageByIdApi : config.api_url+'get-content-page-by-id',
    editContentPageApi : config.api_url+'edit-content-page',
    viewContentPageApi : config.api_url+'view-content-page',
    deleteContentPageApi : config.api_url+'delete-content-page',
    getStaticPageApi : config.api_url+'get-static-page',
    getVendorTermApi : config.api_url+'get-vendor-term-page',

    //Dashboard
    getDashboardApi :  config.api_url+'get-dashboard',
    getMonthWiseDeliveredOrderApi : config.api_url+'get-month-wise-delivered-order',
    getMonthWiseSalesApi :  config.api_url+'get-month-wise-sales',
    


}


export { routes }
