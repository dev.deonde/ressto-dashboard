import config from '../config'

export default {
    config: config,
}
const permissions = config.permissions;
const base = config.base_folder
export const menuItems = [
    {
        id: 1,
        icon: "bx-home-circle",
        label: "Dashboard",
        parentId: 1,
        link: base + "dashboard",
        required_permission: permissions.dashboard_read,
        permissions: [permissions.dashboard_read]
    },
    {
        id: 2,
        icon: "bx-copy-alt",
        label: "Live Order",
        parentId: 2,
        link: base + "orders/list",
        required_permission: permissions.order_read,
        permissions: [permissions.order_read,permissions.order_write]
    },
    {
        id: 3,
        icon: "bx bx-book",
        label: "Menu Management",
        parentId: 3,
        link: base + "menu",
        required_permission: permissions.menu_read,
        permissions: [permissions.menu_read,permissions.menu_write]
    },
    {
        id: 4,
        icon: "bx bx-user",
        label: "Customer",
        parentId: 4,
        link: base + "customer",
        required_permission: permissions.customer_read,
        permissions: [permissions.customer_read,permissions.customer_write]
    },
    {
        id: 5,
        icon: "bx-purchase-tag",
        label: "Promocode",
        parentId: 5,
        link: base + "promocode",
        required_permission: permissions.promo_code_read,
        permissions: [permissions.promo_code_read,permissions.promo_code_write]
    },
    {
        id: 6,
        icon: "bx-notepad",
        label: "Outlet",
        parentId: 6,
        link: base + "outlet",
        required_permission: permissions.outlet_read,
        permissions: [permissions.outlet_read,permissions.outlet_write]
    },
    {
        id: 11,
        icon: "bx-notepad",
        label: "Sales Channel",
        link: base + "sales-channel",
        parentId: 11,
        required_permission: permissions.sales_channel_read,
        permissions: [permissions.sales_channel_read, permissions.sales_channel_write]
    },
    {
        id: 7,
        icon: "bx-wrench",
        label: "Setting",
        subItems: [
            {
                id: 8,
                label: "Web Configurations",
                link: base + "settings/web-layout-setting",
                parentId: 7,
                required_permission: permissions.web_layout_read,
                permissions: [permissions.web_layout_read, permissions.web_layout_write]
            },
            {
                id: 9,
                label: "Settings",
                link: base + "settings/setting",
                parentId: 7,
                required_permission: permissions.setting_read,
                permissions: [permissions.setting_Write, permissions.setting_read]
            },
            {
                id: 10,
                label: "Integrations",
                link: base + "settings/addon-integration",
                parentId: 7,
                required_permission: permissions.addon_integration_read,
                permissions: [permissions.addon_integration_write, permissions.addon_integration_write]
            },
            {
                id: 10,
                label: "Mail",
                link: base + "mail",
                parentId: 7,
                required_permission: permissions.email_template_read,
                permissions: [permissions.email_template_write, permissions.email_template_write]
            },
            {
                id: 12,
                label: "Pages",
                link: base + "content-page",
                parentId: 7,
                required_permission: permissions.content_page_read,
                permissions: [permissions.content_page_read, permissions.content_page_write]
            },
        ]
    }        

];
