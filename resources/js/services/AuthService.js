
// Login services rest api calling

import Axios from "axios";
import { routes } from '../config/api_routes'

export const authService = {
    loginClient: (formData) => {
        return Axios.post(routes.login,formData,{
        headers: {
            'Accept': 'application/json',
          }
        });
    },

    getUserDetail: () => window.axios.get(routes.userDetailApi),

    logout: () => {
        return window.axios.post(routes.logout,{},{
            headers: {
                'Accept': 'application/json',
            }
        });
    },
    
    confirEmailClient: (formData) => {
        return Axios.post(routes.confirEmailClientApi,formData,{
        headers: {
            'Accept': 'application/json',
          }
        });
    },

    signupClient : (formData) => window.axios.post(routes.signupClientApi, formData),
    // confirEmailClient : (formData) => window.axios.post(routes.confirEmailClientApi, formData),
    forgotPassword : (formData) => window.axios.post(routes.forgotPasswordApi, formData),
    resetPassword : (formData) => window.axios.post(routes.resetPasswordApi, formData),
    resendOtp : (formData) => window.axios.post(routes.resendOtpApi, formData)
}
