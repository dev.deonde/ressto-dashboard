import { routes } from '../config/api_routes'

export const walletService = {
    getHistory: (data) => window.axios.get(routes.getHistoryApi,{params: data}),
    getBalance: (customer_id) => window.axios.get(routes.getBalanceApi,{params: {customer_id}}),
    changeWalletStatus: (data)  =>  window.axios.post(routes.changeWalletStatusApi,data),
}