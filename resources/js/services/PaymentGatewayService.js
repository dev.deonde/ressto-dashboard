import { routes } from '../config/api_routes'

export const paymentGatewayService = {

    getPaymentGateways : () => window.axios.get(routes.getPaymentGatewaysApi),
    storeClientPaymentGateway : (data) => window.axios.post(routes.storeClientPaymentGatewayApi, data),
    getClientPaymentGateway : (data) => window.axios.post(routes.getClientPaymentGatewayApi, data),
    clientGateweyDeactive : (data) => window.axios.post(routes.clientGateweyDeactiveApi, data),
}