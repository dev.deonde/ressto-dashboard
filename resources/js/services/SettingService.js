import { routes } from '../config/api_routes'

export const settingService = {
    updateSetting: (data) => window.axios.post(routes.updateSettingApi, data),
    deleteGeneralSettingByKey: (data) => window.axios.post(routes.deleteGeneralSettingByKeyApi, data),
    getSetting: (data) => window.axios.post(routes.getSettingApi, data),    
}