
// Menu services rest api calling
import { multipart_headers } from '../Helper/helper';
import { routes } from '../config/api_routes'

export const menuService = {
    addMenu: (formData) => window.axios.post(routes.addMenuApi, formData, { headers: multipart_headers() }),

    getMenuMaster: (formData) => window.axios.post(routes.getMenuMasterApi, formData),

    getMasterMenuById: (formData) => window.axios.post(routes.getMasterMenuByIdApi, formData),

    editMasterMenu: (formData) => window.axios.post(routes.editMasterMenuApi, formData),

    deleteMasterMenu: (formData) => window.axios.post(routes.deleteMasterMenuApi, formData),

    updateMasterMenuStatus: (formData) => window.axios.post(routes.updateMasterMenuStatusApi, formData),

    updateMenuStatus: (formData) => window.axios.post(routes.updateMenuStatusApi, formData),

    updateMenuOrder: (data) => window.axios.post(routes.updateMenuOrderApi, data)
}