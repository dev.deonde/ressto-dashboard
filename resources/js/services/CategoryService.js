
// Category services rest api calling

import { routes } from '../config/api_routes'

export const categoryService = {
    addMasterCategory: (formData) => window.axios.post(routes.addMasterCategoryApi, formData),

    getMasterCategories: (formData) => window.axios.post(routes.getMasterCategoriesApi, formData),

    getMasterCategoriesById: (formData) => window.axios.post(routes.getMasterCategoriesByIdApi, formData),

    updateMasterCategoryById: (formData) => window.axios.post(routes.updateMasterCategoryByIdApi, formData),

    deleteMasterCategory: (formData) => window.axios.post(routes.deleteMasterCategoryApi, formData),
}