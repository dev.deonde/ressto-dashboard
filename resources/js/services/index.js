import { authService } from './AuthService';
import { menuService } from './MenuService';
import { categoryService } from './CategoryService';
import { masterItemService } from './MasterItemService';
import { commonService } from './CommonService';
import { ProfileService } from './ProfileService';
import { CustomerService } from './CustomerService'
import { customizationService } from './CustomizationService';
import { promoCodeService } from './PromoCodeService';
import { orderService } from './OrderService';
import { walletService } from './WalletService';
import { outletService } from './OutletService';
import { webLayoutService } from './WebLayoutService';
import { settingService } from './SettingService';
import { paymentGatewayService } from './PaymentGatewayService'
import { mailService } from './mailService'
import { clientService } from './ClientService'
import { pageService } from './PageService'
import { dashboardService } from './DashboardService'
export {
    authService,
    menuService,
    categoryService,
    masterItemService,
    commonService,
    ProfileService,
    CustomerService,
    customizationService,
    promoCodeService,
    orderService,
    walletService,
    outletService,
    webLayoutService,
    settingService,
    paymentGatewayService,
    mailService,
    clientService,
    pageService,
    dashboardService
};