import { routes } from '../config/api_routes'

export const outletService = {
    getRestaurant : () => window.axios.get(routes.getRestaurantApi),
    updatedRestaurant : (data) => window.axios.post(routes.updatedRestaurantApi, data),

    // cuisines
    getRestaurantCuisines : () => window.axios.get(routes.getRestaurantCuisinesApi),
    updatedRestaurantCuisines : (data) => window.axios.post(routes.updatedRestaurantCuisinesApi, data),
    
    // service offering
    getReataurantSetting : (data) => window.axios.post(routes.getReataurantSettingApi, data),
    getServiceOffering : (data) => window.axios.post(routes.getServiceOfferingApi, data),
    saveServiceOffering : (data) => window.axios.post(routes.saveServiceOfferingApi, data),

    // tax Setting
    getTaxSetting : () => window.axios.get(routes.getTaxSettingApi),
    taxSettingStoreUpdated : (data) => window.axios.post(routes.taxSettingStoreUpdatedApi, data),

    // other Setting
    getOtherSetting : () => window.axios.get(routes.getOtherSettingApi),
    otherSettingStoreUpdate : (data) => window.axios.post(routes.otherSettingStoreUpdateApi, data),

}