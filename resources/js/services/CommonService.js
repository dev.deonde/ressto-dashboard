
import { routes } from '../config/api_routes'

export const commonService = {
    removeTmpImage: (file) => window.axios.post(routes.removeTmpImageApi,{file}),
}