
// Madster Item services rest api calling

import { routes } from '../config/api_routes'

export const masterItemService = {
    addMasterItem: (formData) => window.axios.post(routes.addMasterItemApi, formData),

    getMasterItem: (formData) => window.axios.post(routes.getMasterItemApi, formData),

    getMastersItemById: (formData) => window.axios.post(routes.getMastersItemByIdApi, formData),

    deleteMenuItem: (formData) => window.axios.post(routes.deleteMenuItemApi, formData),

    deleteMasterWithItem: (formData) => window.axios.post(routes.deleteMasterWithItemApi, formData),

    getMasterItemImage: (formData) => window.axios.post(routes.getMasterItemImageApi, formData),

    addMasterImage: (formData) => window.axios.post(routes.addMasterImageApi, formData),

    importMasterItem: (formData) => window.axios.post(routes.importMasterItemApi, formData),
    menuItemUpdateUplod: (formData) => window.axios.post(routes.menuItemUpdateUplodApi, formData),

    masterItemEdit: (formData) => window.axios.post(routes.masterItemEditApi, formData),

    updateItemAvailability: (data) => window.axios.post(routes.updateItemAvailabilityApi, data),

    updateItemOrder: (data) => window.axios.post(routes.updateItemOrderApi, data),

    getItemTypeService: (data) => window.axios.get(routes.getItemTypes, data)
}