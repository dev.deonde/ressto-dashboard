
// Login services rest api calling
import { routes } from '../config/api_routes'

export const ProfileService = {

    getClientDetaile : () => window.axios.get(routes.getClientDetaileApi),
    updateClientDetaile : (data) => window.axios.post(routes.updateClientDetaileApi, data),
    updateClientPassword : (data) => window.axios.post(routes.updateClientPasswordApi, data),
}
